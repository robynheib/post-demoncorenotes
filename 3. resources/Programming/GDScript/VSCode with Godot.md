# VSCode with Godot

[Godot tools: How to use VSCode with Godot](https://www.youtube.com/watch?v=nPV_qXu1rkg)

----
Learn how the Godot Tools for Visual Studio Code offer a powerful alternative to the native script editor. Visual Studio Code (open source / Win, Mac, Linux): https://code.visualstudio.com/ 
To install the Godot Tools extensions, in VSCode, go to the extensions tab (Ctrl Shift X) and search for "Godot" 
----
0:45: Why VSCode?
2:24: Useful features in VSCode
8:08: Godot Tools overview
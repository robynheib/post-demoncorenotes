[![](https://nobility.org/wp-content/uploads/2011/10/Siemiradski_Fackeln-1024x558.jpg)](https://nobility.org/wp-content/uploads/2011/10/Siemiradski_Fackeln.jpg)

During the persecution of the early martyrs, one of the delights of Nero was to make the Christians into human torches, so his garden would be lit up at night.
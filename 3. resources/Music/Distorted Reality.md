# Distorted Reality

[Spectrasonics Distorted Reality 1 Exclusive Samples (Youtube Video)](https://www.youtube.com/watch?v=VHmN6KDUEXw)

[Official Website](https://www.spectrasonics.net/products/legacy/distortedreality1.php)

[Demo](https://soundcloud.com/ilioenterprises/distorted-reality-demo)

![Cover](https://www.spectrasonics.net/products/legacy/legacy_images/lp_Distorted_Reality-1.png)

# Distorted Reality 2

[# Spectrasonics - Distorted Reality 2 Darkness & Light (Demo) + (akai iso Download) (Youtube Video)](https://www.youtube.com/watch?v=YhJHo7WzBmU)

[Official Website](https://www.spectrasonics.net/products/legacy/distortedreality2.php)

![Cove](https://www.spectrasonics.net/products/legacy/legacy_images/lp_Distorted_Reality-2.png)

https://audioz.download/samples/midi-akai-soundfonts/93354-download_spectrasonics-distorted-reality-vol1-2-akai-audiop2p.html
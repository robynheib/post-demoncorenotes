# [Bakku No Oni HIBIKI Sound Collection 1-3](https://archive.org/details/bakku-no-oni-hibiki-sound-collection/Bakku%20no%20Oni%20Hibiki%201%20front.jpg)

by [A&P CO-ORDINATOR JAPAN](https://archive.org/search.php?query=creator%3A%22A%26P+CO-ORDINATOR+JAPAN%22)



[Bakku no Oni HIBIKI 1 44kHz.7z download](https://archive.org/download/bakku-no-oni-hibiki-sound-collection/Bakku%20no%20Oni%20HIBIKI%201%2044kHz.7z) 56.0M

[Bakku no Oni HIBIKI Sound Collection 1-3.7z](https://archive.org/download/bakku-no-oni-hibiki-sound-collection/Bakku%20no%20Oni%20HIBIKI%20Sound%20Collection%201-3.7z) 621.3M  

![Cover](https://ia803407.us.archive.org/24/items/bakku-no-oni-hibiki-sound-collection/Bakku%20no%20Oni%20Hibiki%201%20front.jpg)

Publication date

[1996-05](https://archive.org/search.php?query=date:1996-05)

Topics

[audio](https://archive.org/search.php?query=subject%3A%22audio%22), [sounds](https://archive.org/search.php?query=subject%3A%22sounds%22), [stock](https://archive.org/search.php?query=subject%3A%22stock%22), [abandonware](https://archive.org/search.php?query=subject%3A%22abandonware%22)

Obtained by Fanamel of [[Render96]]

Addeddate

2021-04-20 12:21:10

Identifier

bakku-no-oni-hibiki-sound-collection

Scanner

Internet Archive HTML5 Uploader 1.6.4
          

# Flamethrower, Portable, No 2

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/Flamethrower,_Portable,_No_2#mw-head) [Jump to search](https://en.wikipedia.org/wiki/Flamethrower,_Portable,_No_2#searchInput)

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This article **needs additional citations for [verification](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=Flamethrower,_Portable,_No_2&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and removed.  
_Find sources:_ ["Flamethrower, Portable, No 2"](https://www.google.com/search?as_eq=wikipedia&q=%22Flamethrower%2C+Portable%2C+No+2%22) – [news](https://www.google.com/search?tbm=nws&q=%22Flamethrower%2C+Portable%2C+No+2%22+-wikipedia) **·** [newspapers](https://www.google.com/search?&q=%22Flamethrower%2C+Portable%2C+No+2%22&tbs=bkt:s&tbm=bks) **·** [books](https://www.google.com/search?tbs=bks:1&q=%22Flamethrower%2C+Portable%2C+No+2%22+-wikipedia) **·** [scholar](https://scholar.google.com/scholar?q=%22Flamethrower%2C+Portable%2C+No+2%22) **·** [JSTOR](https://www.jstor.org/action/doBasicSearch?Query=%22Flamethrower%2C+Portable%2C+No+2%22&acc=on&wc=on) _(April 2009)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

Flamethrower, Portable, No 2

[![IWM-H-37975-Flame-thrower-lifebuoy.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/IWM-H-37975-Flame-thrower-lifebuoy.jpg/220px-IWM-H-37975-Flame-thrower-lifebuoy.jpg)](https://en.wikipedia.org/wiki/File:IWM-H-37975-Flame-thrower-lifebuoy.jpg)

A soldier of the [King's Own Scottish Borderers](https://en.wikipedia.org/wiki/King%27s_Own_Scottish_Borderers "King's Own Scottish Borderers") demonstrates the Lifebuoy flamethrower, Denmead, Hampshire, 29 April 1944.

Type

[Flamethrower](https://en.wikipedia.org/wiki/Flamethrower "Flamethrower")

Place of origin

United Kingdom

Service history

Used by

[British Army](https://en.wikipedia.org/wiki/British_Army "British Army"), [Canadian Forces](https://en.wikipedia.org/wiki/Canadian_Forces "Canadian Forces")

Wars

[Second World War](https://en.wikipedia.org/wiki/Second_World_War "Second World War")

Production history

Produced

1943–1944

No. built

7,000

Specifications

Mass

64 lb (29 kg)

---

[Action](https://en.wikipedia.org/wiki/Action_(firearms) "Action (firearms)")

cordite igniter

[Rate of fire](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire")

10 igniters only

Maximum firing range

120 ft

The **Flamethrower, Portable, No 2** (nicknamed _Lifebuoy_ from the shape of its fuel tank), also known as the _Ack Pack_, was a British design of [flamethrower](https://en.wikipedia.org/wiki/Flamethrower "Flamethrower") for infantry use in the [Second World War](https://en.wikipedia.org/wiki/World_War_II "World War II").

## Contents

-   [1 Description](https://en.wikipedia.org/wiki/Flamethrower,_Portable,_No_2#Description)
-   [2 Pictures](https://en.wikipedia.org/wiki/Flamethrower,_Portable,_No_2#Pictures)
-   [3 See also](https://en.wikipedia.org/wiki/Flamethrower,_Portable,_No_2#See_also)
-   [4 References](https://en.wikipedia.org/wiki/Flamethrower,_Portable,_No_2#References)

## Description

It was a near copy of the German [Wechselapparat](https://en.wikipedia.org/wiki/Wechselapparat "Wechselapparat") ("Wex") from 1917.

The Mark 1 was used as a training weapon, while the improved Mark 2 was used in action. Over 7,000 units were produced from 1943 to 1944. They were ready for service during [Operation Overlord](https://en.wikipedia.org/wiki/Operation_Overlord "Operation Overlord") (the [Allied](https://en.wikipedia.org/wiki/Allies_of_World_War_II "Allies of World War II") invasion of Normandy).

The Ack Pack was a harness carrying a [doughnut](https://en.wikipedia.org/wiki/Doughnut "Doughnut")-shaped fuel container with a capacity of 4 Imperial [gallons](https://en.wikipedia.org/wiki/Gallon "Gallon") (18 [litres](https://en.wikipedia.org/wiki/Litre "Litre")) of fuel on the operator's back. In the middle of the "doughnut" was a spherical container holding nitrogen gas as a propellant, which was pressurized to 2,000 lbf/in² (140 [Bar](https://en.wikipedia.org/wiki/Bar_(unit) "Bar (unit)")). This was sufficient to propel the burning fuel 120 feet (36 metres). A hose from the fuel tank passed to the nozzle assembly which had two pistol grips to hold and aim the spray. The back grip had the trigger.

In some versions the nozzle was fitted with a 10-chambered cylinder which contained the ignition cartridges. These could be fired once, each giving the operator 10 bursts of flame. In practice this gave 10 one-second bursts. It was also possible to spray fuel without igniting it to ensure there was plenty splashed around the target, then fire an ignited burst to light up the whole lot.

At some 64 pounds (29 kg) the flamethrower was considered heavy.

## Pictures

-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/The_British_Army_in_the_United_Kingdom_1939-45_H37977.jpg/120px-The_British_Army_in_the_United_Kingdom_1939-45_H37977.jpg)](https://en.wikipedia.org/wiki/File:The_British_Army_in_the_United_Kingdom_1939-45_H37977.jpg)
    
    The Lifebuoy man-portable flamethrower being demonstrated to men of 1st Battalion, King's Own Scottish Borderers, Denmead, Hampshire, 29 April 1944.
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Flamethrower-portable-no-2-batey-haosef-1.jpg/83px-Flamethrower-portable-no-2-batey-haosef-1.jpg)](https://en.wikipedia.org/wiki/File:Flamethrower-portable-no-2-batey-haosef-1.jpg)
    
    A Flamethrower Portable, No 2 in the [Israel Defense Forces History Museum](https://en.wikipedia.org/wiki/Israel_Defense_Forces_History_Museum "Israel Defense Forces History Museum"), Tel Aviv, Israel (September 2015)
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Flamethrowers_in_Action%2C_August_1944_TR2318.jpg/120px-Flamethrowers_in_Action%2C_August_1944_TR2318.jpg)](https://en.wikipedia.org/wiki/File:Flamethrowers_in_Action,_August_1944_TR2318.jpg)
    
    A Life Buoy flamethrower in action. August 1944.
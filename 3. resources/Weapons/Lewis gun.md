# Lewis gun

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/Lewis_gun#mw-head) [Jump to search](https://en.wikipedia.org/wiki/Lewis_gun#searchInput)

Lewis gun

[![Lewis Gun (derivated).jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Lewis_Gun_%28derivated%29.jpg/220px-Lewis_Gun_%28derivated%29.jpg)](https://en.wikipedia.org/wiki/File:Lewis_Gun_(derivated).jpg)

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/9dc93bb0-0d78-4fd5-8e2a-749e510e963c/dbls1bk-831e1121-449f-4664-a2c0-a58ac2c8c998.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzlkYzkzYmIwLTBkNzgtNGZkNS04ZTJhLTc0OWU1MTBlOTYzY1wvZGJsczFiay04MzFlMTEyMS00NDlmLTQ2NjQtYTJjMC1hNThhYzJjOGM5OTguanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.VYiqn6zT7b-73YzeS8u39NqQnzArVpueDFI3HG4OoHs)

Type

[Light machine gun](https://en.wikipedia.org/wiki/Light_machine_gun "Light machine gun")

Place of origin

United States/United Kingdom

Service history

In service

1914–1953

Used by

See [Users](https://en.wikipedia.org/wiki/Lewis_M1914#Users "Lewis M1914")

Wars

-   [First World War](https://en.wikipedia.org/wiki/World_War_I "World War I")
-   [Easter Rising](https://en.wikipedia.org/wiki/Easter_Rising "Easter Rising")
-   [Pancho Villa Expedition](https://en.wikipedia.org/wiki/Pancho_Villa_Expedition "Pancho Villa Expedition")[[1]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-1)
-   [Emu War](https://en.wikipedia.org/wiki/Emu_War "Emu War")
-   [Banana Wars](https://en.wikipedia.org/wiki/Banana_Wars "Banana Wars")
-   [Irish War of Independence](https://en.wikipedia.org/wiki/Irish_War_of_Independence "Irish War of Independence")
-   [Irish Civil War](https://en.wikipedia.org/wiki/Irish_Civil_War "Irish Civil War")
-   [Russian Civil War](https://en.wikipedia.org/wiki/Russian_Civil_War "Russian Civil War")
-   [Latvian War of Independence](https://en.wikipedia.org/wiki/Latvian_War_of_Independence "Latvian War of Independence")
-   [Polish–Soviet War](https://en.wikipedia.org/wiki/Polish%E2%80%93Soviet_War "Polish–Soviet War")
-   [Chaco War](https://en.wikipedia.org/wiki/Chaco_War "Chaco War")
-   [Spanish Civil War](https://en.wikipedia.org/wiki/Spanish_Civil_War "Spanish Civil War")
-   [Second World War](https://en.wikipedia.org/wiki/World_War_II "World War II")
-   [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War")
-   [Malayan Emergency](https://en.wikipedia.org/wiki/Malayan_Emergency "Malayan Emergency")
-   [1948 Arab–Israeli War](https://en.wikipedia.org/wiki/1948_Arab%E2%80%93Israeli_War "1948 Arab–Israeli War")
-   [Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War")
-   [Algerian War](https://en.wikipedia.org/wiki/Algerian_War "Algerian War")
-   [The Troubles](https://en.wikipedia.org/wiki/The_Troubles "The Troubles")
-   Other conflicts

Production history

Designer

-   Samuel McClean
-   [Isaac Newton Lewis](https://en.wikipedia.org/wiki/Isaac_Newton_Lewis "Isaac Newton Lewis")
-   [Birmingham Small Arms Co. Ltd.](https://en.wikipedia.org/wiki/Birmingham_Small_Arms_Company "Birmingham Small Arms Company")

Designed

1911

Manufacturer

-   Birmingham Small Arms Co. Ltd.
-   [Savage Arms](https://en.wikipedia.org/wiki/Savage_Arms "Savage Arms") Co.

Produced

1913–1942

No. built

152,050 in [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")  
50,000 chambered in [.30-06](https://en.wikipedia.org/wiki/.30-06 ".30-06")

Variants

-   Mks I–V
-   Aircraft Pattern
-   [Anti-Aircraft](https://en.wikipedia.org/wiki/Anti-aircraft_warfare "Anti-aircraft warfare") configuration
-   Light Infantry Pattern
-   Savage M1917

Specifications

Mass

28 pounds (13 kg)

Length

50.5 inches (1,280 mm)

[Barrel](https://en.wikipedia.org/wiki/Gun_barrel "Gun barrel") length

26.5 inches (670 mm)

Width

4.5 inches (110 mm)

---

[Cartridge](https://en.wikipedia.org/wiki/Cartridge_(firearms) "Cartridge (firearms)")

-   [.303 British](https://en.wikipedia.org/wiki/.303_British ".303 British")
-   [.30-06 Springfield](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield")
-   [7.92×57mm Mauser](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser "7.92×57mm Mauser")
-   [7.62×54mmR](https://en.wikipedia.org/wiki/7.62%C3%9754mmR "7.62×54mmR")

[Action](https://en.wikipedia.org/wiki/Action_(firearms) "Action (firearms)")

[Gas-operated](https://en.wikipedia.org/wiki/Gas-operated_reloading "Gas-operated reloading") long stroke gas piston, rotating [open bolt](https://en.wikipedia.org/wiki/Open_bolt "Open bolt")

[Rate of fire](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire")

500–600 rounds/min

[Muzzle velocity](https://en.wikipedia.org/wiki/Muzzle_velocity "Muzzle velocity")

2,440 feet per second (740 m/s)

Effective firing range

880 yards (800 m)

Maximum firing range

3,500 yards (3,200 m)

Feed system

47- or 97-round [pan magazine](https://en.wikipedia.org/wiki/Pan_magazine "Pan magazine")

Sights

Blade and tangent leaf

The **Lewis gun** (or **Lewis automatic machine gun** or **Lewis automatic rifle**) is a [First World War–era](https://en.wikipedia.org/wiki/World_War_I "World War I") [light machine gun](https://en.wikipedia.org/wiki/Light_machine_gun "Light machine gun"). Designed privately in America but not adopted, the design was finalised and mass-produced in the United Kingdom,[[2]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEEasterly199865-2) and widely used by troops of the [British Empire](https://en.wikipedia.org/wiki/British_Empire "British Empire") during the war. It had a distinctive barrel cooling shroud (containing a finned, aluminium breech-to-muzzle [heat sink](https://en.wikipedia.org/wiki/Heat_sink "Heat sink") to cool the gun barrel) and top-mounted [pan magazine](https://en.wikipedia.org/wiki/Pan_magazine "Pan magazine"). The Lewis served to the end of the [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War"), and was widely used as an aircraft machine gun during both World Wars, almost always with the cooling shroud removed, as air flow during flight offers sufficient cooling.

## Contents

-   [1 History](https://en.wikipedia.org/wiki/Lewis_gun#History)
-   [2 Production](https://en.wikipedia.org/wiki/Lewis_gun#Production)
-   [3 Design details](https://en.wikipedia.org/wiki/Lewis_gun#Design_details)
-   [4 Service](https://en.wikipedia.org/wiki/Lewis_gun#Service)
    -   [4.1 First World War](https://en.wikipedia.org/wiki/Lewis_gun#First_World_War)
        -   [4.1.1 Aircraft use](https://en.wikipedia.org/wiki/Lewis_gun#Aircraft_use)
    -   [4.2 Second World War](https://en.wikipedia.org/wiki/Lewis_gun#Second_World_War)
-   [5 Variants](https://en.wikipedia.org/wiki/Lewis_gun#Variants)
    -   [5.1 Canada](https://en.wikipedia.org/wiki/Lewis_gun#Canada)
    -   [5.2 Czechoslovakia](https://en.wikipedia.org/wiki/Lewis_gun#Czechoslovakia)
    -   [5.3 Netherlands](https://en.wikipedia.org/wiki/Lewis_gun#Netherlands)
    -   [5.4 United Kingdom](https://en.wikipedia.org/wiki/Lewis_gun#United_Kingdom)
    -   [5.5 United States](https://en.wikipedia.org/wiki/Lewis_gun#United_States)
    -   [5.6 Experimental projects](https://en.wikipedia.org/wiki/Lewis_gun#Experimental_projects)
-   [6 Influence on later designs](https://en.wikipedia.org/wiki/Lewis_gun#Influence_on_later_designs)
-   [7 Users](https://en.wikipedia.org/wiki/Lewis_gun#Users)
-   [8 See also](https://en.wikipedia.org/wiki/Lewis_gun#See_also)
-   [9 Notes](https://en.wikipedia.org/wiki/Lewis_gun#Notes)
-   [10 References](https://en.wikipedia.org/wiki/Lewis_gun#References)
-   [11 Further reading](https://en.wikipedia.org/wiki/Lewis_gun#Further_reading)
-   [12 External links](https://en.wikipedia.org/wiki/Lewis_gun#External_links)

## History

A predecessor to the Lewis gun incorporating the principles upon which it was based was designed by [Ferdinand Mannlicher](https://en.wikipedia.org/wiki/Ferdinand_Mannlicher "Ferdinand Mannlicher").[[3]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-3) The Lewis gun was invented by [U.S. Army](https://en.wikipedia.org/wiki/U.S._Army "U.S. Army") colonel [Isaac Newton Lewis](https://en.wikipedia.org/wiki/Isaac_Newton_Lewis "Isaac Newton Lewis") in 1911, based on initial work by Samuel Maclean.[[4]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken5-4) Despite its origins, the Lewis gun was not initially adopted by the U.S. military, most likely because of political differences between Lewis and [General William Crozier](https://en.wikipedia.org/wiki/William_Crozier_(artillerist) "William Crozier (artillerist)"), the chief of the Ordnance Department.[[5]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEFord200567–68-5) Lewis became frustrated with trying to persuade the U.S. Army to adopt his design, "slapped by rejections from ignorant hacks", in his words,[[6]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford_2005,_p.68-6) and retired from the army.

Lewis left the United States in 1913 and went to Belgium, where he established the [Armes Automatique Lewis](https://en.wikipedia.org/w/index.php?title=Armes_Automatique_Lewis&action=edit&redlink=1 "Armes Automatique Lewis (page does not exist)") company in [Liège](https://en.wikipedia.org/wiki/Li%C3%A8ge "Liège") to facilitate commercial production of the gun.[[7]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEHogg1978218-7) Lewis had been working closely with British arms manufacturer the [Birmingham Small Arms Company](https://en.wikipedia.org/wiki/Birmingham_Small_Arms_Company "Birmingham Small Arms Company") Limited (BSA) in an effort to overcome some of the production difficulties of the weapon.[[4]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken5-4) The Belgians bought a small number of Lewis guns in 1913, using the [.303 British](https://en.wikipedia.org/wiki/.303_British ".303 British") round and, in 1914, BSA purchased a licence to manufacture the Lewis machine gun in England, which resulted in Lewis receiving significant [royalty payments](https://en.wikipedia.org/wiki/Royalties "Royalties") and becoming very wealthy.[[6]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford_2005,_p.68-6) Lewis and his factory moved to [England](https://en.wikipedia.org/wiki/England "England") before 1914, away from possible seizure in the event of a German invasion.[[8]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Huon1997-8)

The onset of the [First World War](https://en.wikipedia.org/wiki/World_War_I "World War I") increased demand for the Lewis gun, and BSA began production, under the designation _Model 1914_. The design was officially approved for service on 15 October 1915 under the designation "Gun, Lewis, .303-cal."[[9]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken7-9) No Lewis guns were produced in Belgium during the war;[[10]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-10) all manufacture was carried out by BSA in England and the [Savage Arms Company](https://en.wikipedia.org/wiki/Savage_Arms_Company "Savage Arms Company") in the US.[[11]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken41-11)

## Production

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Lewis_Gun_Training.jpg/220px-Lewis_Gun_Training.jpg)](https://en.wikipedia.org/wiki/File:Lewis_Gun_Training.jpg)

[U.S. Marines](https://en.wikipedia.org/wiki/United_States_Marine_Corps "United States Marine Corps") field tested the Lewis machine gun in 1917.

The Lewis was produced by BSA and Savage Arms during the war, and although the two versions were largely similar, enough differences existed to stop them being completely interchangeable, although this was rectified by the time of the [Second World War](https://en.wikipedia.org/wiki/World_War_II "World War II").[[12]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton200115,_41–46-12)

The major difference between the two designs was that the BSA weapons were chambered for .303 British ammunition, whereas the Savage guns were chambered for .30-06 cartridges, which necessitated some difference in the magazine, feed mechanism, bolt, barrel, extractors, and gas operation system.[[11]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken41-11) Savage did make Lewis guns in .303 British calibre, though. The Model 1916 and Model 1917 were exported to Canada and the United Kingdom, and a few were supplied to the US military, particularly the Navy.[[11]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken41-11) The Savage Model 1917 was generally produced in .30-06 calibre. A number of these guns were supplied to the UK under [lend-lease](https://en.wikipedia.org/wiki/Lend-lease "Lend-lease") during the Second World War.[[13]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton200141,_47-13)

## Design details

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/LewisGunParts1.jpg/220px-LewisGunParts1.jpg)](https://en.wikipedia.org/wiki/File:LewisGunParts1.jpg)

List of parts

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Magazynek_Lewisa_z_polska_amunicja_792mm.jpg/130px-Magazynek_Lewisa_z_polska_amunicja_792mm.jpg)](https://en.wikipedia.org/wiki/File:Magazynek_Lewisa_z_polska_amunicja_792mm.jpg)

A 97-round pan magazine, as used on a [7.92×57mm](https://en.wikipedia.org/wiki/8%C3%9757mm_IS "8×57mm IS") Lewis gun, Museum of Coastal Defence, Poland. Note the magazine is only partially filled.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Lewis_gun_reloading_mechanism.gif/310px-Lewis_gun_reloading_mechanism.gif)](https://en.wikipedia.org/wiki/File:Lewis_gun_reloading_mechanism.gif)

Lewis gun reloading mechanism action

The Lewis gun was [gas operated](https://en.wikipedia.org/wiki/Gas-operated_reloading "Gas-operated reloading"). A portion of the expanding propellant gas was tapped off from the barrel, driving a piston to the rear against a spring. The piston was fitted with a vertical post at its rear which rode in a helical [cam](https://en.wikipedia.org/wiki/Cam "Cam") track in the bolt, rotating it at the end of its travel nearest the breech. This allowed the three locking lugs at the rear of the bolt to engage in recesses in the gun's body to lock it into place. The post also carried a fixed [firing pin](https://en.wikipedia.org/wiki/Firing_pin "Firing pin"), which protruded through an aperture in the front of the bolt, firing the next round at the foremost part of the piston's travel.[[14]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEFord200568–70-14)[[15]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith_1943,_p.31-15)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Lewis_gun_St_Thomas_3.JPG/220px-Lewis_gun_St_Thomas_3.JPG)](https://en.wikipedia.org/wiki/File:Lewis_gun_St_Thomas_3.JPG)

A Lewis gun at the [Elgin Military Museum](https://en.wikipedia.org/wiki/Elgin_Military_Museum "Elgin Military Museum") Canada. The rear end of its light-gray finned aluminum heat sink, that fits within the gun's brass-colored cylindrical cooling shroud, can be seen

The gun's aluminum barrel-shroud caused the muzzle blast to draw air over the barrel and cool it, due to the muzzle-to-breech, radially finned aluminum [heat sink](https://en.wikipedia.org/wiki/Heat_sink "Heat sink") within the shroud's barrel, and protruding behind the shroud's aft end, running lengthwise in contact with the gun barrel (somewhat like the later American [M1917/18 Marlin-Rockwell machine gun](https://en.wikipedia.org/wiki/M1895_Colt%E2%80%93Browning_machine_gun#World_War_I "M1895 Colt–Browning machine gun")'s similar gun barrel cooling design)[[16]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-16) from the "bottleneck" near the shroud's muzzle end and protruding externally behind the shroud's rear end. Some discussion occurred over whether the shroud was really necessary—in the Second World War, many old aircraft guns that did not have the tubing were issued to [antiaircraft](https://en.wikipedia.org/wiki/Anti-aircraft_warfare "Anti-aircraft warfare") units of the [British Home Guard](https://en.wikipedia.org/wiki/British_Home_Guard "British Home Guard") and to British [airfields](https://en.wikipedia.org/wiki/Airfield "Airfield"), and others were used on vehicle mounts in the Western Desert; all were found to function properly without it, which led to the suggestion that Lewis had insisted on the cooling arrangement largely to show that his design was different from Maclean's earlier prototypes.[[17]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford70-17) Only the [Royal Navy](https://en.wikipedia.org/wiki/Royal_Navy "Royal Navy") retained the tube/heatsink cooling system on their deck-mounted AA-configuration Lewis guns.[[17]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford70-17)

The Lewis gun used a [pan magazine](https://en.wikipedia.org/wiki/Pan_magazine "Pan magazine") holding 47 or 97 rounds.[[18]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESmith194328,_32-18) Pan magazines hold the ammunition nose-inwards toward the center, in a radial fan. Unlike the more common [drum magazines](https://en.wikipedia.org/wiki/Drum_magazine "Drum magazine"), which hold the rounds parallel to the axis and are fed by spring tension, pan magazines are mechanically indexed. The Lewis magazine was driven by a cam on top of the bolt which operated a [pawl mechanism](https://en.wikipedia.org/wiki/Ratchet_(device) "Ratchet (device)") via a lever.[[15]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith_1943,_p.31-15)

An interesting point of the design was that it did not use a traditional helical coiled recoil spring, but used a spiral spring, much like a large clock spring, in a semicircular housing just in front of the trigger. The operating rod had a toothed underside, which engaged with a cog which wound the spring. When the gun fired, the bolt recoiled and the cog was turned, tightening the spring until the resistance of the spring had reached the recoil force of the bolt assembly. At that moment, as the gas pressure in the breech fell, the spring unwound, turning the cog, which, in turn, wound the operating rod forward for the next round. As with a clock spring, the Lewis gun recoil spring had an adjustment device to alter the recoil resistance for variations in temperature and wear. Unusual as it seems, the Lewis design proved reliable and was even [copied](https://en.wikipedia.org/wiki/Type_92_machine_gun "Type 92 machine gun") by the Japanese and used extensively by them during the Second World War.[[19]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESmith194331–32-19)

The gun's cyclic rate of fire was about 500–600 rounds per minute. A recoil enhancer was added to the 1918 aircraft gun variant (and refitted to many 1917 models) which increased the rate of fire to about 800 rounds per minute. The ground use versions weighed 28 lb (12.7 kg), only about half as much as a typical medium machine gun of the era, such as the [Vickers machine gun](https://en.wikipedia.org/wiki/Vickers_machine_gun "Vickers machine gun"), and was chosen in part because, being more portable than a heavy machine gun, it could be carried and used by one soldier.[[20]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEHoggBatchelor197627-20) BSA even produced at least one model (the "B.S.A. Light Infantry Pattern Lewis Gun", which lacked the aluminium barrel shroud and had a wooden fore grip) designed as a form of [assault rifle](https://en.wikipedia.org/wiki/Assault_rifle "Assault rifle").[[21]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton20014-21)

## Service

### First World War

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Lewis_gun_drill.jpg/220px-Lewis_gun_drill.jpg)](https://en.wikipedia.org/wiki/File:Lewis_gun_drill.jpg)

Men of the 28th Battalion of the 2nd Australian Division practising Lewis gun drill at [Renescure](https://en.wikipedia.org/wiki/Renescure "Renescure").

During the first days of the war, the [Belgian Army](https://en.wikipedia.org/wiki/Belgian_Army "Belgian Army") had put in service 20 prototypes (5 in [7.65×53mm](https://en.wikipedia.org/wiki/7.65%C3%9753mm_Mauser "7.65×53mm Mauser") and 15 in .303) for the [defense of Namur](https://en.wikipedia.org/wiki/Fortified_position_of_Namur#The_Namur_forts_in_1914 "Fortified position of Namur").[[22]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201411-22)

The [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom") officially adopted the Lewis gun in [.303 British](https://en.wikipedia.org/wiki/.303_British ".303 British") calibre for land and aircraft use in October 1915.[[23]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton20016-23) The weapon was generally issued to the [British Army](https://en.wikipedia.org/wiki/British_Army "British Army")'s infantry battalions on the [Western Front](https://en.wikipedia.org/wiki/Western_Front_(World_War_I) "Western Front (World War I)") in early 1916 as a replacement for the heavier and less mobile Vickers machine gun. The Vickers was withdrawn from the infantry[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] for use by specialist machine-gun companies. The [US Navy](https://en.wikipedia.org/wiki/US_Navy "US Navy") and [Marine Corps](https://en.wikipedia.org/wiki/United_States_Marine_Corps "United States Marine Corps") followed in early 1917, adopting the M1917 Lewis gun (produced by the Savage Arms Co.), in .30-06 calibre.

Notes made during his training in 1918 by Arthur Bullock, a private soldier in the 2/4th [Oxfordshire and Buckinghamshire Light Infantry](https://en.wikipedia.org/wiki/Oxfordshire_and_Buckinghamshire_Light_Infantry "Oxfordshire and Buckinghamshire Light Infantry"), record that the chief advantage of the gun was 'its invulnerability' and its chief disadvantages were 'its delicacy, the fact that it is useless for setting up a barrage, and also that the system of air cooling employed does not allow of more than 12 magazines being fired continuously'. He records its weight as 26 lbs unloaded and 30½ lbs loaded (though later he mentions that it weighed 35 lbs loaded), and that it had 47 cartridges in a fully loaded magazine; also that it was supported by a [bipod](https://en.wikipedia.org/wiki/Bipod "Bipod") in front and by the operator's shoulder at the rear.[[24]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-24) About six months into his service, Bullock was sent on Lewis gun refresher course at La Lacque, and he recalled that the rigour of the training meant that 'everyone passed out 100 percent efficient, the meaning of which will be appreciated when I say that part of the final test was to strip down the gun completely and then, blindfolded, put those 104 parts together again correctly in just one minute.'[[25]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-25)

[![](https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/Lewis_Gun_Manual_25th_Aero.jpg/220px-Lewis_Gun_Manual_25th_Aero.jpg)](https://en.wikipedia.org/wiki/File:Lewis_Gun_Manual_25th_Aero.jpg)

Lewis Gun Manual used by Sgt. Don L. Palmer of the [25th Aero Squadron](https://en.wikipedia.org/wiki/25th_Aero_Squadron "25th Aero Squadron").

The gun was operated by a team of seven. Bullock was the First Lewis Gunner who carried the gun and a revolver, while 'The Second Gunner carried a bag containing spare parts, and the remaining five members of the team carried loaded pans of ammunition'. Bullock noted, 'all could fire the gun if required, and all could effect repairs in seconds'.[[26]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-26) Bullock provides several vivid descriptions of the gun's use in combat. For example, on 13 April 1918 he and his fellow soldiers intercepted a German advance along the [Calonne](https://en.wikipedia.org/wiki/Calonne-sur-la-Lys "Calonne-sur-la-Lys")/[Robecq](https://en.wikipedia.org/wiki/Robecq "Robecq") road, noting 'we fired the gun in turns until it was too hot to hold'[[27]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-27) and recording that 400 German casualties were caused, 'chiefly by my Lewis gun!'.[[28]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-28)[[29]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-29)

The [US Army](https://en.wikipedia.org/wiki/US_Army "US Army") never officially adopted the weapon for infantry use[[17]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford70-17) and even went so far as to take Lewis guns away from US Marines arriving in France and replace them with the [Chauchat](https://en.wikipedia.org/wiki/Chauchat "Chauchat") LMG[[30]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEHoggBatchelor197630-31-30)—a practice believed to be related to [General Crozier's](https://en.wikipedia.org/wiki/William_Crozier_(artillerist) "William Crozier (artillerist)") dislike of Lewis and his gun.[[31]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEHoggBatchelor197631-31) The divisions of the US II Corps attached to the British Army were equipped with the gun.[[32]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-32) The US Army eventually adopted the [Browning Automatic Rifle](https://en.wikipedia.org/wiki/Browning_Automatic_Rifle "Browning Automatic Rifle") in 1917 (although it was September 1918 before any of the new guns reached the front).[[33]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford71-33) The US Navy and Marine Corps continued to use the .30-06 calibre Lewis until the early part of the Second World War.[[34]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith_1973,_p.270-34)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Lewis_gun_world_war_I.jpg/220px-Lewis_gun_world_war_I.jpg)](https://en.wikipedia.org/wiki/File:Lewis_gun_world_war_I.jpg)

Australian soldiers firing at enemy aircraft during the First World War

The [Russian Empire](https://en.wikipedia.org/wiki/Russian_Empire "Russian Empire") purchased 10,000 Lewis guns in 1917 from the British government, and ordered another 10,000 weapons from Savage Arms in the US. The US government was unwilling to supply the Tsarist Russian government with the guns and some doubt exists as to whether they were actually delivered, although records indicate that 5,982 Savage weapons were delivered to Russia by 31 March 1917. The Lewis guns supplied by Britain were dispatched to Russia in May 1917, but it is not known for certain whether these were the Savage-made weapons being trans-shipped through the UK, or a separate batch of UK-produced units.[[35]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton200146-35) [White armies](https://en.wikipedia.org/wiki/White_movement "White movement") in Northwest Russia received several hundred Lewis guns in 1918–1919.[[36]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-36)

British [Mark IV](https://en.wikipedia.org/wiki/Mark_I_(tank)#Mark_IV "Mark I (tank)") tanks used the Lewis, replacing the Vickers and [Hotchkiss](https://en.wikipedia.org/wiki/Hotchkiss_M1909 "Hotchkiss M1909") used in earlier tanks. The Lewis was chosen for its relatively compact magazines, but as soon as an improved magazine belt for the Hotchkiss was developed, the Lewis was replaced by them in later tank models.[[37]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGlanfield2001[[Category:Wikipedia_articles_needing_page_number_citations_from_September_2018]]<sup_class="noprint_Inline-Template_"_style="white-space:nowrap;">&#91;<i>[[Wikipedia:Citing_sources|<span_title="This_citation_requires_a_reference_to_the_specific_page_or_range_of_pages_in_which_the_material_appears.&#32;(September_2018)">page&nbsp;needed</span>]]</i>&#93;</sup>-37)

As their enemies used the mobility of the gun to ambush German raiding parties, the Germans nicknamed the Lewis "the Belgian Rattlesnake".[[38]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201464-38) They used captured Lewis guns in both World Wars, and included instruction in its operation and care as part of their machine-gun crew training.[[39]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-The_Lewis_Gun-39)

Despite costing more than a Vickers gun to manufacture (the cost of a Lewis gun was £165 in 1915[[9]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken7-9) and £175 in 1918;[[40]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-40) the Vickers cost about £100),[[33]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford71-33) Lewis machine guns were in high demand with the British military during the First World War. The Lewis also had the advantage of being about 80% faster to build than the Vickers, and was a lot more portable.[[20]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEHoggBatchelor197627-20) Accordingly, the British government placed orders for 3,052 guns between August 1914 and June 1915.[[9]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken7-9) Lewis guns outnumbered the Vickers by a ratio of about 3:1.[[33]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford71-33)

#### Aircraft use

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/ChandlerKirtlandLewisGun.jpg/220px-ChandlerKirtlandLewisGun.jpg)](https://en.wikipedia.org/wiki/File:ChandlerKirtlandLewisGun.jpg)

Captain Charles Chandler (with prototype Lewis Gun) and Lt Roy Kirtland in a [Wright Model B](https://en.wikipedia.org/wiki/Wright_Model_B "Wright Model B") Flyer after the first successful firing of a machine gun from an aeroplane in June 1912.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Sopdol.jpg/220px-Sopdol.jpg)](https://en.wikipedia.org/wiki/File:Sopdol.jpg)

No. 87 Squadron Dolphin flown by [Cecil Montgomery-Moore](https://en.wikipedia.org/wiki/Cecil_Montgomery-Moore "Cecil Montgomery-Moore"). A Lewis gun is mounted atop the lower right wing

The Lewis gun has the distinction of being the first machine gun fired from an aeroplane; on 7 June 1912, Captain [Charles Chandler](https://en.wikipedia.org/wiki/Charles_deForest_Chandler "Charles deForest Chandler") of the US Army fired a prototype Lewis gun from the foot-bar of a [Wright Model B](https://en.wikipedia.org/wiki/Wright_Model_B "Wright Model B") Flyer.[[39]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-The_Lewis_Gun-39)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Albert_Ball_SE5a_cockpit.jpg/220px-Albert_Ball_SE5a_cockpit.jpg)](https://en.wikipedia.org/wiki/File:Albert_Ball_SE5a_cockpit.jpg)

Albert Ball in an S.E.5a, showing the Foster mount's arc-shaped I-beam rail.

Lewis guns were used extensively on British and French aircraft during the First World War, as either an observer's or gunner's weapon or an additional weapon to the more common Vickers. The Lewis's popularity as an aircraft machine gun was partly due to its low weight, the fact that it was air-cooled and that it used self-contained 97-round drum magazines. Because of this, the Lewis was first mounted on the [Vickers F.B.5](https://en.wikipedia.org/wiki/Vickers_F.B.5 "Vickers F.B.5") "Gunbus", which was probably the world's first purpose-built combat aircraft when it entered service in August 1914, replacing the [Vickers machine gun](https://en.wikipedia.org/wiki/Vickers_machine_gun "Vickers machine gun") used on earlier experimental versions.[[41]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-41) It was also fitted on two early production examples of the [Bristol Scout](https://en.wikipedia.org/wiki/Bristol_Scout "Bristol Scout") C aircraft by [Lanoe Hawker](https://en.wikipedia.org/wiki/Lanoe_Hawker "Lanoe Hawker") in the summer of 1915, mounted on the port side and firing forwards and outwards at a 30° angle to avoid the propeller arc.

The problem in mounting a Lewis to fire forward in most single-engined [tractor configuration](https://en.wikipedia.org/wiki/Tractor_configuration "Tractor configuration") fighters was due to the [open bolt](https://en.wikipedia.org/wiki/Open_bolt "Open bolt") firing cycle of the Lewis, which prevented it from being [synchronized](https://en.wikipedia.org/wiki/Synchronization_gear "Synchronization gear") to fire directly forward through the propeller arc of such aircraft; only the unusual French [SPAD S.A](https://en.wikipedia.org/wiki/SPAD_S.A "SPAD S.A") "pulpit plane" which possessed a unique hinged gunner's nacelle immediately _ahead_ of the propeller (and the pilot), and the British [_pusher_](https://en.wikipedia.org/wiki/Pusher_configuration "Pusher configuration") fighters [Vickers F.B.5](https://en.wikipedia.org/wiki/Vickers_F.B.5 "Vickers F.B.5"), [Airco D.H.2](https://en.wikipedia.org/wiki/Airco_D.H.2 "Airco D.H.2"), [Royal Aircraft Factory F.E.2](https://en.wikipedia.org/wiki/Royal_Aircraft_Factory_F.E.2 "Royal Aircraft Factory F.E.2") and [F.E.8](https://en.wikipedia.org/wiki/Royal_Aircraft_Factory_F.E.8 "Royal Aircraft Factory F.E.8") could readily use the Lewis as direct forward-firing armament early in the war. Some British single-engined [_tractor_](https://en.wikipedia.org/wiki/Tractor_configuration "Tractor configuration") fighters used a [Foster mounting](https://en.wikipedia.org/wiki/Foster_mounting "Foster mounting") on the top wing to elevate a Lewis gun above the propeller arc for unsynchronized firing, including production [S.E.5/S.E.5a](https://en.wikipedia.org/wiki/Royal_Aircraft_Factory_S.E.5 "Royal Aircraft Factory S.E.5") fighters and field-modified examples of the [Avro 504](https://en.wikipedia.org/wiki/Avro_504 "Avro 504"). For the use of observers or rear gunners, the Lewis was mounted on a [Scarff ring](https://en.wikipedia.org/wiki/Scarff_ring "Scarff ring"), which allowed the gun to be rotated and elevated whilst supporting the gun's weight.[[42]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEHoggBatchelor197627,_33-42)

Until September 1916 [Zeppelin](https://en.wikipedia.org/wiki/Zeppelin "Zeppelin") airships were very difficult to attack successfully at high altitude, although this also made accurate bombing impossible. Aeroplanes struggled to reach a typical altitude of 10,000 feet (3,000 m), and firing the solid bullets usually used by aircraft Lewis guns was ineffectual: they made small holes causing inconsequential gas leaks. Britain developed new bullets, the Brock containing spontaneously igniting [potassium chlorate](https://en.wikipedia.org/wiki/Potassium_chlorate "Potassium chlorate"),[[43]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-43) and the Buckingham filled with [pyrophoric](https://en.wikipedia.org/wiki/Pyrophoricity "Pyrophoricity") [phosphorus](https://en.wikipedia.org/wiki/Phosphorus "Phosphorus"),[[44]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-44) to set fire to the Zeppelin's [hydrogen](https://en.wikipedia.org/wiki/Hydrogen "Hydrogen"). These had become available by September 1916.[[45]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-45) They proved very successful, and Lewis guns loaded with a mixture of Brock and Buckingham ammunition were often employed for balloon-busting against German Zeppelins, other airships and _Drache_ barrage balloons.[[39]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-The_Lewis_Gun-39)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Sopdol2.jpg/220px-Sopdol2.jpg)](https://en.wikipedia.org/wiki/File:Sopdol2.jpg)

1918 Sopwith Dolphin with twin Lewis guns aimed upwards.

On the French [Nieuport 11](https://en.wikipedia.org/wiki/Nieuport_11 "Nieuport 11") and later [Nieuport 17](https://en.wikipedia.org/wiki/Nieuport_17 "Nieuport 17") [sesquiplanes](https://en.wikipedia.org/wiki/Sesquiplane "Sesquiplane"), a Lewis gun was mounted above the top wing (in a similar way as fitted to the British [S.E.5a](https://en.wikipedia.org/wiki/Royal_Aircraft_Factory_S.E.5 "Royal Aircraft Factory S.E.5")) – sometimes on a [Foster mount](https://en.wikipedia.org/wiki/Foster_mounting "Foster mounting"), which allowed firing directly forward outside the [propeller](https://en.wikipedia.org/wiki/Propeller_(aircraft) "Propeller (aircraft)") arc. The Foster mount usually incorporated an arc-shaped I-beam rail as its rearmost structural member, that a Lewis gun could be slid backwards and downwards along the rail towards the cockpit, to allow the ammunition drum to be changed in flight – but [RFC](https://en.wikipedia.org/wiki/Royal_Flying_Corps "Royal Flying Corps") fighter ace [Albert Ball](https://en.wikipedia.org/wiki/Albert_Ball "Albert Ball") [VC](https://en.wikipedia.org/wiki/Victoria_Cross "Victoria Cross") also understood that the Lewis gun in such a mount also retained its original trigger, and could thus be [fired upwards](https://en.wikipedia.org/wiki/Schr%C3%A4ge_Musik#World_War_I "Schräge Musik"). He used the upward firing Lewis to attack solitary German two-seater aircraft from below and behind, where the observer could not see him or fire back. It was his use of the weapon in this way, in a Nieuport, that led to its later introduction on the S.E.5/S.E.5a: Ball had acted in a consultant capacity on the development of this aeroplane. The later [Sopwith Dolphin](https://en.wikipedia.org/wiki/Sopwith_Dolphin "Sopwith Dolphin"), already armed with twin synchronized Vickers guns just forward of the pilot and just above its [V-8 engine](https://en.wikipedia.org/wiki/Hispano-Suiza_8 "Hispano-Suiza 8"), could also use one or two Lewis guns mounted on the forward crossbar of its [cabane](https://en.wikipedia.org/wiki/Cabane_strut "Cabane strut") structure, between the top wing panels, as an anti-Zeppelin measure. A few of the Dolphins in use with [No. 87 Squadron RAF](https://en.wikipedia.org/wiki/No._87_Squadron_RAF "No. 87 Squadron RAF") in the summer of 1918, alternatively mounted their twin Lewises [atop the lower wings just inboard of the inner wing struts](https://en.wikipedia.org/wiki/Sopwith_Dolphin#Use_of_the_Lewis_guns "Sopwith Dolphin") for an additional pair of forward-firing machine guns; in such a field-achieved configuration, however, neither gun-jam clearing, nor drum magazine replacement were possible on their Lewises during a mission.

Lewis guns were also carried as defensive guns on British airships. The [SS class blimps](https://en.wikipedia.org/wiki/SS_class_blimp "SS class blimp") carried one gun. The larger [NS class blimps](https://en.wikipedia.org/wiki/NS_class_blimp "NS class blimp") carried two or three guns in the control car and some were fitted with an additional gun and a gunner's position at the top of the gasbag.[[46]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-46)

### Second World War

By the Second World War, the British Army had replaced the Lewis gun with the [Bren gun](https://en.wikipedia.org/wiki/Bren_light_machine_gun "Bren light machine gun") for most infantry use. As an airborne weapon, the Lewis was largely supplanted by the [Vickers K](https://en.wikipedia.org/wiki/Vickers_K_machine_gun "Vickers K machine gun"), a weapon that could achieve over twice the rate of fire of the Lewis.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Singapore_Volunteer_Force_training_November_1941.jpg/220px-Singapore_Volunteer_Force_training_November_1941.jpg)](https://en.wikipedia.org/wiki/File:Singapore_Volunteer_Force_training_November_1941.jpg)

Recruits of the [Singapore Volunteer Force](https://en.wikipedia.org/wiki/Straits_Settlements_Volunteer_Force "Straits Settlements Volunteer Force") training with a Lewis gun, 1941

In the crisis following the Fall of France, where a large part of the British Army's equipment had been lost up to and at Dunkirk, stocks of Lewis guns in both .303 and .30-06 were hurriedly pressed back into service, primarily for [Home Guard](https://en.wikipedia.org/wiki/Home_Guard_(United_Kingdom) "Home Guard (United Kingdom)"), airfield defence and anti-aircraft use.[[47]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton198858-47) 58,983 Lewis guns were taken from stores, repaired, refitted and issued by the British during the course of the war.[[48]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton200146–47-48) In addition to their reserve weapon role in the UK, they also saw front-line use with the Dutch, British, Australian, and New Zealand forces in the early years of the [Pacific campaign](https://en.wikipedia.org/wiki/Pacific_War "Pacific War") against the Japanese.[[49]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESkennerton20017–9-49) The Lewis gun saw continued service as an anti-aircraft weapon during the war; in this role, it was credited by the British for bringing down more low-flying enemy aircraft than any other AA weapon.[[50]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith43-50) Peter White indicates that his battalion of the King's Own Scottish Borderers was still using the Lewis on [Universal Carriers](https://en.wikipedia.org/wiki/Universal_Carriers "Universal Carriers") in 1945.[[51]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-51)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Unditch1.jpg/220px-Unditch1.jpg)](https://en.wikipedia.org/wiki/File:Unditch1.jpg)

A New Zealand-crewed [LRDG](https://en.wikipedia.org/wiki/Long_Range_Desert_Group "Long Range Desert Group") truck (equipped with a Lewis Gun) is dug out of the sand, c.1942.

At the start of the Second World War, the Lewis was the [Royal Navy](https://en.wikipedia.org/wiki/Royal_Navy "Royal Navy")'s standard close-range air defence weapon. It could be found on major warships, [armed trawlers](https://en.wikipedia.org/wiki/Armed_trawlers "Armed trawlers") and [defensively equipped merchant ships](https://en.wikipedia.org/wiki/Defensively_Equipped_Merchant_Ships "Defensively Equipped Merchant Ships"). It was often used in twin mountings and a quadruple mount was developed for [motor torpedo boats](https://en.wikipedia.org/wiki/Motor_Torpedo_Boat "Motor Torpedo Boat"). British submarines generally carried two guns on single mounts. Although it was gradually replaced by the [Oerlikon 20 mm cannon](https://en.wikipedia.org/wiki/Oerlikon_20_mm_cannon "Oerlikon 20 mm cannon"), new [corvettes](https://en.wikipedia.org/wiki/Corvette "Corvette") were still being fitted with twin Lewises as late as 1942. Lewis guns were also carried by the [Royal Air Force](https://en.wikipedia.org/wiki/Royal_Air_Force "Royal Air Force")'s [air-sea rescue launches](https://en.wikipedia.org/wiki/RAF_Rescue_Launch "RAF Rescue Launch").[[52]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-52)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Royal_Navy_Motor_Torpedo_Boats_on_patrol%2C_1940._A60.jpg/220px-Royal_Navy_Motor_Torpedo_Boats_on_patrol%2C_1940._A60.jpg)](https://en.wikipedia.org/wiki/File:Royal_Navy_Motor_Torpedo_Boats_on_patrol,_1940._A60.jpg)

A Royal Navy Motor Torpedo Boat with dual twin Lewis guns, 1940.

American forces used the Lewis gun (in .30-06 calibre) throughout the war. The [US Navy](https://en.wikipedia.org/wiki/United_States_Navy "United States Navy") used the weapon on armed merchant cruisers, small auxiliary ships, landing craft and submarines. The US Coast Guard also used the Lewis on their vessels.[[50]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith43-50) It was never officially adopted by the US Army for anything other than aircraft use.[[17]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford70-17)

The Germans used captured British Lewis guns during the war under the designation **MG 137(e)**,[[53]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEChant200147-53) whilst the Japanese copied the Lewis design and employed it extensively during the war;[[50]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith43-50) it was designated the **[Type 92](https://en.wikipedia.org/wiki/Type_92_machine_gun "Type 92 machine gun")** and chambered for a [7.7 mm rimmed cartridge](https://en.wikipedia.org/wiki/.303_British#Japanese_7.7_mm_ammunition ".303 British") that was interchangeable with the .303 British round.[[54]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESmith1973512-54)[[55]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTESmith1943131-55)

The Lewis was officially withdrawn from British service in 1946,[[33]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford71-33) but continued to be used by forces operating against the United Nations in the [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War"). It was also used against French and US forces in the [First Indochina War](https://en.wikipedia.org/wiki/First_Indochina_War "First Indochina War") and the subsequent [Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War").[[56]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken9-56)

Total production of the Lewis gun during the Second World War by BSA was over 145,000 units,[[17]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Ford70-17) a total of 3,550 guns were produced by the Savage Arms Co. for US service—2,500 in .30-06 and 1,050 in .303 British calibre.[[34]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith_1973,_p.270-34)

## Variants

### Canada

-   **Model 1915**. This was the designation given to .303 Lewis Mk I weapons manufactured for [Canada](https://en.wikipedia.org/wiki/Canada "Canada") in the [United States](https://en.wikipedia.org/wiki/United_States "United States") by the [Savage Arms Company](https://en.wikipedia.org/wiki/Savage_Arms_Company "Savage Arms Company"). Large numbers of these guns were also produced by Savage for the [British Army](https://en.wikipedia.org/wiki/British_Army "British Army") and in an aircraft configuration, for [France](https://en.wikipedia.org/wiki/France "France") and [Italy](https://en.wikipedia.org/wiki/Italy "Italy").[[57]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201416-57)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Leteck%C3%A9_muzeum_Kbely_%289%29.jpg/220px-Leteck%C3%A9_muzeum_Kbely_%289%29.jpg)](https://en.wikipedia.org/wiki/File:Leteck%C3%A9_muzeum_Kbely_(9).jpg)

Czech Vz 28/L, chambered for the 7.92×57mm Mauser ammunition.

### Czechoslovakia

-   **Vz 28/L**. 731 7.92×57mm Lewis guns formerly used by the [Czechoslovakian](https://en.wikipedia.org/wiki/First_Czechoslovak_Republic "First Czechoslovak Republic") infantry were modified to aircraft (or anti-aircraft) machine guns by [Česká zbrojovka Strakonice](https://en.wikipedia.org/wiki/%C4%8Cesk%C3%A1_zbrojovka_Strakonice "Česká zbrojovka Strakonice").[[58]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Czech-58)

### Netherlands

-   **Mitrailleur M. 20**. In the [Netherlands](https://en.wikipedia.org/wiki/Netherlands "Netherlands"), the Lewis in both ground and aircraft versions was used in [6.5×53 mm R](https://en.wikipedia.org/wiki/6.5%C3%9753mmR "6.5×53mmR") calibre, using a 97-round magazine only.[[59]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-59) The infantry version was equipped with a carrying handle on a clamp around the rear of the cooling tube. After the [German invasion of May 1940](https://en.wikipedia.org/wiki/Battle_of_the_Netherlands "Battle of the Netherlands"), the weapon was also used by Germany under the designation _6,5 mm leichtes Maschinengewehr 100 (h)_.[[60]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-60) This Dutch modification of the older BSA redesign would have been extremely simple, as the Dutch/Romanian 6.5mm Mannlicher round has very nearly the same critical dimensions of the case head and rim as .303" British.

### United Kingdom

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/The_Home_Guard_1939-45_H14697.jpg/220px-The_Home_Guard_1939-45_H14697.jpg)](https://en.wikipedia.org/wiki/File:The_Home_Guard_1939-45_H14697.jpg)

A British [Home Guard](https://en.wikipedia.org/wiki/Home_Guard_(United_Kingdom) "Home Guard (United Kingdom)") platoon in 1941. The soldier on the right is carrying either a Lewis Mk III* or Mk III** with the improvised skeleton stock and fore-stock to make it usable as a ground weapon. The man next to him is carrying the drum magazine.

-   **Mark I**. The .303 Lewis Mk I was the basic ground pattern model used by [British](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom") and [British Empire](https://en.wikipedia.org/wiki/British_Empire "British Empire") forces from 1915 with few improvements.[[61]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201414-15-61)
-   **Mark II**. This was the first purpose built aircraft version of the Lewis, earlier versions had been improvised from Mk I guns. The cooling fins were omitted to save weight, but a light protective shroud around the barrel was retained. The wooden [stock](https://en.wikipedia.org/wiki/Stock_(firearms) "Stock (firearms)") was removed and replaced with a "spade" grip, which resembled the handle of a garden [spade](https://en.wikipedia.org/wiki/Spade "Spade"). A 97-round drum magazine was introduced which required a larger magazine spigot on the body of the gun.
-   **Mark II***. An improved Mk II with an increased rate of fire introduced in 1918.
-   **Mark III**. A further upgrade of the Mk II with an even faster rate of fire and the barrel shroud deleted, introduced later in 1918.[[62]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201417-18-62)
-   **Mark III***. The [British](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom") designation for the US .30-06 M1918 aircraft gun, some 46,000 of which were imported for the use of the Home Guard in 1940. These guns were modified for ground use by the replacement of the spade grip with a crude [skeleton stock](https://en.wikipedia.org/wiki/Skeleton_stock "Skeleton stock") and the addition of a simple wooden fore-stock which would allow the gun to be fired while resting on a sandbag, or from the hip while advancing.
-   **Mark III****. The designation for the .303 Mark III modified in the same way as the US M1918s.
-   **Mark III DEMS**. Intended for [Defensively Equipped Merchant Ships](https://en.wikipedia.org/wiki/Defensively_Equipped_Merchant_Ship "Defensively Equipped Merchant Ship") (DEMS), it was similar to the Mk III** but with the addition of a [pistol grip](https://en.wikipedia.org/wiki/Pistol_grip "Pistol grip") on the fore-stock, so that the weapon could be fired free-standing from the shoulder, from any part of a ship's decks.
-   **Mark IV**. After all the usable weapons had been reconditioned and issued, there remained a large number of incomplete Lewis guns and spare parts. These were assembled into guns similar to the Mk III**. There was a particular shortage of the fragile "clock" springs for the Lewis, so a simpler spring was manufactured and housed in a straight tube which extended into the skeleton stock. Many of these guns were fitted with a simple and light tripod which had been specially produced.[[63]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201463-63)

### United States

-   **M1917 Lewis**. Savage produced a version of the Lewis Mk I for US forces, rechambered for the [.30-06](https://en.wikipedia.org/wiki/.30-06 ".30-06") round and with a modified gas operation due to the greater power of the US [ammunition](https://en.wikipedia.org/wiki/Ammunition "Ammunition"). A few of these were modified for [aircraft](https://en.wikipedia.org/wiki/Aircraft "Aircraft") use, when intended for [non-synchronized](https://en.wikipedia.org/wiki/Synchronization_gear "Synchronization gear") emplacements on an airframe. The [US Navy](https://en.wikipedia.org/wiki/US_Navy "US Navy") designation was **Lewis Mark IV**.
-   **M1918 Lewis**. A purpose built [aircraft](https://en.wikipedia.org/wiki/Aircraft "Aircraft") version of the M1917.

### Experimental projects

A commercial venture in 1921 by the Birmingham Small Arms Company was a version which fired the [12.7×81mm](https://en.wikipedia.org/w/index.php?title=12.7%C3%9781mm&action=edit&redlink=1 "12.7×81mm (page does not exist)") (0.5-inch Vickers) ammunition, intended for use against aircraft and tanks. At around the same time, BSA developed the Light Infantry Model which had a 22-round magazine and a wooden fore-stock in place of the radiator fins and shroud; it was intended to be used in a similar way to the [Browning Automatic Rifle](https://en.wikipedia.org/wiki/Browning_Automatic_Rifle "Browning Automatic Rifle"). Another development was a twin Lewis for aircraft use in which the bodies of the two weapons were joined side-by-side and the drum magazines were mounted vertically, one on each side. None of these projects was accepted by any armed forces.[[64]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201426-64)

Lewis had also experimented with lighter, 30-06 calibre, box magazine-fed infantry rifle variants intended for shoulder or hip fire as a competition to the BAR. They were dubbed **"Assault Phase Rifle"** – what could be understood as the first use of the term "Assault Rifle", despite the weapon being, by today's designation, a battle rifle. Despite being three pounds lighter than it and loaded with very forward-thinking features for the time (such as an [ambidexterous](https://en.wikipedia.org/wiki/Ambidexterous "Ambidexterous") magazine release), the U.S. Army still chose to adopt the BAR.[[65]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-65)

A short-barrelled light machine gun variant was developed at the start of the [Second World War](https://en.wikipedia.org/wiki/Second_World_War "Second World War"). It came with a hand guard and was fed from a 30-round [Bren](https://en.wikipedia.org/wiki/Bren "Bren") magazines; however, it was decided by the British authorities to concentrate production on the [Bren](https://en.wikipedia.org/wiki/Bren "Bren"), which had the advantage of a changeable barrel.[[66]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201442-66)

## Influence on later designs

The German [FG 42](https://en.wikipedia.org/wiki/FG_42 "FG 42") paratrooper's rifle used the Lewis gun's gas assembly and bolt design which were in turn incorporated into the [M60 machine gun](https://en.wikipedia.org/wiki/M60_machine_gun "M60 machine gun").[[56]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken9-56)

The [Type 92 machine gun](https://en.wikipedia.org/wiki/Type_92_machine_gun "Type 92 machine gun"), the standard hand-held machine gun used by [Imperial Japanese Navy](https://en.wikipedia.org/wiki/Imperial_Japanese_Navy "Imperial Japanese Navy") aircraft gunners in WWII, was essentially a copy of the Lewis gun.[[50]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Smith43-50)

The Russian [PKP Pecheneg](https://en.wikipedia.org/wiki/PKP_Pecheneg "PKP Pecheneg") general purpose machine gun uses a streamlined version of the Lewis gun's forced air cooling in a fixed heavy barrel. This enables the Pecheneg to fire more than 600 rounds through the barrel without warping, in contrast to [NATO](https://en.wikipedia.org/wiki/NATO "NATO") machine guns which require removable barrels to maintain suppressive fire.[[67]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-67)

## Users

-   [![Algeria](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Algeria.svg/23px-Flag_of_Algeria.svg.png)](https://en.wikipedia.org/wiki/Algeria "Algeria")[Armée de Libération Nationale](https://en.wikipedia.org/wiki/Arm%C3%A9e_de_Lib%C3%A9ration_Nationale "Armée de Libération Nationale") guerrillas[[68]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-68)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/23px-Flag_of_Australia_%28converted%29.svg.png) [Australia](https://en.wikipedia.org/wiki/Australia "Australia")[[69]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201439-69)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Flag_of_Barbados_%281870%E2%80%931966%29.svg/23px-Flag_of_Barbados_%281870%E2%80%931966%29.svg.png) [Barbados](https://en.wikipedia.org/wiki/Barbados "Barbados")[[70]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-70)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_Belgium_%28civil%29.svg/23px-Flag_of_Belgium_%28civil%29.svg.png) [Belgium](https://en.wikipedia.org/wiki/Belgium "Belgium")[[22]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201411-22)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Flag_of_Bermuda_%281910%E2%80%931999%29.svg/23px-Flag_of_Bermuda_%281910%E2%80%931999%29.svg.png) [Bermuda](https://en.wikipedia.org/wiki/Bermuda "Bermuda"): [Bermuda Volunteer Rifle Corps](https://en.wikipedia.org/wiki/Bermuda_Volunteer_Rifle_Corps "Bermuda Volunteer Rifle Corps")[[71]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924126-71)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Flag_of_Biafra.svg/23px-Flag_of_Biafra.svg.png) [Biafra](https://en.wikipedia.org/wiki/Biafra "Biafra")[[72]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-72)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Bolivia.svg/22px-Flag_of_Bolivia.svg.png) [Bolivia](https://en.wikipedia.org/wiki/Bolivia "Bolivia")[[73]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Chaco-73)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Flag_of_Hong_Kong_%281959%E2%80%931997%29.svg/23px-Flag_of_Hong_Kong_%281959%E2%80%931997%29.svg.png) [British Hong Kong](https://en.wikipedia.org/wiki/British_Hong_Kong "British Hong Kong")[[74]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924181-74)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/British_Raj_Red_Ensign.svg/23px-British_Raj_Red_Ensign.svg.png) [British India](https://en.wikipedia.org/wiki/British_Raj "British Raj")[[75]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-75)
-   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_the_Federated_Malay_States_%281895%E2%80%931946%29.svg/23px-Flag_of_the_Federated_Malay_States_%281895%E2%80%931946%29.svg.png) [British Malaya](https://en.wikipedia.org/wiki/British_Malaya "British Malaya")[[76]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924185_187-76)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Flag_of_Canada_%281868%E2%80%931921%29.svg/23px-Flag_of_Canada_%281868%E2%80%931921%29.svg.png) [Canada](https://en.wikipedia.org/wiki/Canada "Canada")[[77]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201435-77)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Flag_of_Ceylon_%281875%E2%80%931948%29.svg/23px-Flag_of_Ceylon_%281875%E2%80%931948%29.svg.png) [Ceylon](https://en.wikipedia.org/wiki/British_Ceylon "British Ceylon")[[78]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924179-78)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Flag_of_the_Czech_Republic.svg/23px-Flag_of_the_Czech_Republic.svg.png) [Czechoslovakia](https://en.wikipedia.org/wiki/Czechoslovakia "Czechoslovakia")[[58]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Czech-58)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Flag_of_Estonia.svg/23px-Flag_of_Estonia.svg.png) [Estonia](https://en.wikipedia.org/wiki/Estonia "Estonia")[[79]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-79) Kept in reserve in 1940.[[80]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-80)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Fiji.svg/23px-Flag_of_Fiji.svg.png) [Fiji](https://en.wikipedia.org/wiki/Fiji "Fiji")[[81]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924196-81)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/23px-Flag_of_Finland.svg.png) [Finland](https://en.wikipedia.org/wiki/Finland "Finland"): Used as _7,62 pk/Lewis_ and _7,70 pk/Lewis_[[82]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-82)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/23px-Flag_of_France.svg.png) [France](https://en.wikipedia.org/wiki/France "France")[[83]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201444-83)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Flag_of_Georgia_%281918%E2%80%931921%29.svg/23px-Flag_of_Georgia_%281918%E2%80%931921%29.svg.png) [Democratic Republic of Georgia](https://en.wikipedia.org/wiki/Democratic_Republic_of_Georgia "Democratic Republic of Georgia") (Mostly People's Guard and some army units in 1918–1921)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_Japan_%281870%E2%80%931999%29.svg/22px-Flag_of_Japan_%281870%E2%80%931999%29.svg.png) [Empire of Japan](https://en.wikipedia.org/wiki/Empire_of_Japan "Empire of Japan")[[83]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201444-83)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Flag_of_Germany_%281867%E2%80%931918%29.svg/23px-Flag_of_Germany_%281867%E2%80%931918%29.svg.png) [German Empire](https://en.wikipedia.org/wiki/German_Empire "German Empire") (Which found it much lighter than the MG08/15)[[84]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201465-84)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Flag_of_British_Guiana_%281955%E2%80%931966%29.svg/23px-Flag_of_British_Guiana_%281955%E2%80%931966%29.svg.png) [Guiana](https://en.wikipedia.org/wiki/British_Guiana "British Guiana")[[85]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924128-85)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Flag_of_Ireland.svg/23px-Flag_of_Ireland.svg.png) [Ireland](https://en.wikipedia.org/wiki/Republic_of_Ireland "Republic of Ireland")[[63]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201463-63)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/21px-Flag_of_Israel.svg.png) [Israel](https://en.wikipedia.org/wiki/Israel "Israel")[[38]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201464-38)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Flag_of_Italy_%281861-1946%29_crowned.svg/23px-Flag_of_Italy_%281861-1946%29_crowned.svg.png) [Italy](https://en.wikipedia.org/wiki/Kingdom_of_Italy "Kingdom of Italy"): infantry variant, modified to be used on aircraft[[86]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201416-18-86)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Flag_of_Yugoslavia_%281918%E2%80%931941%29.svg/23px-Flag_of_Yugoslavia_%281918%E2%80%931941%29.svg.png) [Kingdom of Yugoslavia](https://en.wikipedia.org/wiki/Kingdom_of_Yugoslavia "Kingdom of Yugoslavia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Flag_of_Latvia.svg/23px-Flag_of_Latvia.svg.png) [Latvia](https://en.wikipedia.org/wiki/Latvia "Latvia"): standard LMG during [Latvian War of Independence](https://en.wikipedia.org/wiki/Latvian_War_of_Independence "Latvian War of Independence") and interwar period.[[56]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken9-56) Used by [Latvian Police Battalions](https://en.wikipedia.org/wiki/Latvian_Police_Battalions "Latvian Police Battalions") of WW2.[[87]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-87)
-   [![Mauritius](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Flag_of_Mauritius_%281923%E2%80%931968%29.svg/23px-Flag_of_Mauritius_%281923%E2%80%931968%29.svg.png)](https://en.wikipedia.org/wiki/Mauritius "Mauritius") [Mauritius](https://en.wikipedia.org/wiki/British_Mauritius "British Mauritius")[[88]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924193-88)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Flag_of_Mexico_%281893-1916%29.svg/23px-Flag_of_Mexico_%281893-1916%29.svg.png) [Mexico](https://en.wikipedia.org/wiki/Mexico "Mexico")[[89]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-89)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Germany_%281935%E2%80%931945%29.svg/23px-Flag_of_Germany_%281935%E2%80%931945%29.svg.png) [Nazi Germany](https://en.wikipedia.org/wiki/Nazi_Germany "Nazi Germany")[[84]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201465-84)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/23px-Flag_of_the_Netherlands.svg.png) [Netherlands](https://en.wikipedia.org/wiki/Netherlands "Netherlands")[[90]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201463-64-90)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/23px-Flag_of_New_Zealand.svg.png) [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand")[[91]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-91)[[92]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-92)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Flag_of_Nicaragua_%281908%E2%80%931971%29.svg/23px-Flag_of_Nicaragua_%281908%E2%80%931971%29.svg.png) [Nicaragua](https://en.wikipedia.org/wiki/Nicaragua "Nicaragua")[[93]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-93)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Norway.svg/21px-Flag_of_Norway.svg.png) [Norway](https://en.wikipedia.org/wiki/Norway "Norway"): manufactured before WWI[[94]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Segel-94)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Flag_of_North_Borneo_%281902%E2%80%931946%29.svg/23px-Flag_of_North_Borneo_%281902%E2%80%931946%29.svg.png) [North Borneo](https://en.wikipedia.org/wiki/North_Borneo "North Borneo")[[95]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924177-95)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Flag_of_Northern_Rhodesia_%281939%E2%80%931964%29.svg/23px-Flag_of_Northern_Rhodesia_%281939%E2%80%931964%29.svg.png) [Northern Rhodesia](https://en.wikipedia.org/wiki/Northern_Rhodesia "Northern Rhodesia")[[96]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924156-96)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Flag_of_the_Philippines.svg/23px-Flag_of_the_Philippines.svg.png) [Philippines](https://en.wikipedia.org/wiki/Philippines "Philippines")[[97]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201476-97)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/1/12/Flag_of_Poland.svg/23px-Flag_of_Poland.svg.png) [Poland](https://en.wikipedia.org/wiki/Poland "Poland")[[63]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201463-63)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/23px-Flag_of_Portugal.svg.png) [Portugal](https://en.wikipedia.org/wiki/Portugal "Portugal")[[57]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201416-57)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Flag_of_China_%281912%E2%80%931928%29.svg/23px-Flag_of_China_%281912%E2%80%931928%29.svg.png) [Republic of China (1912–1949)](https://en.wikipedia.org/wiki/Republic_of_China_(1912%E2%80%931949) "Republic of China (1912–1949)"): used by [warlord armies](https://en.wikipedia.org/wiki/Warlord_Era "Warlord Era")[[98]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-98)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Flag_of_Romania.svg/23px-Flag_of_Romania.svg.png) [Romania](https://en.wikipedia.org/wiki/Romania "Romania")[[99]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-99)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/23px-Flag_of_Russia.svg.png) [Russian Empire](https://en.wikipedia.org/wiki/Russian_Empire "Russian Empire")[[38]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201464-38)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Flag_of_British_Somaliland_%281952%E2%80%931960%29.svg/23px-Flag_of_British_Somaliland_%281952%E2%80%931960%29.svg.png) [Somaliland](https://en.wikipedia.org/wiki/British_Somaliland "British Somaliland"): [Somaliland Camel Corps](https://en.wikipedia.org/wiki/Somaliland_Camel_Corps "Somaliland Camel Corps")[[100]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924172-100)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Flag_of_Southern_Rhodesia_%281924%E2%80%931964%29.svg/23px-Flag_of_Southern_Rhodesia_%281924%E2%80%931964%29.svg.png) [Southern Rhodesia](https://en.wikipedia.org/wiki/Southern_Rhodesia "Southern Rhodesia")[[101]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTELeague_of_Nations1924173-101)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Flag_of_the_Soviet_Union_%281924%E2%80%931955%29.svg/23px-Flag_of_the_Soviet_Union_%281924%E2%80%931955%29.svg.png) [Soviet Union](https://en.wikipedia.org/wiki/Soviet_Union "Soviet Union")[[38]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201464-38)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Flag_of_Spain_%281931%E2%80%931939%29.svg/23px-Flag_of_Spain_%281931%E2%80%931939%29.svg.png) [Spanish Republic](https://en.wikipedia.org/wiki/Second_Spanish_Republic "Second Spanish Republic")[[102]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-102)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Flag_of_Tibet.svg/23px-Flag_of_Tibet.svg.png) [Tibet](https://en.wikipedia.org/wiki/Tibet "Tibet")[[103]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-103)
-   [![Jordan](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Flag_of_Jordan.svg/23px-Flag_of_Jordan.svg.png)](https://en.wikipedia.org/wiki/Jordan "Jordan") [Transjordan](https://en.wikipedia.org/wiki/Jordan "Jordan")[[90]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-FOOTNOTEGrant201463-64-90)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/23px-Flag_of_the_United_Kingdom.svg.png) [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom") & [British Empire](https://en.wikipedia.org/wiki/British_Empire "British Empire")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Flag_of_the_United_States_%281912-1959%29.svg/23px-Flag_of_the_United_States_%281912-1959%29.svg.png) [United States](https://en.wikipedia.org/wiki/United_States "United States")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Vietnam.svg/23px-Flag_of_Vietnam.svg.png) [Vietnam](https://en.wikipedia.org/wiki/Vietnam "Vietnam")[[56]](https://en.wikipedia.org/wiki/Lewis_gun#cite_note-Sken9-56)
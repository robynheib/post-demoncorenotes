# [Enfield No2 Mk1](https://en.wikipedia.org/wiki/Enfield_No._2#mw-head)

<img src="https://i.ytimg.com/vi/G2sLd8FJRXM/maxresdefault.jpg" alt="thum" width="500"/>

[Enfield No2 Mk1 shooting](https://www.youtube.com/watch?v=G2sLd8FJRXM)



From Wikipedia, the free encyclopedia

Enfield No 2 Mk I Revolver

[![Enfield-No2.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Enfield-No2.jpg/300px-Enfield-No2.jpg)](https://en.wikipedia.org/wiki/File:Enfield-No2.jpg)

An Enfield No.2 manufactured in 1940

Type

[Service](https://en.wikipedia.org/wiki/Military_service "Military service") [revolver](https://en.wikipedia.org/wiki/Revolver "Revolver")

Place of origin

United Kingdom

Service history

In service

1932–1963

Used by

[United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom_of_Great_Britain_%26_Ireland "United Kingdom of Great Britain & Ireland") & [Colonies](https://en.wikipedia.org/wiki/British_Empire "British Empire")  
[British Commonwealth](https://en.wikipedia.org/wiki/Commonwealth_of_Nations "Commonwealth of Nations")  
[other countries](https://en.wikipedia.org/wiki/Enfield_No._2#Users)

Wars

[World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")  
[Indonesian National Revolution](https://en.wikipedia.org/wiki/Indonesian_National_Revolution "Indonesian National Revolution")  
[Malayan Emergency](https://en.wikipedia.org/wiki/Malayan_Emergency "Malayan Emergency")  
[Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War")  
[British colonial conflicts](https://en.wikipedia.org/wiki/British_Empire "British Empire")  
[Northern Campaign](https://en.wikipedia.org/wiki/Northern_Campaign_(Irish_Republican_Army) "Northern Campaign (Irish Republican Army)")  
[Border Campaign](https://en.wikipedia.org/wiki/Border_Campaign_(Irish_Republican_Army) "Border Campaign (Irish Republican Army)")  
[The Troubles](https://en.wikipedia.org/wiki/The_Troubles "The Troubles")

Production history

Designer

[Royal Small Arms Factory](https://en.wikipedia.org/wiki/Royal_Small_Arms_Factory "Royal Small Arms Factory"), [Webley & Scott](https://en.wikipedia.org/wiki/Webley_%26_Scott "Webley & Scott")

Designed

1928

Manufacturer

[RSAF Enfield](https://en.wikipedia.org/wiki/Royal_Small_Arms_Factory "Royal Small Arms Factory")

Produced

1932–1957

No. built

approx 270,000

Variants

Enfield No 2 Mk I*, Enfield No 2 Mk I**

Specifications

Mass

1.7 lb (765 g), unloaded

Length

10.25 in (260 mm)

[Barrel](https://en.wikipedia.org/wiki/Gun_barrel "Gun barrel") length

5.03 in (127 mm)

---

[Cartridge](https://en.wikipedia.org/wiki/Cartridge_(firearms) "Cartridge (firearms)")

[.380" Revolver](https://en.wikipedia.org/wiki/.38/200 ".38/200") Mk I or Mk IIz

[Calibre](https://en.wikipedia.org/wiki/Caliber "Caliber")

0.38 inch (9.65 mm)

[Action](https://en.wikipedia.org/wiki/Action_(firearms) "Action (firearms)")

Double-action revolver (Mk I* and Mk I** double action only)

[Rate of fire](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire")

20–30 rounds/minute

[Muzzle velocity](https://en.wikipedia.org/wiki/Muzzle_velocity "Muzzle velocity")

620 ft/s (189 m/s)

Effective firing range

15 yards (13.7 m)

Maximum firing range

200 yd (182.8 m)

Feed system

6-round cylinder

Sights

fixed front post and rear notch

The **Enfield No. 2** was a British top-break revolver using the [.38 S&W](https://en.wikipedia.org/wiki/.38_S%26W ".38 S&W") round manufactured from 1930 to 1957. It was the standard British/Commonwealth sidearm in the [Second World War](https://en.wikipedia.org/wiki/World_War_II "World War II"), alongside the [Webley Mk IV](https://en.wikipedia.org/wiki/Webley_revolver "Webley revolver") and [Smith & Wesson Victory Model](https://en.wikipedia.org/wiki/Smith_%26_Wesson_Victory_Model "Smith & Wesson Victory Model") revolvers chambered in the same calibre.

## Contents

-   [1 History](https://en.wikipedia.org/wiki/Enfield_No._2#History)
-   [2 Variants](https://en.wikipedia.org/wiki/Enfield_No._2#Variants)
-   [3 Ammunition](https://en.wikipedia.org/wiki/Enfield_No._2#Ammunition)
-   [4 Other manufacturers](https://en.wikipedia.org/wiki/Enfield_No._2#Other_manufacturers)
-   [5 Users](https://en.wikipedia.org/wiki/Enfield_No._2#Users)
-   [6 See also](https://en.wikipedia.org/wiki/Enfield_No._2#See_also)
-   [7 Notes](https://en.wikipedia.org/wiki/Enfield_No._2#Notes)
-   [8 References](https://en.wikipedia.org/wiki/Enfield_No._2#References)
-   [9 External links](https://en.wikipedia.org/wiki/Enfield_No._2#External_links)

## History

After the First World War, it was decided by the British Government that a smaller and lighter .38 calibre (9.2 mm) sidearm firing a long, heavy 200 grain (13 g) soft lead bullet would be preferable to the large Webley service revolvers using the [.455 (11.6 mm) round](https://en.wikipedia.org/wiki/.455_Webley ".455 Webley").[[1]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-1)[[2]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Smith,_W.H.B_page_11-2) While the .455 had proven to be an effective weapon for stopping enemy soldiers, the recoil of the .455 cartridge complicated marksmanship training.[[3]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-3) The authorities began a search for a double-action revolver with less weight and recoil that could be quickly mastered by a minimally trained soldier,[[4]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-4) with a good probability of hitting an enemy with the first shot at extremely close ranges.[[5]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-5) By using a long, heavy, round-nosed lead bullet in a .38 calibre cartridge, it was found that the bullet, being minimally stabilised for its weight and calibre, tended to 'keyhole' or tumble longitudinally when striking an object, theoretically increasing wounding and stopping ability of human targets at short ranges.[[6]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-6)[[7]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Barnes,_Frank_C._1989_p._239-7) At the time, the .38 calibre Smith & Wesson cartridge with 200-grain (13 g) lead bullet, known as the [.38/200](https://en.wikipedia.org/wiki/.38/200 ".38/200"), was also a popular cartridge in civilian and police use (in the USA, the .38/200 or 380/200 was known as the ".38 Super Police" load).[[7]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Barnes,_Frank_C._1989_p._239-7) Consequently, the British firm of [Webley & Scott](https://en.wikipedia.org/wiki/Webley_%26_Scott "Webley & Scott") tendered their [Webley Mk IV revolver](https://en.wikipedia.org/wiki/Webley_Revolver "Webley Revolver") in .38/200 calibre.[[8]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-8) Rather than adopting it, the British authorities took the design to the Government-run Royal Small Arms Factory at [Enfield](https://en.wikipedia.org/wiki/London_Borough_of_Enfield "London Borough of Enfield"), and the Enfield factory came up with a revolver that was very similar to the Webley Mk IV .38, but internally slightly different. The Enfield-designed pistol was quickly accepted under the designation "Revolver, No 2 Mk I" (single/double action, with a hammer spur), and was adopted in 1931,[[9]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-9) followed in 1938 by the Mk I* (lightened trigger pull, spurless hammer, double-action only),[[10]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-§_B2289,_LoC-10) and finally the Mk I** (simplified for wartime production) in 1942.[[11]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-11)

Webley sued the British Government for £2,250, being "costs involved in the research and design" of the revolver. Their action was contested by Enfield, who stated that the Enfield No 2 Mk I was actually designed by Captain Boys (the Assistant Superintendent of Design, after whom the [Boys Rifle](https://en.wikipedia.org/wiki/Rifle,_Anti-Tank,_.55_in,_Boys "Rifle, Anti-Tank, .55 in, Boys") was named) with assistance from Webley & Scott, and not the other way around—accordingly, their claim was denied. By way of compensation, however, the Royal Commission on Awards to Inventors awarded Webley & Scott £1,250.[[12]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-12)

RSAF Enfield was unable to manufacture enough No. 2 revolvers to meet the military's wartime demands, and as a result Webley's Mk IV was issued as a substitute standard for the British Army.

## Variants

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Enfield_No._2_Mk_1_1938.jpg/220px-Enfield_No._2_Mk_1_1938.jpg)](https://en.wikipedia.org/wiki/File:Enfield_No._2_Mk_1_1938.jpg)

Enfield No. 2 Mk 1 mfg 1938 in original condition, with hammer spur and original wood grips. Operates both single and double action.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Enfieldbench25.jpg/220px-Enfieldbench25.jpg)](https://en.wikipedia.org/wiki/File:Enfieldbench25.jpg)

An Enfield No. 2 Mk1 in fine condition. From bench rest at 25 yards.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/7_yards.jpg/220px-7_yards.jpg)](https://en.wikipedia.org/wiki/File:7_yards.jpg)

One hand rapid fire group with Enfield 38/200

There were two main variants of the Enfield No 2 Mk I revolver. The first was the **Mk I***, which had a spurless hammer and was [double-action only](https://en.wikipedia.org/wiki/Double-action_only "Double-action only"), meaning that the hammer could not be thumb-cocked by the shooter for each shot. Additionally, in keeping with the revolver's purpose as a close-range weapon, the handgrips, now made of plastic, were redesigned to improve grip when used in rapid double-action fire; the new handgrip design was given the designation Mk II.[[13]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Dunlap,_Roy_1948_p._141-13) The majority of Enfields produced were either Mk I* or modified to that standard.[[14]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Weeks,_John_1979_p._76-14) The second variant was the **Mk I****, which was a 1942 variant of the Mk I* simplified in order to increase production, but was discontinued shortly thereafter as a result of safety concerns over some of the introduced modifications.

The vast majority of Enfield No 2 Mk I revolvers were modified to Mk I* during World War II, generally as they came in for repair or general maintenance;[[10]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-§_B2289,_LoC-10) It is believed that the reason was that the Mk I* version was cheaper and faster to manufacture and allowed for far quicker training.[[15]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-15) When used in the manner in which British forces trained (rapid double-action fire at very close ranges), the No 2 Mk I* is at least as accurate as any other service pistol of its time, because of the relatively light double-action trigger pull. It is not, however, the best choice for deliberately aimed, long-distance shooting — the double-action pull will throw the most competent shooter's aim off enough to noticeably affect accuracy at ranges of more than 15 yards (14 m) or so.[[2]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Smith,_W.H.B_page_11-2) Despite officially being declared obsolete at the end of World War II, the Enfield (and Webley revolvers) were not completely phased out in favour of the [Browning Hi-Power](https://en.wikipedia.org/wiki/Browning_Hi-Power "Browning Hi-Power") until April 1969.[[16]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-16)

The Enfield No 2 is very fast to reload—as are all British [top-break](https://en.wikipedia.org/wiki/Top-break#Top_break "Top-break") revolvers—because of its automatic ejector, which simultaneously removes all six cases from the cylinder. British combat experience with the .38/200 Enfield revolvers during World War II seemed to confirm that, "for the average soldier", the Enfield No. 2 Mk I could be used far more effectively than the bulkier and heavier .455 calibre Webley revolvers that had been issued during World War I.[[2]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Smith,_W.H.B_page_11-2) Perhaps because of the relatively long double-action trigger pull compared to other pistols capable of single-action fire,[[14]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Weeks,_John_1979_p._76-14) the double-action-only Mk I* revolvers were not popular with troops,[[14]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Weeks,_John_1979_p._76-14) many of whom took the first available opportunity to exchange them in favour of [Smith & Wesson](https://en.wikipedia.org/wiki/Smith_%26_Wesson_Model_10 "Smith & Wesson Model 10"), [Colt](https://en.wikipedia.org/wiki/Colt_Official_Police "Colt Official Police"), or [Webley](https://en.wikipedia.org/wiki/Webley_Revolver "Webley Revolver") revolvers.[[17]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-17)

## Ammunition

The Enfield No.2 Mk I was designed for use with the Cartridge S.A. Ball Revolver .380 inch Mk. I and Mk. Iz, a deviation of the .38 Smith & Wesson cartridge, also known as the .38/200. It had a 200 gr (13 g). unjacketed round-nose, lead bullet of .359 inch diameter that developed a muzzle velocity of 620–650 ft/s (190–200 m/s).

Just prior to the outbreak of the Second World War, British authorities became concerned that the soft unjacketed lead bullet used in the 380/200 might be considered as violating the [Hague Convention of 1899](https://en.wikipedia.org/wiki/Hague_Conventions_of_1899_and_1907 "Hague Conventions of 1899 and 1907") governing deforming or 'explosive bullets'. A new .38 loading was introduced for use in combat utilizing a 178-grain (11.5 g), gilding-metal-jacketed lead bullet; new foresights were issued to compensate for the new cartridge's ballistics and change to the point of aim.[[13]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Dunlap,_Roy_1948_p._141-13) The new cartridge was accepted into Commonwealth Service as "Cartridge, Pistol, .380 Mk IIz", firing a 178 - 180 grain (11.7 g) full metal jacket round-nose bullet. The 380/200 Mk I lead bullet cartridge was continued in service, originally restricted to training and marksmanship practice.[[13]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Dunlap,_Roy_1948_p._141-13) However, after the outbreak of war, supply exigencies forced British authorities to use both the 380/200 Mk I and the .380 Mk IIz loadings interchangeably in combat. U.S. ammunition manufacturers such as Winchester-Western supplied 380/200 Mk I cartridges to British forces throughout the war.[[18]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-18)

## Other manufacturers

The vast majority of Enfield No 2 revolvers were made by RSAF (Royal Small Arms Factory) Enfield, but wartime necessities meant that numbers were produced elsewhere. [Albion Motors](https://en.wikipedia.org/wiki/Albion_Motors "Albion Motors") in Scotland made the Enfield No 2 Mk I* from 1941 to 1943, whereupon the contract for production was passed onto Coventry Gauge & Tool Co. By 1945, 24,000[[19]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-19) Enfield No 2 Mk I* and Mk I** revolvers had been produced by Albion/CG&T. The Singer Sewing Machine Company of Clydebank made components but they were assembled at Enfield under their proofmarks; Singer-made parts are marked "SSM".

The Howard Auto Cultivator Company (HAC) in [New South Wales](https://en.wikipedia.org/wiki/New_South_Wales "New South Wales"), Australia tooled up and began manufacturing the Enfield No 2 Mk I* and I** revolvers in 1941, but the production run was very limited (estimated at around 350 or so revolvers in total), and the revolvers produced were criticised for being non-interchangeable, even with other HAC-produced revolvers. Very few HAC revolvers are known to exist, and it is thought by many collectors that most of the HAC revolvers may have been destroyed in the various Australian Gun Amnesties and "Buy-Backs".

## Users

-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/23px-Flag_of_Australia_%28converted%29.svg.png) [Australia](https://en.wikipedia.org/wiki/Australia "Australia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_Botswana.svg/23px-Flag_of_Botswana.svg.png) [Botswana](https://en.wikipedia.org/wiki/Botswana "Botswana")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/23px-Flag_of_Canada_%28Pantone%29.svg.png) [Canada](https://en.wikipedia.org/wiki/Canada "Canada")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_The_Gambia.svg/23px-Flag_of_The_Gambia.svg.png) [Gambia](https://en.wikipedia.org/wiki/The_Gambia "The Gambia")[[20]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Hogg-20)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/21px-Flag_of_Israel.svg.png) [Israel](https://en.wikipedia.org/wiki/Israel "Israel") - Manufactured unlicensed by [Israeli Military Industries](https://en.wikipedia.org/wiki/Israeli_Military_Industries "Israeli Military Industries") from 1951 to 1974, standard issue sidearm for Israeli Police and Border Police to 1975 and for the IDF to 1968.
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Flag_of_Lesotho.svg/23px-Flag_of_Lesotho.svg.png) [Lesotho](https://en.wikipedia.org/wiki/Lesotho "Lesotho")[[20]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-Hogg-20)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Flag_of_Libya_%281977%E2%80%932011%29.svg/23px-Flag_of_Libya_%281977%E2%80%932011%29.svg.png) [Libya](https://en.wikipedia.org/wiki/Libya "Libya")[[21]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-21)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Flag_of_Malaysia.svg/23px-Flag_of_Malaysia.svg.png) [Malaysia](https://en.wikipedia.org/wiki/Malaysia "Malaysia")[[22]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-22)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Pakistan.svg/23px-Flag_of_Pakistan.svg.png) [Pakistan](https://en.wikipedia.org/wiki/Pakistan "Pakistan") - Standard issue sidearm for Pakistan Army officers till 1974, for Pakistan Police till 1989.
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/23px-Flag_of_Myanmar.svg.png) [Myanmar](https://en.wikipedia.org/wiki/Myanmar "Myanmar") :Retired[[23]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-23)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Flag_of_the_Philippines_%28navy_blue%29.svg/23px-Flag_of_the_Philippines_%28navy_blue%29.svg.png) [Philippines](https://en.wikipedia.org/wiki/Philippines "Philippines") - The Enfield No 2 Mk I Revolver was used by the Philippine Commonwealth military and recognized guerrillas during World War II from 1942 to 1945 and used by the Philippine military during the post-war era from 1945 to 1960s and the Hukbalahap Rebellion from 1946 to 1954.
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Flag_of_Sri_Lanka.svg/23px-Flag_of_Sri_Lanka.svg.png) [Sri Lanka](https://en.wikipedia.org/wiki/Sri_Lanka "Sri Lanka")
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/23px-Flag_of_the_United_Kingdom.svg.png) [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Flag_of_Rhodesia_%281968%E2%80%931979%29.svg/23px-Flag_of_Rhodesia_%281968%E2%80%931979%29.svg.png) [Rhodesia](https://en.wikipedia.org/wiki/Rhodesia "Rhodesia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/23px-Flag_of_the_Netherlands.svg.png) [Netherlands](https://en.wikipedia.org/wiki/Netherlands "Netherlands"): used in [Indonesia](https://en.wikipedia.org/wiki/Indonesia "Indonesia")[[24]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-ArmsControl-24)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/23px-Flag_of_New_Zealand.svg.png) [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand")[[25]](https://en.wikipedia.org/wiki/Enfield_No._2#cite_note-25)
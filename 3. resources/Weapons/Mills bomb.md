# Mills bomb

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/Mills_bomb#mw-head) [Jump to search](https://en.wikipedia.org/wiki/Mills_bomb#searchInput)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Text_document_with_red_question_mark.svg/40px-Text_document_with_red_question_mark.svg.png)

This article includes a list of general [references](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources"), but it remains largely unverified because **it lacks sufficient corresponding [inline citations](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources#Inline_citations "Wikipedia:Citing sources")**. Please help to [improve](https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Fact_and_Reference_Check "Wikipedia:WikiProject Fact and Reference Check") this article by [introducing](https://en.wikipedia.org/wiki/Wikipedia:When_to_cite "Wikipedia:When to cite") more precise citations. _(January 2010)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

Mills bomb

[![N°5-MkII N°23-MkII N°36-MkI.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/N%C2%B05-MkII_N%C2%B023-MkII_N%C2%B036-MkI.jpg/300px-N%C2%B05-MkII_N%C2%B023-MkII_N%C2%B036-MkI.jpg)](https://en.wikipedia.org/wiki/File:N%C2%B05-MkII_N%C2%B023-MkII_N%C2%B036-MkI.jpg)

Mills bombs. From left to right : No. 5, No. 23, No. 36

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/9dc93bb0-0d78-4fd5-8e2a-749e510e963c/dblry5i-622eac75-7766-4adf-a91e-0bf51ee534c8.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzlkYzkzYmIwLTBkNzgtNGZkNS04ZTJhLTc0OWU1MTBlOTYzY1wvZGJscnk1aS02MjJlYWM3NS03NzY2LTRhZGYtYTkxZS0wYmY1MWVlNTM0YzguanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.cigu9uwFh7M3ywONK9TE-juV21tikHz4NpqY74mLLyM)

Type

[Hand grenade](https://en.wikipedia.org/wiki/Hand_grenade "Hand grenade")

Place of origin

United Kingdom

Service history

In service

1915–1980s

Production history

Designed

1915

No. built

over 75 million

Variants

No. 5; No. 23 Mk I, II and III; No. 36 Mk I; No. 36M Mk I; No. 36 Mk II

Specifications

Mass

765 g (1 lb 11.0 oz)

Length

95.2 mm (3.75 in)

Diameter

61 mm (2.4 in)

---

Filling

[Baratol](https://en.wikipedia.org/wiki/Baratol_(explosive) "Baratol (explosive)")

Detonation  
mechanism

Percussion cap and time delay fuse: 7 seconds, later reduced to 4

"**Mills bomb**" is the popular name for a series of British [hand grenades](https://en.wikipedia.org/wiki/Hand_grenade "Hand grenade"). They were the first modern [fragmentation grenades](https://en.wikipedia.org/wiki/Fragmentation_grenade "Fragmentation grenade") used by the [British Army](https://en.wikipedia.org/wiki/British_Army "British Army") and saw widespread use in the [First](https://en.wikipedia.org/wiki/First_World_War "First World War") and [Second World Wars](https://en.wikipedia.org/wiki/Second_World_War "Second World War").

## Contents

-   [1 Overview](https://en.wikipedia.org/wiki/Mills_bomb#Overview)
-   [2 Models](https://en.wikipedia.org/wiki/Mills_bomb#Models)
-   [3 Identification marks](https://en.wikipedia.org/wiki/Mills_bomb#Identification_marks)
-   [4 Rifle grenade](https://en.wikipedia.org/wiki/Mills_bomb#Rifle_grenade)
-   [5 Gallery](https://en.wikipedia.org/wiki/Mills_bomb#Gallery)
-   [6 See also](https://en.wikipedia.org/wiki/Mills_bomb#See_also)
-   [7 References](https://en.wikipedia.org/wiki/Mills_bomb#References)
-   [8 External links](https://en.wikipedia.org/wiki/Mills_bomb#External_links)

## Overview

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/A_bombing_officer_lobbing_a_Mills_grenade%2C_Bestanddeelnr_158-2200.jpg/220px-A_bombing_officer_lobbing_a_Mills_grenade%2C_Bestanddeelnr_158-2200.jpg)](https://en.wikipedia.org/wiki/File:A_bombing_officer_lobbing_a_Mills_grenade,_Bestanddeelnr_158-2200.jpg)

An officer of the [British Salonika Army](https://en.wikipedia.org/wiki/British_Salonika_Army "British Salonika Army") demonstrates how to "lob" a Mills bomb during the First World War

[William Mills](https://en.wikipedia.org/wiki/William_Mills_(inventor_of_the_Mills_bomb) "William Mills (inventor of the Mills bomb)"), a [hand grenade](https://en.wikipedia.org/wiki/Hand_grenade "Hand grenade") designer from [Sunderland](https://en.wikipedia.org/wiki/Sunderland,_Tyne_and_Wear "Sunderland, Tyne and Wear"), patented, developed and manufactured the "Mills bomb" at the Mills Munitions Factory in [Birmingham](https://en.wikipedia.org/wiki/Birmingham "Birmingham"), England, in 1915.[[1]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-1) The Mills bomb was inspired by an earlier design by Belgian captain Leon Roland, who later engaged in a patent lawsuit.[[2]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-Sheffield2007-2) Col. Arthur Morrow, a New Zealand Wars officer, also believed aspects of his patent were incorporated into the Mills Bomb.[[3]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-3) The Mills bomb was adopted by the [British Army](https://en.wikipedia.org/wiki/British_Army "British Army") as its standard hand grenade in 1915 as the No. 5.[[4]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-listA-4)

The Mills bomb underwent numerous modifications. The No. 23 was a No. 5 with a rodded base plug which allowed it to be fired from a rifle. This concept evolved further with the No. 36, a variant with a detachable base plate for use with a rifle discharger cup. The final variation of the Mills bomb, the No. 36M, was specially designed and waterproofed with [shellac](https://en.wikipedia.org/wiki/Shellac "Shellac") for use in the hot climate of [Mesopotamia](https://en.wikipedia.org/wiki/Mesopotamia "Mesopotamia") in 1917 at first but remained in production for many years.[[4]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-listA-4) By 1918 the No. 5 and No. 23 had been declared obsolete and the No. 36 (but not the 36M) followed in 1932.

The Mills was a classic design; a grooved [cast iron](https://en.wikipedia.org/wiki/Cast_iron "Cast iron") "pineapple" with a central striker held by a close hand lever and secured with a pin. According to Mills's notes, the casing was grooved to make it easier to grip, not as an aid to fragmentation and it has been shown that it does not shatter along the segmented lines. The Mills was a _[defensive grenade](https://en.wikipedia.org/wiki/Grenade "Grenade")_ meant to be thrown from behind cover at a target in the open, wounding with fragmentation, as opposed to an _[offensive grenade](https://en.wikipedia.org/wiki/Offensive_grenade "Offensive grenade")_, which does not fragment, relying on short-range [blast effect](https://en.wikipedia.org/wiki/Blast_injury "Blast injury") to wound or stun the victim without endangering the thrower with fragments, which travel a much longer distance than blast. Despite the designations and their traits, "defensive" grenades were frequently used offensively and vice versa. A competent thrower could manage 49 ft (15 m) with reasonable accuracy,[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] but the grenade could throw lethal fragments farther than this. The British Home Guard were instructed that the throwing range of the No. 36 was about 30 yd (27 m) with a danger area of about 100 yd (91 m).[[5]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-5)

At first the grenade was fitted with a seven-second fuse but in the [Battle of France](https://en.wikipedia.org/wiki/Battle_of_France "Battle of France") in 1940 this delay proved to be too long, giving defenders time to escape the explosion, or even to throw the grenade back. Therefore, the delay was reduced to four seconds. In either case, [Hollywood](https://en.wikipedia.org/wiki/Hollywood "Hollywood") images of a soldier pulling the pin with his teeth are incorrect. The force needed would damage the teeth before it would arm the mechanism.[[6]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-6)

The heavy segmented bodies of "pineapple" type grenades result in an unpredictable pattern of fragmentation. After the Second World War, Britain adopted grenades that contained segmented coiled wire in smooth metal casings. The No. 36M Mk.I remained the standard grenade of the [British Armed Forces](https://en.wikipedia.org/wiki/British_Armed_Forces "British Armed Forces") and was manufactured in the UK until 1972, when it was replaced by the [L2](https://en.wikipedia.org/wiki/M61_grenade "M61 grenade") series. The 36M remained in service in some parts of the world such as India and Pakistan, where it was manufactured until the early 1980s. Mills bombs were still being used in combat as recently as 2004, for example in the incident which killed US Marine [Jason Dunham](https://en.wikipedia.org/wiki/Jason_Dunham "Jason Dunham") and wounded two of his comrades.[[7]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-7)

## Models

-   The _No. 5 Mk 1_ was the first version. The explosive was filled through a small circular plug on the upper half, the detonator assembly was inserted into the centre tube through the bottom of the grenade body via the base plug, the striker and spring was held in tension through the middle by the lever that was held down on the lugs (ears) located on the top of the grenade body via a split pin and ring called the safety pin/pull ring. It was issued in May 1915 and entered general issue when mass production caught up a year later in 1916.
-   The _No. 23 Mk 1_, the hand/rifle-grenade had a base plug drilled with a threaded hole for a rifle launching rod. The No. 23 Mk II had a new-style iron base plug that was easier to tighten with the fingers without the need for a spanner. The No. 23 Mk III was a new-style body with a larger filler hole plug and more solid -lever lugs/ears but retaining the Mk II style plug.
-   The _No. 36 Mk. 1_ was first introduced in May 1918. It used the No. 23 Mk III body with a new-style plug. Mostly made of iron, it was drilled and threaded for attaching a metal disk called a gas check to fire the grenade from a cup discharger (Burns) mounted on a rifle's muzzle and launched using a balastite blank cartridge.
-   The shellac-coated "[Mesopotamian](https://en.wikipedia.org/wiki/Mesopotamian_campaign "Mesopotamian campaign")" variant (_No. 36M Mk I_) was designed to keep moisture and humidity out of the detonator's fuse. The No. 36M Mk I was the British army's standard hand-grenade from the 1930s to 1972.[[8]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-8)

## Identification marks

-   A green band around the middle originally indicated an [Amatol](https://en.wikipedia.org/wiki/Amatol "Amatol") filling (1915–1920s), while it later indicated a [Baratol](https://en.wikipedia.org/wiki/Baratol "Baratol") or [Trotyl](https://en.wikipedia.org/wiki/Trotyl "Trotyl") filling (1920s–1970s).
-   A pink band around the middle indicates an [Ammonal](https://en.wikipedia.org/wiki/Ammonal "Ammonal") or Alumatol filling. (Alumatol is defined by the _[Dictionary of Explosives, pub 1920](https://archive.org/details/dictionaryofexpl00marsrich "iarchive:dictionaryofexpl00marsrich")_[[9]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-9) as "a mixture of ammonium nitrate, TNT and 'a small quantity' of aluminium powder".) A red band around the base plug on the bottom indicated the detonator was already installed and that the grenade was live.
-   Three red _X_s along each side indicates that it is the waterproofed No.36M model.

## Rifle grenade

The Mills bomb was developed into a [rifle grenade](https://en.wikipedia.org/wiki/Rifle_grenade "Rifle grenade") by attaching a metallic rod to its base. This rod-type rifle-grenade had an effective range of about 150 yards (140 m). The operating procedure was to insert the Mills bomb rod down the barrel of a [standard rifle](https://en.wikipedia.org/wiki/Lee%E2%80%93Enfield "Lee–Enfield"), put a special [blank cartridge](https://en.wikipedia.org/wiki/Blank_(cartridge) "Blank (cartridge)") (Ballistite cartridge) in the rifle's [chamber](https://en.wikipedia.org/wiki/Chamber_(firearms) "Chamber (firearms)"), place the rifle stock on the ground, then pull the Mills bomb's safety pin, releasing the safety lever and immediately fire the rifle. If the soldier did not launch the grenade quickly, the grenade's fuse would time out and explode. The British soon developed a simple cradle attached to the rifle's [bayonet lug](https://en.wikipedia.org/wiki/Bayonet_lug "Bayonet lug") to hold the safety-lever in place and prevent accidental detonations.[[10]](https://en.wikipedia.org/wiki/Mills_bomb#cite_note-10) However, it was found that the repeated launching of rod-type grenades caused damage to the rifle's barrel, causing the middle to bulge out due to the prolonged pressure spike from driving the much heavier, larger projectile up the barrel (typically a much faster process with a normal bullet); a rifle cartridge rapidly burns up all the available powder, which fills the volume behind the bullet with extremely high pressure gases (tens of thousands of PSI), the pressure rising as the bullet moves up the barrel, peaking at some point before the bullet leaves the muzzle. With the much heavier grenade and rod, the cartridge had to accelerate a much heavier mass, which resulted in the powder burning up and the pressure peaking before the rod had got more than a part of the way up the barrel, putting peak pressure on sooner and sustaining it for longer.

The British subsequently developed a cup-type launcher to replace the rod-type rifle-grenade. In this design, a can-shaped launcher was attached to the muzzle of the rifle and a gas check disc was screwed onto the base of the grenade before the grenade was placed in the launcher. The safety pin could then be removed as the launcher cup kept the safety-lever in place. The operator inserted the blank cartridge into the rifle before setting the stock, angled on the ground to absorb the recoil of the weapon. When the cartridge was fired it pushed the grenade out of the cup releasing the lever. The cup-type launcher could launch the grenade about 200 yards (180 m). Lee–Enfield rifles equipped with the cup launcher were modified with copper wire wrapped around the stock, to prevent the wood from splitting under the increased recoil. If necessary, both the rod and the gas check grenade could be thrown as a standard hand-grenade.

## Gallery

-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Mills_N%C2%B05_MkII.jpg/91px-Mills_N%C2%B05_MkII.jpg)](https://en.wikipedia.org/wiki/File:Mills_N%C2%B05_MkII.jpg)
    
    No. 5 Mk II Mills bomb
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Mills_N%C2%B05-_Eclat%C3%A9e.jpg/83px-Mills_N%C2%B05-_Eclat%C3%A9e.jpg)](https://en.wikipedia.org/wiki/File:Mills_N%C2%B05-_Eclat%C3%A9e.jpg)
    
    Cutaway view of a No. 5 Mills bomb
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Mills_N%C2%B023_MkII.jpg/85px-Mills_N%C2%B023_MkII.jpg)](https://en.wikipedia.org/wiki/File:Mills_N%C2%B023_MkII.jpg)
    
    No. 23 Mk II Mills bomb
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Mills_Bomb_SGM-1.JPG/82px-Mills_Bomb_SGM-1.JPG)](https://en.wikipedia.org/wiki/File:Mills_Bomb_SGM-1.JPG)
    
    36M grenade dated 1940
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Mills_Bomb_SGM-2.JPG/120px-Mills_Bomb_SGM-2.JPG)](https://en.wikipedia.org/wiki/File:Mills_Bomb_SGM-2.JPG)
    
    Base of 36M grenade dated 1940
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Mills_N%C2%B036_PGM.jpg/94px-Mills_N%C2%B036_PGM.jpg)](https://en.wikipedia.org/wiki/File:Mills_N%C2%B036_PGM.jpg)
    
    36M Mills bomb
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/N%C2%B023_MkII-Version_Fusil.jpg/53px-N%C2%B023_MkII-Version_Fusil.jpg)](https://en.wikipedia.org/wiki/File:N%C2%B023_MkII-Version_Fusil.jpg)
    
    Mills bomb No. 23 Mk II, with rod for launch by rifle
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/N%C2%B036%2B_plaque_de_base.jpg/94px-N%C2%B036%2B_plaque_de_base.jpg)](https://en.wikipedia.org/wiki/File:N%C2%B036%2B_plaque_de_base.jpg)
    
    Drawing of the Mills No. 36 rifle grenade, with its gascheck disk for use with cup-launcher
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Tromblon_Lance-Grenade%2C_Blunderbuss_Grenade_Launcher%2C_Mus%C3%A9e_Somme_1916%2C_pic-040.JPG/120px-Tromblon_Lance-Grenade%2C_Blunderbuss_Grenade_Launcher%2C_Mus%C3%A9e_Somme_1916%2C_pic-040.JPG)](https://en.wikipedia.org/wiki/File:Tromblon_Lance-Grenade,_Blunderbuss_Grenade_Launcher,_Mus%C3%A9e_Somme_1916,_pic-040.JPG)
    
    Lee-Enfield cup-launcher in the 1916 Somme Battlefield Museum, France
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Mills_Bomb_MK36.jpg/82px-Mills_Bomb_MK36.jpg)](https://en.wikipedia.org/wiki/File:Mills_Bomb_MK36.jpg)
    
    A case of derived type with special base plug
# Bren light machine gun

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/Bren_light_machine_gun#mw-head) [Jump to search](https://en.wikipedia.org/wiki/Bren_light_machine_gun#searchInput)

"Bren" redirects here. For other uses, see [Bren (disambiguation)](https://en.wikipedia.org/wiki/Bren_(disambiguation) "Bren (disambiguation)").

Bren

[![Bren wog.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Bren_wog.jpg/300px-Bren_wog.jpg)](https://en.wikipedia.org/wiki/File:Bren_wog.jpg)

A Bren Mk.1 gun

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/9dc93bb0-0d78-4fd5-8e2a-749e510e963c/dbls2ip-743e6935-ce4e-4e24-82b8-58fdc79b3a3b.jpg/v1/fill/w_1024,h_1060,q_75,strp/bren_mk_2_by_redroguexiii_dbls2ip-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTA2MCIsInBhdGgiOiJcL2ZcLzlkYzkzYmIwLTBkNzgtNGZkNS04ZTJhLTc0OWU1MTBlOTYzY1wvZGJsczJpcC03NDNlNjkzNS1jZTRlLTRlMjQtODJiOC01OGZkYzc5YjNhM2IuanBnIiwid2lkdGgiOiI8PTEwMjQifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.TERk0kq-tGmSLYl8BBST4aGpmcgxcsppw1f9cwYIPik)

Type

[Light machine gun](https://en.wikipedia.org/wiki/Light_machine_gun "Light machine gun")

Place of origin

-   [Czechoslovakia](https://en.wikipedia.org/wiki/Czechoslovakia "Czechoslovakia") (Design)
-   [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom") (Manufacture)

Service history

In service

1938–2006

Used by

_See [Users](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Users)_

Wars

-   [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")
-   [Chinese Civil War](https://en.wikipedia.org/wiki/Chinese_Civil_War "Chinese Civil War")
-   [Second Sino-Japanese War](https://en.wikipedia.org/wiki/Second_Sino-Japanese_War "Second Sino-Japanese War")
-   [Greek Civil War](https://en.wikipedia.org/wiki/Greek_Civil_War "Greek Civil War")
-   [First Indochina War](https://en.wikipedia.org/wiki/First_Indochina_War "First Indochina War")
-   [Indonesian National Revolution](https://en.wikipedia.org/wiki/Indonesian_National_Revolution "Indonesian National Revolution")
-   [Malayan Emergency](https://en.wikipedia.org/wiki/Malayan_Emergency "Malayan Emergency")
-   [1948 Arab–Israeli War](https://en.wikipedia.org/wiki/1948_Arab%E2%80%93Israeli_War "1948 Arab–Israeli War")
-   [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War")
-   [Bangladesh Liberation War](https://en.wikipedia.org/wiki/Bangladesh_Liberation_War "Bangladesh Liberation War")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-   [Mau Mau Uprising](https://en.wikipedia.org/wiki/Mau_Mau_Uprising "Mau Mau Uprising")[[1]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Encyclopedie-1)
-   [Algerian War](https://en.wikipedia.org/wiki/Algerian_War "Algerian War")
-   [Cyprus crisis of 1963–64](https://en.wikipedia.org/wiki/Cyprus_crisis_of_1963%E2%80%9364 "Cyprus crisis of 1963–64")
-   [Suez Crisis](https://en.wikipedia.org/wiki/Suez_Crisis "Suez Crisis")
-   [1958 Lebanon Crisis](https://en.wikipedia.org/wiki/1958_Lebanon_Crisis "1958 Lebanon Crisis")
-   [Sino-Indian War](https://en.wikipedia.org/wiki/Sino-Indian_War "Sino-Indian War")
-   [Congo Crisis](https://en.wikipedia.org/wiki/Congo_Crisis "Congo Crisis")
-   [Portuguese Colonial War](https://en.wikipedia.org/wiki/Portuguese_Colonial_War "Portuguese Colonial War")
-   [Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War")
-   [South African Border War](https://en.wikipedia.org/wiki/South_African_Border_War "South African Border War")
-   [Second Malayan Emergency](https://en.wikipedia.org/wiki/Communist_insurgency_in_Malaysia_(1968%E2%80%931989) "Communist insurgency in Malaysia (1968–1989)")
-   [Biafran War](https://en.wikipedia.org/wiki/Biafran_War "Biafran War")
-   [Rhodesian Bush War](https://en.wikipedia.org/wiki/Rhodesian_Bush_War "Rhodesian Bush War")
-   [Lebanese Civil War](https://en.wikipedia.org/wiki/Lebanese_Civil_War "Lebanese Civil War")
-   [Aden Emergency](https://en.wikipedia.org/wiki/Aden_Emergency "Aden Emergency")
-   [The Troubles](https://en.wikipedia.org/wiki/The_Troubles "The Troubles")
-   [Falklands War](https://en.wikipedia.org/wiki/Falklands_War "Falklands War")
-   [Nepalese Civil War](https://en.wikipedia.org/wiki/Nepalese_Civil_War "Nepalese Civil War")
-   [Invasion of Grenada](https://en.wikipedia.org/wiki/Invasion_of_Grenada "Invasion of Grenada")
-   [Gulf War](https://en.wikipedia.org/wiki/Gulf_War "Gulf War")
-   [Kargil War](https://en.wikipedia.org/wiki/Kargil_War "Kargil War")

Production history

Designed

1935

Manufacturer

-   [Royal Small Arms Factory](https://en.wikipedia.org/wiki/Royal_Small_Arms_Factory "Royal Small Arms Factory")
-   [Enfield](https://en.wikipedia.org/wiki/London_Borough_of_Enfield "London Borough of Enfield")
-   [John Inglis and Company](https://en.wikipedia.org/wiki/John_Inglis_and_Company "John Inglis and Company")
-   [Long Branch Factory](https://en.wikipedia.org/wiki/Long_Branch,_Toronto "Long Branch, Toronto")
-   [Ishapore Rifle Factory](https://en.wikipedia.org/wiki/Ordnance_Factory_Board "Ordnance Factory Board")
-   [Lithgow Small Arms Factory](https://en.wikipedia.org/wiki/Lithgow_Small_Arms_Factory "Lithgow Small Arms Factory")

Unit cost

£40[[2]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDugelbyStevens1999125-2)

Produced

1935–1971

No. built

500,000[[3]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201317-3)

Variants

Mk I, II, III, IV  
L4

Specifications

Mass

-   Mk1 & Mk2: 22.8 lb (10.3 kg), 25 lb (11.3 kg) loaded
-   Mk3 & Mk4: 19.15 lb (8.69 kg), 21.6 lb (9.8 kg) loaded

Length

Mk1 & Mk2: 45.5 in (1,160 mm)  
Mk3 & Mk4 42.9 in (1,090 mm)[[3]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201317-3)

[Barrel](https://en.wikipedia.org/wiki/Gun_barrel "Gun barrel") length

25 in (635 mm)

Crew

2, gunner and assistant

---

[Cartridge](https://en.wikipedia.org/wiki/Cartridge_(firearms) "Cartridge (firearms)")

-   [.303 British](https://en.wikipedia.org/wiki/.303_British ".303 British")
-   [8×56mmR](https://en.wikipedia.org/wiki/8%C3%9756mmR "8×56mmR") (for [Bulgaria](https://en.wikipedia.org/wiki/Kingdom_of_Bulgaria "Kingdom of Bulgaria"))
-   [7.92×57mm Mauser](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser "7.92×57mm Mauser") (for [China](https://en.wikipedia.org/wiki/Republic_of_China_(1912%E2%80%9349) "Republic of China (1912–49)") in World War II)
-   [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO") (post-WWII)
-   [7.62x39 Soviet](https://en.wikipedia.org/wiki/7.62x39_Soviet "7.62x39 Soviet") (for [China](https://en.wikipedia.org/wiki/People%27s_Republic_of_China "People's Republic of China") Post World War II)

[Action](https://en.wikipedia.org/wiki/Action_(firearms) "Action (firearms)")

[Gas-operated](https://en.wikipedia.org/wiki/Gas-operated_reloading "Gas-operated reloading"), [tilting bolt](https://en.wikipedia.org/wiki/Tilting_bolt "Tilting bolt")

[Rate of fire](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire")

-   500–520 rounds/min
-   practical 120 rounds/min sustained

[Muzzle velocity](https://en.wikipedia.org/wiki/Muzzle_velocity "Muzzle velocity")

2,440 ft/s (743.7 m/s)

Effective firing range

600 yd (550 m)

Maximum firing range

1,850 yd (1,690 m)

Feed system

-   20-round L1A1 SLR magazine
-   30-round detachable [box magazine](https://en.wikipedia.org/wiki/Box_magazine "Box magazine")
-   100-round detachable [pan magazine](https://en.wikipedia.org/wiki/Pan_magazine "Pan magazine")

Sights

Iron

The **Bren gun** was a series of [light machine guns](https://en.wikipedia.org/wiki/Light_machine_gun "Light machine gun") (LMG) made by Britain in the 1930s and used in various roles until 1992. While best known for its role as the British and [Commonwealth](https://en.wikipedia.org/wiki/British_Empire "British Empire") forces' primary infantry LMG in [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II"), it was also used in the [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War") and saw service throughout the latter half of the 20th century, including the 1982 [Falklands War](https://en.wikipedia.org/wiki/Falklands_War "Falklands War"). Although fitted with a [bipod](https://en.wikipedia.org/wiki/Bipod "Bipod"), it could also be mounted on a tripod or be vehicle-mounted.

The Bren gun was a licensed version of the Czechoslovak ZGB 33 light machine gun which, in turn, was a modified version of the [ZB vz. 26](https://en.wikipedia.org/wiki/ZB_vz._26 "ZB vz. 26"), which [British Army](https://en.wikipedia.org/wiki/British_Army "British Army") officials had tested during a firearms service competition in the 1930s. The later Bren gun featured a distinctive top-mounted curved box magazine, conical flash hider, and quick change barrel. The name _Bren_ was derived from [Brno](https://en.wikipedia.org/wiki/Brno "Brno"), the Czechoslovak city in [Moravia](https://en.wikipedia.org/wiki/Moravia "Moravia"), where the Zb vz. 26 was designed (in the [Zbrojovka Brno Factory](https://en.wikipedia.org/wiki/Zbrojovka_Brno "Zbrojovka Brno")) and [Enfield](https://en.wikipedia.org/wiki/London_Borough_of_Enfield "London Borough of Enfield"), site of the British [Royal Small Arms Factory](https://en.wikipedia.org/wiki/Royal_Small_Arms_Factory "Royal Small Arms Factory"). The designer was [Václav Holek](https://en.wikipedia.org/wiki/V%C3%A1clav_Holek "Václav Holek"), a gun inventor and design engineer.

In the 1950s, many Bren guns were re-barrelled to accept the [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO") cartridge and modified to feed from the magazine for the [L1](https://en.wikipedia.org/wiki/L1A1_Self-Loading_Rifle "L1A1 Self-Loading Rifle") (Commonwealth version of the [FN FAL](https://en.wikipedia.org/wiki/FN_FAL "FN FAL")) rifle as the L4 light machine gun. It was replaced in the British Army as the section LMG by the [L7](https://en.wikipedia.org/wiki/FN_MAG#British_versions "FN MAG") [general-purpose machine gun](https://en.wikipedia.org/wiki/General-purpose_machine_gun "General-purpose machine gun") (GPMG), a heavier [belt-fed](https://en.wikipedia.org/wiki/Belt_(firearm) "Belt (firearm)") weapon. This was supplemented in the 1980s by the [L86 Light Support Weapon](https://en.wikipedia.org/wiki/SA80#L86_LSW "SA80") firing the [5.56×45mm NATO](https://en.wikipedia.org/wiki/5.56%C3%9745mm_NATO "5.56×45mm NATO") round, leaving the Bren gun in use only as a pintle mount on some vehicles. The Bren gun was manufactured by [Indian Ordnance Factories](https://en.wikipedia.org/wiki/Ordnance_Factories_Organisation "Ordnance Factories Organisation") as the "Gun Machine 7.62mm 1B"[[4]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Ordnance_Factory_Board-4) before it was discontinued in 2012.[[5]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201366-5)

## Contents

-   [1 Development](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Development)
-   [2 Service](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Service)
    -   [2.1 Second World War](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Second_World_War)
    -   [2.2 Post-war](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Post-war)
-   [3 Variants](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Variants)
    -   [3.1 Mark 1](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Mark_1)
    -   [3.2 Mark 2](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Mark_2)
    -   [3.3 Mark 3](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Mark_3)
    -   [3.4 Mark 4](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Mark_4)
    -   [3.5 L4](https://en.wikipedia.org/wiki/Bren_light_machine_gun#L4)
    -   [3.6 Taden gun](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Taden_gun)
    -   [3.7 Semiautomatic Bren guns](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Semiautomatic_Bren_guns)
-   [4 Production](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Production)
-   [5 Users](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Users)
-   [6 Gallery](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Gallery)
-   [7 See also](https://en.wikipedia.org/wiki/Bren_light_machine_gun#See_also)
-   [8 Notes](https://en.wikipedia.org/wiki/Bren_light_machine_gun#Notes)
-   [9 References](https://en.wikipedia.org/wiki/Bren_light_machine_gun#References)
-   [10 External links](https://en.wikipedia.org/wiki/Bren_light_machine_gun#External_links)

## Development

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Australian_assault_on_pillbox%2C_January_1943%2C_Papua%2C_Giropa_Point.jpg/220px-Australian_assault_on_pillbox%2C_January_1943%2C_Papua%2C_Giropa_Point.jpg)](https://en.wikipedia.org/wiki/File:Australian_assault_on_pillbox,_January_1943,_Papua,_Giropa_Point.jpg)

Australian assault on a pillbox at Giropa Point, January 1943

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/A_member_of_No._9_Commando_at_Anzio%2C_equipped_for_a_patrol_with_his_Bren_gun%2C_5_March_1944._NA12469.jpg/220px-A_member_of_No._9_Commando_at_Anzio%2C_equipped_for_a_patrol_with_his_Bren_gun%2C_5_March_1944._NA12469.jpg)](https://en.wikipedia.org/wiki/File:A_member_of_No._9_Commando_at_Anzio,_equipped_for_a_patrol_with_his_Bren_gun,_5_March_1944._NA12469.jpg)

A member of [No. 9 Commando](https://en.wikipedia.org/wiki/No._9_Commando "No. 9 Commando") at Anzio, equipped for a patrol with his Bren gun, 5 March 1944

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Allied_Forces_in_the_United_Kingdom_1939-45_H11120.jpg/220px-Allied_Forces_in_the_United_Kingdom_1939-45_H11120.jpg)](https://en.wikipedia.org/wiki/File:Allied_Forces_in_the_United_Kingdom_1939-45_H11120.jpg)

A Bren gunner of the [Norwegian Brigade](https://en.wikipedia.org/wiki/Free_Norwegian_forces "Free Norwegian forces") takes aim during training at Dumfries, Scotland, 27 June 1941.

At the close of the [First World War](https://en.wikipedia.org/wiki/First_World_War "First World War") in 1918, the British Army was equipped with two main automatic weapons; the [Vickers medium machine gun](https://en.wikipedia.org/wiki/Vickers_machine_gun "Vickers machine gun") (MMG) and the [Lewis light machine gun](https://en.wikipedia.org/wiki/Lewis_gun "Lewis gun"). The Vickers was heavy and required a supply of water to keep it in operation, which tended to relegate it to static defence and indirect fire support. The Lewis, although lighter, was still heavy and was prone to frequent stoppages; its barrel could not be changed in the field, which meant that sustained firing resulted in overheating until it stopped altogether. In 1922, to find a replacement for the Lewis, the Small Arms Committee of the British Army ran competitive trials between the [Madsen machine gun](https://en.wikipedia.org/wiki/Madsen_machine_gun "Madsen machine gun"), the [M1918 Browning Automatic Rifle](https://en.wikipedia.org/wiki/M1918_Browning_Automatic_Rifle "M1918 Browning Automatic Rifle") (BAR), the [Hotchkiss M1909 machine gun](https://en.wikipedia.org/wiki/Hotchkiss_M1909_Ben%C3%A9t%E2%80%93Merci%C3%A9_machine_gun "Hotchkiss M1909 Benét–Mercié machine gun"), the [Beardmore-Farquhar rifle](https://en.wikipedia.org/wiki/Farquhar%E2%80%93Hill_rifle "Farquhar–Hill rifle"), and the Lewis itself. Although the BAR was recommended, the sheer number of Lewis guns available and the difficult financial conditions meant that nothing was done. Various new models of light machine gun were tested as they became available, and in 1930, a further set of extensive trials commenced, overseen by [Frederick Hubert Vinden](https://en.wikipedia.org/wiki/Frederick_Hubert_Vinden "Frederick Hubert Vinden").[[6]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-6) This time the weapons tested included the [SIG Neuhausen KE7](https://en.wikipedia.org/wiki/SIG_Neuhausen_KE7 "SIG Neuhausen KE7"), the [Vickers-Berthier](https://en.wikipedia.org/wiki/Vickers-Berthier "Vickers-Berthier") and the Czechoslovak ZB vz.26. The Vickers-Berthier was later adopted by the [Indian Army](https://en.wikipedia.org/wiki/British_Indian_Army "British Indian Army") because it could be manufactured at once, rather than wait for the British Lewis production run to finish; it too saw extensive service in World War II.[[7]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant20139–11-7)

Following these trials, the British Army adopted the Czechoslovak ZB vz.26 light machine gun manufactured in [Brno](https://en.wikipedia.org/wiki/Brno "Brno") in 1935, although a slightly modified model, the ZB vz. 27, rather than the ZB vz. 26 which had been submitted for the trials. The design was modified to British requirements under new designation ZGB 33, which was then licensed for British manufacture under the Bren name. The major changes were in the magazine and barrel and the lower pistol grip assembly which went from a swivelling grip frame pivoted on the front of the trigger guard to a sliding grip frame which included the forward tripod mount and sliding ejection port cover. The magazine was curved in order to feed the rimmed [.303 inch SAA](https://en.wikipedia.org/wiki/.303_British ".303 British") ("Small Arms Ammunition") cartridge, a change from the various [rimless](https://en.wikipedia.org/wiki/Rimless "Rimless") [Mauser](https://en.wikipedia.org/wiki/Mauser "Mauser")-design [cartridges](https://en.wikipedia.org/wiki/Cartridge_(firearm) "Cartridge (firearm)") such as the [8mm Mauser](https://en.wikipedia.org/wiki/8%C3%9757mm_IS "8×57mm IS") round previously used by Czech designs. These modifications were categorised in various numbered designations, ZB vz. 27, ZB vz. 30, ZB vz. 32, and finally the ZGB 33, which was licensed for manufacture under the Bren name.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

The Bren was a gas-operated weapon, which used the same .303 ammunition as the standard British [bolt-action rifle](https://en.wikipedia.org/wiki/Bolt-action_rifle "Bolt-action rifle"), the [Lee–Enfield](https://en.wikipedia.org/wiki/Lee%E2%80%93Enfield "Lee–Enfield"), firing at a rate of between 480 and 540 rounds per minute (rpm), depending on the model. Propellant gases vented from a port towards the muzzle end of the barrel through a regulator (visible just in front of the bipod) with four quick-adjustment apertures of different sizes, intended to tailor the gas volume to different ambient temperatures (smallest flow at high temperature, e.g. summer desert, largest at low temperature, e.g. winter Arctic). The vented gas drove a piston which in turn actuated the breech block. Each gun came with a spare barrel that could be quickly changed when the barrel became hot during sustained fire, though later guns featured a [chrome](https://en.wikipedia.org/wiki/Chromium "Chromium")-lined barrel, which reduced the need for a spare. To change barrels, the release catch in front of the magazine was rotated to unlock the barrel. The carrying handle above the barrel was used to grip and remove the hot barrel without burning the hands.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

The Bren was magazine-fed, which slowed its rate of fire and required more frequent reloading than British belt-fed machine guns such as the larger .303 Vickers machine gun. The slower rate of fire prevented more rapid overheating of the Bren's air-cooled barrel, and the Bren was much lighter than belt-fed machine guns, which typically had cooling jackets, often liquid filled. The magazines also prevented the ammunition from getting dirty, which was more of a problem with the Vickers with its 250-round canvas belts. The [sights](https://en.wikipedia.org/wiki/Sight_(device) "Sight (device)") were offset to the left, to avoid the magazine on the top of the weapon. The position of the sights meant that the Bren could be fired only from the right shoulder.[[8]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201340-8)

## Service

### Second World War

In the British and Commonwealth armies, the Bren was generally issued on a scale of one per rifle section.[[9]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201328-9) An infantry battalion also had a "carrier" platoon, equipped with [Universal Carriers](https://en.wikipedia.org/wiki/Universal_Carrier "Universal Carrier"), each of which carried a Bren gun.[[10]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201332-10) Parachute battalions from 1944 had an extra Bren in the Anti-tank platoon.[[11]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-11) The 66-man "Assault Troop" of [British Commandos](https://en.wikipedia.org/wiki/British_Commandos "British Commandos") had a nominal establishment of four Bren guns. Realising the need for additional section-level firepower, the British Army endeavoured to issue the Bren in great numbers, with a stated goal of one Bren to every four private soldiers.[[12]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDunlap1948[[Category:Wikipedia_articles_needing_page_number_citations_from_September_2010]]<sup_class="noprint_Inline-Template_"_style="white-space:nowrap;">&#91;<i>[[Wikipedia:Citing_sources|<span_title="This_citation_requires_a_reference_to_the_specific_page_or_range_of_pages_in_which_the_material_appears.&#32;(September_2010)">page&nbsp;needed</span>]]</i>&#93;</sup>-12) The Bren was operated by a two-man crew, sometimes commanded by a [Lance Corporal](https://en.wikipedia.org/wiki/Lance_Corporal "Lance Corporal") as an infantry section's "gun group", the remainder of the section forming the "rifle group". The gunner or "Number 1" carried and fired the Bren, and a loader or "Number 2" carried extra magazines, a spare barrel and a tool kit.[[13]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201329-13) Number 2 helped reload the gun and replace the barrel when it overheated, and spotted targets for Number 1.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

Generally, the Bren was fired from the prone position using the attached bipod.[[14]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-14) On occasion, a Bren gunner would use his weapon on the move supported by a sling, much like an automatic rifle, and from standing or kneeling positions. Using the sling, Australian soldiers regularly fired the Bren from the hip, for instance in the [marching fire](https://en.wikipedia.org/wiki/Marching_fire "Marching fire") tactic, a form of [suppressive fire](https://en.wikipedia.org/wiki/Suppressive_fire "Suppressive fire") moving forward in assault. A [Victoria Cross](https://en.wikipedia.org/wiki/Victoria_Cross "Victoria Cross") was awarded to Private [Bruce Kingsbury](https://en.wikipedia.org/wiki/Bruce_Kingsbury "Bruce Kingsbury") for such use at [Isurava](https://en.wikipedia.org/wiki/Battle_of_Isurava "Battle of Isurava"), New Guinea, in 1942, during the Australians' fighting retreat from Kokoda.[[15]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-15)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Bren_gunner_of_the_Royal_Scots_06-11-1944.jpg/220px-Bren_gunner_of_the_Royal_Scots_06-11-1944.jpg)](https://en.wikipedia.org/wiki/File:Bren_gunner_of_the_Royal_Scots_06-11-1944.jpg)

Bren gunner of the [Royal Scots](https://en.wikipedia.org/wiki/Royal_Scots "Royal Scots") in [North Brabant](https://en.wikipedia.org/wiki/North_Brabant "North Brabant"), the Netherlands, 1944

Each British soldier's equipment normally included two magazines for his section's Bren gun. The large ammunition pouches on the [1937 Pattern Web Equipment](https://en.wikipedia.org/wiki/1937_Pattern_Web_Equipment "1937 Pattern Web Equipment") were designed around the Bren magazine. The Bren was regarded as the principal weapon of an infantry section, providing the majority of its firepower. As such, all ranks were trained in its operation.[[16]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-16)

The Bren had an effective range of around 600 yards (550 m) when fired from a prone position with a [bipod](https://en.wikipedia.org/wiki/Bipod "Bipod").[[12]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDunlap1948[[Category:Wikipedia_articles_needing_page_number_citations_from_September_2010]]<sup_class="noprint_Inline-Template_"_style="white-space:nowrap;">&#91;<i>[[Wikipedia:Citing_sources|<span_title="This_citation_requires_a_reference_to_the_specific_page_or_range_of_pages_in_which_the_material_appears.&#32;(September_2010)">page&nbsp;needed</span>]]</i>&#93;</sup>-12) or could deliver a beaten ground of 115 yds by 12 ft out to 1000 yds on the bipod.[[17]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-17)

For a light machine gun of the interwar and early World War II era, the Bren was about average in weight. On long marches in non-operational areas it was often partially disassembled and its parts were carried by two soldiers. The top-mounted magazine vibrated and moved during fire, making the weapon more visible in combat, and many Bren gunners used paint or improvised canvas covers to disguise the prominent magazine.[[18]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGeorge1948[[Category:Wikipedia_articles_needing_page_number_citations_from_September_2010]]<sup_class="noprint_Inline-Template_"_style="white-space:nowrap;">&#91;<i>[[Wikipedia:Citing_sources|<span_title="This_citation_requires_a_reference_to_the_specific_page_or_range_of_pages_in_which_the_material_appears.&#32;(September_2010)">page&nbsp;needed</span>]]</i>&#93;</sup>-18)

The 30-round magazine was in practice usually filled with 27 or 28 rounds to prevent jams and avoid wearing out the magazine spring. Care needed to be taken when loading the magazine to ensure that each round went ahead of the previous round, so that the .303 cartridge rims did not overlap the wrong way, which would cause a jam. The spent [cartridge cases](https://en.wikipedia.org/wiki/Cartridge_case "Cartridge case") were [ejected downwards](https://en.wikipedia.org/wiki/Downward_ejection "Downward ejection"), which was an improvement on the Lewis gun, which ejected sideways, since the glint of them flying through the air could compromise a concealed firing position.[[19]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201341-19)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Private_of_the_Canadian_Perth_Regiment_advancing_through_forest_north_of_Arnhem_with_Bren_gun_1945.jpg/170px-Private_of_the_Canadian_Perth_Regiment_advancing_through_forest_north_of_Arnhem_with_Bren_gun_1945.jpg)](https://en.wikipedia.org/wiki/File:Private_of_the_Canadian_Perth_Regiment_advancing_through_forest_north_of_Arnhem_with_Bren_gun_1945.jpg)

Bren carried by a Canadian soldier in 1945

In general, the Bren was considered a reliable and effective light machine gun, though in [North Africa](https://en.wikipedia.org/wiki/North_African_campaign "North African campaign") it was reported to jam regularly unless kept very clean and free of sand or dirt.[[12]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDunlap1948[[Category:Wikipedia_articles_needing_page_number_citations_from_September_2010]]<sup_class="noprint_Inline-Template_"_style="white-space:nowrap;">&#91;<i>[[Wikipedia:Citing_sources|<span_title="This_citation_requires_a_reference_to_the_specific_page_or_range_of_pages_in_which_the_material_appears.&#32;(September_2010)">page&nbsp;needed</span>]]</i>&#93;</sup>-12) It was popular with British troops, who respected its reliability and combat effectiveness. The quality of the materials used would generally ensure minimal jamming. When the gun did jam through fouling caused by prolonged firing, the operator could adjust the four-position gas regulator to feed more gas to the piston increasing the power to operate the mechanism. The barrel needed to be unlocked and slid forward slightly to allow the regulator to be turned. It was even said that all problems with the Bren could simply be cleared by hitting the gun, turning the regulator or doing both. It was "by general consent the finest light machine gun in the world of its period, and the most useful weapon provided to the (French) "maquis" ... accurate up to 1,000 meters, and (it) could withstand immense maltreatment and unskilled use. "Resistants" were constantly pleading for maximum drops of Brens".[[20]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-20)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Tysklandsbrigaden%2C_%C3%B8velse_Skandia_III_-_Fo30141603020010.jpg/220px-Tysklandsbrigaden%2C_%C3%B8velse_Skandia_III_-_Fo30141603020010.jpg)](https://en.wikipedia.org/wiki/File:Tysklandsbrigaden,_%C3%B8velse_Skandia_III_-_Fo30141603020010.jpg)

Bren with 100 round detachable [pan magazine](https://en.wikipedia.org/wiki/Pan_magazine "Pan magazine"). With the magazine fitted, the original sights cannot be used.[[21]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-21)

Although they were generally well-liked, the high cost of £40 each gun was an issue for the British Army leadership. This became a greater issue when it was discovered that only 2,300 of the 30,000 Bren guns issued to the [British Expeditionary Force](https://en.wikipedia.org/wiki/British_Expeditionary_Force_(World_War_II) "British Expeditionary Force (World War II)") came back to Britain after the defeat of France. As the result, cost savings and increased rate of production became two main goals for subsequent variant designs. The Bren Mk II design simplified production by replacing the drum rear sight with a ladder design, making the bipod legs non-adjustable, simplifying the gun butt, reducing the use of [stainless steel](https://en.wikipedia.org/wiki/Stainless_steel "Stainless steel"), among other steps that reduced the cost by 20% to 25%; Mk II was approved in September 1940 and entered production in 1941. While the Bren Mk III design also aimed at reducing cost, it also had the concurrent goal of being lightened for jungle warfare; the final product weighed 19 lb 5 oz (8.8 kg), 3 pounds lighter than the original Bren Mk I design; it was standardised in July 1944 and saw a production of 57,600. Also standardised in July 1944 was the Bren Mk IV, which was further lightened to 19 lb 2 oz (8.7 kg); however, it did not enter production until July 1945, and only 250 were built before the end of the war. While Enfield was able to produce only 400 Bren Mk I guns each month, with the various simplification efforts production numbers rose to 1,000 guns per week by 1943. Among the variant designs were two speciality prototypes that never entered production: The belt-fed [Taden gun](https://en.wikipedia.org/wiki/TADEN_gun "TADEN gun") for stationary defence use, and the ultra-simplified [Besal](https://en.wikipedia.org/wiki/Besal "Besal") gun to be produced in case a German invasion of Britain actually took place (which would hinder British production efforts). Later designs of production Bren guns featured chrome-lined barrels that offered less resistance, preventing overheating and reducing the need for quick changes of barrels.[[22]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-WW2DB-22)

Bren guns were produced outside of Britain as well. In Canada, the [John Inglis](https://en.wikipedia.org/wiki/John_Inglis_and_Company "John Inglis and Company") plant in Toronto began tooling its facilities for production in 1938; the first of 186,000 examples was completed in March 1940. Some of the Inglis-built Bren guns were chambered for the 7.92-mm Mauser ammunition; these were destined for export to [Nationalist Chinese forces](https://en.wikipedia.org/wiki/National_Revolutionary_Army "National Revolutionary Army") rather than for British and Commonwealth forces. In Australia, the [Lithgow Small Arms Factory](https://en.wikipedia.org/wiki/Lithgow_Small_Arms_Factory "Lithgow Small Arms Factory") in New South Wales began building Bren guns in 1940; a total of 17,249 were built. In India, the [factory at Ishapore](https://en.wikipedia.org/wiki/Rifle_Factory_Ishapore "Rifle Factory Ishapore") began building Bren guns in 1942 (it had produced Vickers-Berthier machine guns prior to this time), and would continue producing them for decades long after the end of the Second World War. Many of the Bren guns produced at Ishapore went to Indian troops, who had lost a great number of automatic weapons during the disastrous campaigns against the Japanese in Malaya and Burma; [17th Indian Infantry Division](https://en.wikipedia.org/wiki/17th_Infantry_Division_(India) "17th Infantry Division (India)"), for example, found itself with only 56 Bren guns after f[fleeing out of Burma in 1942](https://en.wikipedia.org/wiki/Burma_campaign_(1942%E2%80%9343) "Burma campaign (1942–43)").[[22]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-WW2DB-22)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/1944_Bren_light_machine_gun.jpg/220px-1944_Bren_light_machine_gun.jpg)](https://en.wikipedia.org/wiki/File:1944_Bren_light_machine_gun.jpg)

Bren gun mounted on a tripod, 2010

A tripod mount with 42 degrees of traverse was available to allow the Bren to be used as an indirect-fire weapon, but this was rarely used in the field. The Bren was also used on many vehicles, the [Universal Carrier](https://en.wikipedia.org/wiki/Universal_Carrier "Universal Carrier") was known as the "Bren Gun Carrier" (actually the name of a predecessor vehicle), and on tanks and armoured cars. It could not be used as a co-axial weapon on tanks, as the magazine restricted its depression and was awkward to handle in confined spaces, and it was therefore used on a [pintle mount](https://en.wikipedia.org/wiki/Pintle_mount "Pintle mount") only. (The belt fed [Vickers](https://en.wikipedia.org/wiki/Vickers_light_machine_gun "Vickers light machine gun") or [Besa](https://en.wikipedia.org/wiki/Besa_machine_gun "Besa machine gun"), the latter being another Czechoslovak machine gun design adopted by the British, were instead used as co-axial weapons.) An unfortunate problem occurred when the Bren was fired from the [Dingo Scout Car](https://en.wikipedia.org/wiki/Dingo_Scout_Car "Dingo Scout Car"); the hot cartridge cases tended to be ejected down the neck of the driver, whose position was next to the pintle. A canvas bag was designed to catch the cartridges and overcome the problem, but it seems to have been rarely issued.[[19]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201341-19)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Anti-aircraft_BrenGun.jpg/220px-Anti-aircraft_BrenGun.jpg)](https://en.wikipedia.org/wiki/File:Anti-aircraft_BrenGun.jpg)

[Indian](https://en.wikipedia.org/wiki/British_Indian_Army "British Indian Army") troops man a Bren gun on an anti-aircraft tripod, [Western Desert](https://en.wikipedia.org/wiki/Western_Desert_(North_Africa) "Western Desert (North Africa)") April 1941

The Bren was also employed in the [anti-aircraft](https://en.wikipedia.org/wiki/Anti-aircraft_warfare "Anti-aircraft warfare") role with the tripod reconfigured for high angle fire. There were also several designs of less-portable mountings, including the _Gallows_ and _Mottley_ mounts. A 100-round [pan magazine](https://en.wikipedia.org/wiki/Magazine_(firearm)#Pan "Magazine (firearm)") was available for the Bren for use in the anti-aircraft role.[[23]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-23)

The Bren's direct ancestor, the Czechoslovak ZB vz. 26, was also used in World War II by German and Romanian forces, including units of the [Waffen SS](https://en.wikipedia.org/wiki/Waffen_SS "Waffen SS"). Many [7.92 mm](https://en.wikipedia.org/wiki/8%C3%9757mm_IS "8×57mm IS") ZB light machine guns were shipped to China, where they were employed first against the Japanese in World War II, and later against UN forces in Korea, including British and Commonwealth units. Some ex-Chinese Czech ZB weapons were also in use in the early stages of the [Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War"). Production of a 7.92 mm round model for the Far East was carried out by Inglis of Canada. The Bren was also delivered to the [Soviet Union](https://en.wikipedia.org/wiki/Soviet_Union "Soviet Union") as part of the [lend-lease](https://en.wikipedia.org/wiki/Lend-lease "Lend-lease") program[[24]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-24)

### Post-war

The British Army, and the armies of various countries of the [Commonwealth](https://en.wikipedia.org/wiki/British_Empire "British Empire"), used the Bren in the [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War"), the [Malayan Emergency](https://en.wikipedia.org/wiki/Malayan_Emergency "Malayan Emergency"), the [Mau Mau Uprising](https://en.wikipedia.org/wiki/Mau_Mau_Uprising "Mau Mau Uprising") and the [Indonesia–Malaysia confrontation](https://en.wikipedia.org/wiki/Indonesia%E2%80%93Malaysia_confrontation "Indonesia–Malaysia confrontation"), where it was preferred to its replacement, the belt-fed [L7 GPMG](https://en.wikipedia.org/wiki/FN_MAG "FN MAG"), on account of its lighter weight. In the [conflict in Northern Ireland](https://en.wikipedia.org/wiki/Operation_Banner "Operation Banner") (1969–1998), a British Army squad typically carried the L4A4 version of the Bren as the squad automatic weapon in the 1970s.[[25]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201326-25) During the [Falklands War](https://en.wikipedia.org/wiki/Falklands_War "Falklands War") in 1982, [40 Commando](https://en.wikipedia.org/wiki/40_Commando "40 Commando") [Royal Marines](https://en.wikipedia.org/wiki/Royal_Marines "Royal Marines") carried one LMG and one GPMG per section. Its final operational deployment with the British Army, on a limited scale, was in the [First Gulf War](https://en.wikipedia.org/wiki/First_Gulf_War "First Gulf War") in 1991.[[26]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201326_and_59-26)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Armamento_-_Museo_de_Armas_de_la_Naci%C3%B3n_09.JPG/220px-Armamento_-_Museo_de_Armas_de_la_Naci%C3%B3n_09.JPG)](https://en.wikipedia.org/wiki/File:Armamento_-_Museo_de_Armas_de_la_Naci%C3%B3n_09.JPG)

Bren gun in an Argentine museum

When the British Army adopted the [7.62 mm](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO") [NATO](https://en.wikipedia.org/wiki/NATO "NATO") cartridge, the Bren was re-designed to 7.62 mm calibre, fitted with a new bolt, barrel and magazine. It was re-designated as the "L4 light machine gun" (in various sub-versions) and remained in British Army service into the 1990s. A slotted flash hider similar to that of the contemporary L1 rifle and L7 general purpose machine gun replaced the conical flash hider. The change from a rimmed to rimless cartridge and nearly straight magazine improved feeding considerably, and allowed use of 20-round magazines from the 7.62 mm [L1A1 Self-Loading Rifle](https://en.wikipedia.org/wiki/L1A1_Self-Loading_Rifle "L1A1 Self-Loading Rifle"). Bren gunners using the L4A1 were normally issued with the 30-round magazine from the SAW L2A1.

Completion of the move to a [5.56 mm](https://en.wikipedia.org/wiki/5.56%C3%9745mm_NATO "5.56×45mm NATO") NATO cartridge led to the Army removing the Bren/L4 from the list of approved weapons and then withdrawing it from service.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

The Mark III Bren remained in limited use with the [Army Reserve](https://en.wikipedia.org/wiki/Irish_Army_Reserve "Irish Army Reserve") of the [Irish Defence Forces](https://en.wikipedia.org/wiki/Defence_Forces_(Ireland) "Defence Forces (Ireland)") until 2006, when the 7.62 mm GPMG replaced it. The Bren was popular with the soldiers who fired it (known as Brenners) as it was light and durable, and had a reputation for accuracy. The most notable use of the Bren by Irish forces was in the [Congo Crisis](https://en.wikipedia.org/wiki/Congo_Crisis "Congo Crisis") during the 1960s, when the Bren was the regular army's standard section automatic weapon.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

Bren guns were in service with the [Rhodesian Security Forces](https://en.wikipedia.org/wiki/Rhodesian_Security_Forces "Rhodesian Security Forces") during the [Rhodesian Bush War](https://en.wikipedia.org/wiki/Rhodesian_Bush_War "Rhodesian Bush War"), including a substantial number re-chambered for 7.62 mm cartridges similar to those examples in the British Army.[[27]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Moorcraft-27) The Rhodesian Bren guns continued to see frequent action until the 1970s, when they were largely replaced by the [FN MAG](https://en.wikipedia.org/wiki/FN_MAG "FN MAG").[[28]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Fireforce-28) A few were captured and re-issued by the [Zimbabwe People's Revolutionary Army](https://en.wikipedia.org/wiki/Zimbabwe_People%27s_Revolutionary_Army "Zimbabwe People's Revolutionary Army") (ZIPRA).[[29]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Impasse-29) Some examples were still in service with reservists of the [British South Africa Police](https://en.wikipedia.org/wiki/British_South_Africa_Police "British South Africa Police") in 1980, and were inherited by the [Zimbabwe Republic Police](https://en.wikipedia.org/wiki/Zimbabwe_Republic_Police "Zimbabwe Republic Police") upon the country's internationally recognised independence.[[30]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Cocks2-30) Zimbabwean policemen continued to deploy Bren guns during operations against ZIPRA dissidents throughout the early 1980s.[[30]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Cocks2-30)

The [South African Defence Force](https://en.wikipedia.org/wiki/South_African_Defence_Force "South African Defence Force") deployed Bren guns during the [South African Border War](https://en.wikipedia.org/wiki/South_African_Border_War "South African Border War") alongside the more contemporary FN MAG as late as 1978.[[31]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Captor-31)

## Variants

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **needs additional citations for [verification](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=Bren_light_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and removed. _(January 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

### Mark 1

Introduced September 1937; the original Czechoslovak designed ZGB 33. Overall length 45.5 inches (1.16 m), 25 inches (0.64 m) barrel length. Weight 22 lb 2 oz (10.0 kg).

Features:

-   Drum-pattern rear aperture sight
-   Buttstrap for use over-the-shoulder when firing
-   Rear grip under butt
-   Telescoping bipod
-   Folding cocking handle

An Enfield-made .303 Bren Mk 1 was converted to [7.92mm](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser#British_military_ammunition "7.92×57mm Mauser") in 1938 due to the suggestion of a possibility of a British Army change over to a rimless cartridge for machine guns being mooted.[[i]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-32)

### Mark 2

Introduced 1941. A simplified version of the Mk1 more suited to wartime production with original design features subsequently found to be unnecessary deleted.[[ii]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-33) Produced by Inglis of Canada and the Monotype Group through a number of component manufacturing factories. Sometimes known as the "Garage hands" model. Overall length 45.5 inches (1.16 m), 25 inches (0.64 m) barrel length. Weight 23 lb 3 oz (10.5 kg).

**Features:**

-   Folding-leaf rear sight
-   Buttstrap deleted
-   Rear grip deleted
-   Fixed height bipod
-   Fixed cocking handle

The Bren Mk2 was much simplified in the body, which although still being milled from a solid billet of steel, required significantly fewer milling operations than the Mk1, resulting in a much cleaner appearance. The bipod was simplified in design as well as not having extending legs. Most Mk2 bipods resembled a simple A-frame and were more 'soldier proof'. The Mk2 also featured a slightly higher rate of fire than the Mk1.

The woodwork on the Mk2 was simplified by being less ornate and ergonomic, which sped up the manufacturing process. The barrel was also simplified by means of a non-stepped removable flash hider and, in some cases, a barrel fore-end that was matte instead of highly polished. The buffered buttplate of the Mk1 was omitted and replaced with a sheet metal buttplate.

A small number of Inglis-made .303 Bren Mk 2 were converted post-war to fire the [.280 in (7 mm) Mk 1Z](https://en.wikipedia.org/wiki/.280_British ".280 British") round used by the [EM-2 rifle](https://en.wikipedia.org/wiki/EM-2_rifle "EM-2 rifle").[[32]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201323-34)

The Inglis version of the Bren Mk 2 chambered for the [.30-06](https://en.wikipedia.org/wiki/.30-06 ".30-06") (7.62 mm) cartridge and known as the M41 was also manufactured in [Taiwan](https://en.wikipedia.org/wiki/Taiwan "Taiwan") after the end of the [Chinese Civil War](https://en.wikipedia.org/wiki/Chinese_Civil_War "Chinese Civil War").[[33]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201324-35)

### Mark 3

A shorter and lighter Bren made by Enfield from 1944 for the war in the East and for Airborne Forces. This was similar to the Mk2 but with the light weight features of the early Mk1. With the main distinguishing feature being a shorter barrel and serrated area in front of the barrel nut. Overall length 42.9 in (1.09 m), 22.25 in (0.565 m) barrel length. Weight 19 lb 5 oz (8.8 kg).

### Mark 4

As with the Mk3 but this was a conversion of a Mk2. Overall length 42.9 in (1.09 m), 22.25 in (0.565 m) barrel length. Weight 19 lb 2 oz (8.7 kg).

### L4

The Bren was converted to 7.62x51mm in the 1950s, and designated the L4. L4 Brens can easily be identified by their straighter magazine and cylindrical flash hider. The British-issue L4 magazine retains the 30-round capacity and has a slight curve. The L4 magazine was interchangeable with the [L1A1 SLR](https://en.wikipedia.org/wiki/L1A1_Self-Loading_Rifle "L1A1 Self-Loading Rifle") magazine, so the L4 Bren can be seen fitted with straight 20-round magazines from the SLR or with the straight 30-round magazine from the Australian [L2A1](https://en.wikipedia.org/wiki/L1A1_Self-Loading_Rifle#Australia "L1A1 Self-Loading Rifle") or Canadian C2A1 heavy-barrel SLR. The flash suppressor was changed from the cone type of .303 variants to a slotted, cylindrical type similar in appearance to that used on the SLR and L7 GPMG. The L4 remained in British service until the 1990s.[[34]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-36)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Machine_Gun_%288037159644%29.jpg/220px-Machine_Gun_%288037159644%29.jpg)](https://en.wikipedia.org/wiki/File:Machine_Gun_(8037159644).jpg)

L4-series Bren with distinctive flash hider and magazine

L4 designations[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

Designation

Description

L4A1

Bren Mk3 conversion originally known as X10E1, with Mk1 bipod and two steel barrels.[[iii]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-37)

L4A2

Bren Mk3 conversion originally known as X10E2, lightened bipod and two steel barrels.

L4A3

Bren Mk2 conversion, one chromium-plated steel barrel.[[iv]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-38)

L4A4

L4A2 variant with one chromium-plated steel barrel

L4A5

L4A3 with two steel barrels for [Royal Navy](https://en.wikipedia.org/wiki/Royal_Navy "Royal Navy")

L4A6

L4A1 variant with one chromium-plated steel barrel

L4A7

Conversion of MK1 Bren. None made but drawings prepared for overseas buyer

L4A9

Bren conversion with L7 dovetail

### Taden gun

Main article: [Taden gun](https://en.wikipedia.org/wiki/Taden_gun "Taden gun")

The [Taden gun](https://en.wikipedia.org/wiki/Taden_gun "Taden gun") was a development of the Bren to use with the [.280 British](https://en.wikipedia.org/wiki/.280_British ".280 British") (7 mm) intermediate round proposed to replace the .303 in British service. The Taden was belt-fed with spade grips and would have replaced both the Bren and the Vickers machine gun. Although reliable[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] it was not accepted due to the US-driven standardization within NATO on the larger [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO") round.[[35]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-39)

### Semiautomatic Bren guns

Many nations' militaries have disposed of their Bren guns as surplus to their needs. Surplus Brens have been imported to the United States for sale to collectors, but due to US gun laws restricting the importation of automatic weapons such guns must be legally destroyed by cutting up the receivers. A number of US gunsmiths have manufactured new semiautomatic Brens by welding the pieces of destroyed receivers back together, with modifications to prevent the use of full automatic parts, and fitting new fire-control components capable of only semiautomatic fire. The balance of the parts are surplus Bren parts. Such "semiautomatic machine guns" are legally considered rifles under US Federal law and the laws of most states.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

## Production

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/VeronicaFoster-RonnieBrenGunGirl-smoke.jpg/220px-VeronicaFoster-RonnieBrenGunGirl-smoke.jpg)](https://en.wikipedia.org/wiki/File:VeronicaFoster-RonnieBrenGunGirl-smoke.jpg)

[Veronica Foster](https://en.wikipedia.org/wiki/Veronica_Foster "Veronica Foster") as "Ronnie, the Bren Gun Girl", was a Canadian icon for women working in war production

United Kingdom

Bren guns were produced at the [Royal Small Arms Factory](https://en.wikipedia.org/wiki/Royal_Small_Arms_Factory "Royal Small Arms Factory"), in [Enfield, London](https://en.wikipedia.org/wiki/Enfield,_London "Enfield, London").[[36]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEHobart19727-40) The first Bren guns were built in September 1937, and by December, a total of 42 had been produced. Weekly production was 300 Brens a week in July 1938, and 400 a week in September 1939.[[37]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEHobart19729-41) The Monotype Group[[a]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-42) produced Mark 2 Brens.[[38]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-43) Enfield produced a total of 220,000 Mark I Bren guns,[[39]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDugelbyStevens1999114-44) 57,600 in Mark III,[[40]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDugelbyStevens1999171-45) and 250 in Mark IV.[[41]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDugelbyStevens1999174-46)

Canada

[John Inglis and Company](https://en.wikipedia.org/wiki/John_Inglis_and_Company "John Inglis and Company") received a contract from the British and Canadian governments in March 1938 to supply 5,000 Bren machine guns to the UK and 7,000 Bren machine guns to Canada. Both countries shared the [capital](https://en.wikipedia.org/wiki/Capital_(economics) "Capital (economics)") costs of bringing in this new production facility.[[42]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-47) Production started in 1940; by August 1942, the Inglis plant was averaging 10,000 Brens per month, and produced 186,000 Bren guns of all variants by the end of the war, including 43,000 chambered in [7.92×57mm Mauser](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser "7.92×57mm Mauser") for export to the Chinese [National Revolutionary Army](https://en.wikipedia.org/wiki/National_Revolutionary_Army "National Revolutionary Army").[[43]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201322-48)[[44]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEDugelbyStevens1999244-247-49)

India

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Keeping_Watch.jpg/220px-Keeping_Watch.jpg)](https://en.wikipedia.org/wiki/File:Keeping_Watch.jpg)

Indian army soldier armed with Indian version of Bren LMG rechambered to fire 7.62 mm

In 1942, the [Ishapore Arsenal](https://en.wikipedia.org/wiki/Ishapore_Arsenal "Ishapore Arsenal") began to produce Bren guns, and continued to do so long after the end of World War II, also manufacturing variants in [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO").[[43]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201322-48) A shadow factory for Ishapore was set up at [Kanpur](https://en.wikipedia.org/wiki/Kanpur "Kanpur") and produced .303 Brens before it was later rechambered to fire 7.62 NATO ammo in 1964[[45]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-50)[[46]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-51) as the 1A LMG.[[47]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-52)

Australia

In 1940, the [Lithgow Small Arms Factory](https://en.wikipedia.org/wiki/Lithgow_Small_Arms_Factory "Lithgow Small Arms Factory") in [New South Wales](https://en.wikipedia.org/wiki/New_South_Wales "New South Wales") began to manufacture Bren guns, producing a total of 17,249 by 1945.[[43]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201322-48)

## Users

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Member_of_the_FFI.jpg/170px-Member_of_the_FFI.jpg)](https://en.wikipedia.org/wiki/File:Member_of_the_FFI.jpg)

Watched by two small boys, a member of the FFI ([French Forces of the Interior](https://en.wikipedia.org/wiki/French_Forces_of_the_Interior "French Forces of the Interior")), poses with his Bren at [Châteaudun](https://en.wikipedia.org/wiki/Ch%C3%A2teaudun "Châteaudun"), 1944.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Monument_of_the_Martyrs_05_Algiers.jpg/170px-Monument_of_the_Martyrs_05_Algiers.jpg)](https://en.wikipedia.org/wiki/File:Monument_of_the_Martyrs_05_Algiers.jpg)

One of the soldier statues of the Algerian [Martyrs' Memorial](https://en.wikipedia.org/wiki/Martyrs%27_Memorial,_Algiers "Martyrs' Memorial, Algiers") carries a Bren machine gun

-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Algeria.svg/23px-Flag_of_Algeria.svg.png) [Algeria](https://en.wikipedia.org/wiki/Algeria "Algeria"): [National Liberation Army](https://en.wikipedia.org/wiki/National_Liberation_Army_(Algeria) "National Liberation Army (Algeria)") received 500 Brens from Egypt[[48]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-53)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/23px-Flag_of_Australia_%28converted%29.svg.png) [Australia](https://en.wikipedia.org/wiki/Australia "Australia"): during World War II and Korean War. [Indonesia–Malaysia confrontation](https://en.wikipedia.org/wiki/Indonesia%E2%80%93Malaysia_confrontation "Indonesia–Malaysia confrontation") (as L4A4).[[49]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-54) Continued in limited service until about the time of the general introduction of the [F88 Steyr](https://en.wikipedia.org/wiki/F88_Steyr "F88 Steyr") (ca 1990).[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Flag_of_Bangladesh.svg/23px-Flag_of_Bangladesh.svg.png) [Bangladesh](https://en.wikipedia.org/wiki/Bangladesh "Bangladesh")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Flag_of_Barbados.svg/23px-Flag_of_Barbados.svg.png) [Barbados](https://en.wikipedia.org/wiki/Barbados "Barbados")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Flag_of_Belize.svg/23px-Flag_of_Belize.svg.png) [Belize](https://en.wikipedia.org/wiki/Belize "Belize")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_Belgium_%28civil%29.svg/23px-Flag_of_Belgium_%28civil%29.svg.png) [Belgium](https://en.wikipedia.org/wiki/Belgium "Belgium"): post-war[[51]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969212-56)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Flag_of_Biafra.svg/23px-Flag_of_Biafra.svg.png) [Biafra](https://en.wikipedia.org/wiki/Biafra "Biafra")[[52]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-auto-57)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_Botswana.svg/23px-Flag_of_Botswana.svg.png) [Botswana](https://en.wikipedia.org/wiki/Botswana "Botswana")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Bulgaria.svg/23px-Flag_of_Bulgaria.svg.png) [Kingdom of Bulgaria](https://en.wikipedia.org/wiki/Kingdom_of_Bulgaria "Kingdom of Bulgaria"): received Czech-made Brens in [8×56mmR](https://en.wikipedia.org/wiki/8%C3%9756mmR "8×56mmR"), designated **kartěnice 39**[[53]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-58)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Flag_of_Canada_%281921%E2%80%931957%29.svg/23px-Flag_of_Canada_%281921%E2%80%931957%29.svg.png) [Canada](https://en.wikipedia.org/wiki/Canada "Canada")[[54]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969285-59)
-   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Flag_of_Frolinat.svg/21px-Flag_of_Frolinat.svg.png) [Chadian](https://en.wikipedia.org/wiki/Chad "Chad") [FROLINAT](https://en.wikipedia.org/wiki/FROLINAT "FROLINAT")[[55]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-60)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Flag_of_the_Central_African_Republic.svg/23px-Flag_of_the_Central_African_Republic.svg.png) [Central African Republic](https://en.wikipedia.org/wiki/Central_African_Republic "Central African Republic"): used by [Gendarmerie](https://en.wikipedia.org/wiki/Law_enforcement_in_the_Central_African_Republic "Law enforcement in the Central African Republic") and [Republican Guard](https://en.wikipedia.org/wiki/Republican_Guard_(Central_African_Republic) "Republican Guard (Central African Republic)")[[56]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-61)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_Croatia.svg/23px-Flag_of_Croatia.svg.png) [Croatia](https://en.wikipedia.org/wiki/Croatia "Croatia"): Mark 2 version known to be used in the [Croatian War of Independence](https://en.wikipedia.org/wiki/Croatian_War_of_Independence "Croatian War of Independence").[[57]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-62)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/23px-Flag_of_the_People%27s_Republic_of_China.svg.png) [People's Republic of China](https://en.wikipedia.org/wiki/China "China"): Many guns captured from [Kuomintang](https://en.wikipedia.org/wiki/Kuomintang "Kuomintang"). Used during [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War").[[58]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201352-63) Some converted to fire 7.62x39 caliber ammunition from ammo supplied by their Soviet allies. They used regular AK-47 magazines.[[33]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201324-35)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Flag_of_the_Republic_of_China.svg/23px-Flag_of_the_Republic_of_China.svg.png) [Republic of China](https://en.wikipedia.org/wiki/Republic_of_China_(1912%E2%80%931949) "Republic of China (1912–1949)"): used by [National Revolutionary Army](https://en.wikipedia.org/wiki/National_Revolutionary_Army "National Revolutionary Army")[[59]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant20136-64) 43,000 guns produced in [7.92×57mm Mauser](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser "7.92×57mm Mauser") by [Inglis](https://en.wikipedia.org/wiki/John_Inglis_and_Company "John Inglis and Company") in Canada.[[60]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201322&24-65) Later in 1952, [Taiwan](https://en.wikipedia.org/wiki/Taiwan "Taiwan") produced a [.30-06 Springfield](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield") version of Bren Mk II, the _Type 41_.[[61]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969293&296-66)[[33]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201324-35)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Flag_of_the_Democratic_Republic_of_the_Congo.svg/20px-Flag_of_the_Democratic_Republic_of_the_Congo.svg.png) [Democratic Republic of Congo](https://en.wikipedia.org/wiki/Democratic_Republic_of_the_Congo "Democratic Republic of the Congo")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Flag_of_the_Central_African_Republic.svg/23px-Flag_of_the_Central_African_Republic.svg.png) [Central African Republic](https://en.wikipedia.org/wiki/Central_African_Republic "Central African Republic")[[62]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-67)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Cyprus.svg/23px-Flag_of_Cyprus.svg.png) [Cyprus](https://en.wikipedia.org/wiki/Cyprus "Cyprus")[[63]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-68)[[64]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-69)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Flag_of_Denmark.svg/20px-Flag_of_Denmark.svg.png) [Denmark](https://en.wikipedia.org/wiki/Denmark "Denmark"): post-war[[65]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969337-70)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Flag_of_the_United_Arab_Republic_%281958%E2%80%931971%29.svg/23px-Flag_of_the_United_Arab_Republic_%281958%E2%80%931971%29.svg.png) [Egypt](https://en.wikipedia.org/wiki/Egypt "Egypt")[[66]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969613-71)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/23px-Flag_of_France.svg.png) [France](https://en.wikipedia.org/wiki/France "France")
    -    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Flag_of_Free_France_%281940-1944%29.svg/23px-Flag_of_Free_France_%281940-1944%29.svg.png) [Free France](https://en.wikipedia.org/wiki/Free_France "Free France"): Used by the [Free French Forces](https://en.wikipedia.org/wiki/Free_French_Forces "Free French Forces") and [French Resistance](https://en.wikipedia.org/wiki/French_Resistance "French Resistance").[[67]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969372-72)[[68]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-73)
    -    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Flag_of_France_%281794%E2%80%931815%2C_1830%E2%80%931958%29.svg/23px-Flag_of_France_%281794%E2%80%931815%2C_1830%E2%80%931958%29.svg.png) [Vichy France](https://en.wikipedia.org/wiki/Vichy_France "Vichy France"): Captured Brens were issued to the [Milice](https://en.wikipedia.org/wiki/Milice "Milice").[[69]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-74)
    -   [French Far East Expeditionary Corps](https://en.wikipedia.org/wiki/French_Far_East_Expeditionary_Corps "French Far East Expeditionary Corps")[[70]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Indochina-75)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_The_Gambia.svg/23px-Flag_of_The_Gambia.svg.png) [Gambia](https://en.wikipedia.org/wiki/The_Gambia "The Gambia")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Ghana.svg/23px-Flag_of_Ghana.svg.png) [Ghana](https://en.wikipedia.org/wiki/Ghana "Ghana")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Greece.svg/23px-Flag_of_Greece.svg.png) [Greece](https://en.wikipedia.org/wiki/Greece "Greece")[[71]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-76)[[72]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969450-77)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Flag_of_Guyana.svg/23px-Flag_of_Guyana.svg.png) [Guyana](https://en.wikipedia.org/wiki/Guyana "Guyana")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Flag_of_Hong_Kong_%281959%E2%80%931997%29.svg/23px-Flag_of_Hong_Kong_%281959%E2%80%931997%29.svg.png) [Hong Kong](https://en.wikipedia.org/wiki/Hong_Kong "Hong Kong"): Used by the [Royal Hong Kong Regiment](https://en.wikipedia.org/wiki/Royal_Hong_Kong_Regiment "Royal Hong Kong Regiment").[[73]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-RHKR_Weapon-78)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/23px-Flag_of_India.svg.png) [India](https://en.wikipedia.org/wiki/India "India"): manufactured by the [Ordnance Factories Board](https://en.wikipedia.org/wiki/Ordnance_Factories_Board "Ordnance Factories Board")[[4]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Ordnance_Factory_Board-4)[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_] Phased out of Indian military service in 2012.[[5]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201366-5)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/23px-Flag_of_Indonesia.svg.png) [Indonesia](https://en.wikipedia.org/wiki/Indonesia "Indonesia"):[[74]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-79)[[75]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969461-80)
-   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Flag_of_Iraq_%281924%E2%80%931959%29.svg/23px-Flag_of_Iraq_%281924%E2%80%931959%29.svg.png) [Iraq](https://en.wikipedia.org/wiki/Kingdom_of_Iraq "Kingdom of Iraq")[[76]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-81)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Flag_of_Ireland.svg/23px-Flag_of_Ireland.svg.png) [Ireland](https://en.wikipedia.org/wiki/Republic_of_Ireland "Republic of Ireland"): [Irish Defence Forces](https://en.wikipedia.org/wiki/Irish_Defence_Forces "Irish Defence Forces"), replaced by the [FN MAG](https://en.wikipedia.org/wiki/FN_MAG "FN MAG") in 1960s.[[77]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201365-82) Remained in use with Irish military reserve forces until the early 2000s.
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/21px-Flag_of_Israel.svg.png) [Israel](https://en.wikipedia.org/wiki/Israel "Israel"): During the [1948 Arab–Israeli War](https://en.wikipedia.org/wiki/1948_Arab%E2%80%93Israeli_War "1948 Arab–Israeli War") and for some time thereafter by the [Haganah](https://en.wikipedia.org/wiki/Haganah "Haganah") and the [Israeli Defense Forces](https://en.wikipedia.org/wiki/Israeli_Defense_Forces "Israeli Defense Forces"). Replaced after [Operation Kadesh](https://en.wikipedia.org/wiki/Operation_Kadesh "Operation Kadesh") (1953).[[78]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201364-83)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/0/03/Flag_of_Italy.svg/23px-Flag_of_Italy.svg.png) [Italy](https://en.wikipedia.org/wiki/Italy "Italy"): [airdropped](https://en.wikipedia.org/wiki/Airdrop "Airdrop") to [partisans](https://en.wikipedia.org/wiki/Resistenza "Resistenza") and also issued to the [Italian Co-Belligerent Army](https://en.wikipedia.org/wiki/Italian_Co-Belligerent_Army "Italian Co-Belligerent Army") in the latter part of WWII. It continued to see post-war use with the Italian Army.[[79]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-ArmiEMezzi1955-84) Also used by the [Italian Police](https://en.wikipedia.org/wiki/Polizia_di_Stato "Polizia di Stato") in .30-06 caliber.[[33]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201324-35)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Flag_of_Jamaica.svg/23px-Flag_of_Jamaica.svg.png) [Jamaica](https://en.wikipedia.org/wiki/Jamaica "Jamaica")[[5]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201366-5)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_Japan_%281870%E2%80%931999%29.svg/22px-Flag_of_Japan_%281870%E2%80%931999%29.svg.png) [Empire of Japan](https://en.wikipedia.org/wiki/Empire_of_Japan "Empire of Japan"): captured weapons.[[80]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969498-85)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Flag_of_Jordan.svg/23px-Flag_of_Jordan.svg.png) [Jordan](https://en.wikipedia.org/wiki/Jordan "Jordan"): [Arab Legion](https://en.wikipedia.org/wiki/Arab_Legion "Arab Legion")[[81]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-86)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Kenya.svg/23px-Flag_of_Kenya.svg.png) [Kenya](https://en.wikipedia.org/wiki/Kenya "Kenya")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Flag_of_North_Korea.svg/23px-Flag_of_North_Korea.svg.png) [North Korea](https://en.wikipedia.org/wiki/North_Korea "North Korea")[[82]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-87)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Flag_of_Lesotho.svg/23px-Flag_of_Lesotho.svg.png) [Lesotho](https://en.wikipedia.org/wiki/Lesotho "Lesotho")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Libya_%281951%E2%80%931969%29.svg/23px-Flag_of_Libya_%281951%E2%80%931969%29.svg.png) [Libya](https://en.wikipedia.org/wiki/Libya "Libya")[[25]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201326-25)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Flag_of_Luxembourg.svg/23px-Flag_of_Luxembourg.svg.png) [Luxembourg](https://en.wikipedia.org/wiki/Luxembourg "Luxembourg")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Flag_of_Malaysia.svg/23px-Flag_of_Malaysia.svg.png) [Malaysia](https://en.wikipedia.org/wiki/Malaysia "Malaysia")[[83]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-88)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/23px-Flag_of_Myanmar.svg.png) [Myanmar](https://en.wikipedia.org/wiki/Myanmar "Myanmar")[[84]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-89)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Mauritius.svg/23px-Flag_of_Mauritius.svg.png) [Mauritius](https://en.wikipedia.org/wiki/Mauritius "Mauritius")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Germany_%281935%E2%80%931945%29.svg/23px-Flag_of_Germany_%281935%E2%80%931945%29.svg.png) [Nazi Germany](https://en.wikipedia.org/wiki/Nazi_Germany "Nazi Germany"): used captured examples[[85]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-90) under the designation _7.7 mm Leichtes MG 138(e)_[[77]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201365-82)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Flag_of_Nepal.svg/16px-Flag_of_Nepal.svg.png) [Nepal](https://en.wikipedia.org/wiki/Nepal "Nepal"):[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_] Bren L4[[86]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Nepal-91)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/23px-Flag_of_the_Netherlands.svg.png) [Netherlands](https://en.wikipedia.org/wiki/Netherlands "Netherlands"): post-war[[87]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969521-92)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/23px-Flag_of_New_Zealand.svg.png) [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand"): WWII and L4 post war[[88]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-93)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Flag_of_Nigeria.svg/23px-Flag_of_Nigeria.svg.png) [Nigeria](https://en.wikipedia.org/wiki/Nigeria "Nigeria")[[89]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJowett201620-94)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Norway.svg/21px-Flag_of_Norway.svg.png) [Norway](https://en.wikipedia.org/wiki/Norway "Norway"): post-war[[90]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969523-95)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Pakistan.svg/23px-Flag_of_Pakistan.svg.png) [Pakistan](https://en.wikipedia.org/wiki/Pakistan "Pakistan")[[5]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201366-5)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Flag_of_Papua_New_Guinea.svg/20px-Flag_of_Papua_New_Guinea.svg.png) [Papua New Guinea](https://en.wikipedia.org/wiki/Papua_New_Guinea "Papua New Guinea")[[91]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Alpers-96)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/1/12/Flag_of_Poland.svg/23px-Flag_of_Poland.svg.png) [Poland](https://en.wikipedia.org/wiki/Poland "Poland"): Used by the [Polish Underground State](https://en.wikipedia.org/wiki/Polish_Underground_State "Polish Underground State")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] and [Polish Armed Forces in the West](https://en.wikipedia.org/wiki/Polish_Armed_Forces_in_the_West "Polish Armed Forces in the West")[[92]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-97) during World War II.
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/23px-Flag_of_Portugal.svg.png) [Portugal](https://en.wikipedia.org/wiki/Portugal "Portugal"): _m/43_[[93]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTESmith1969530-98)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Flag_of_Rhodesia_%281968%E2%80%931979%29.svg/23px-Flag_of_Rhodesia_%281968%E2%80%931979%29.svg.png) [Rhodesia](https://en.wikipedia.org/wiki/Rhodesia "Rhodesia")[[94]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-99)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Seychelles.svg/23px-Flag_of_Seychelles.svg.png) [Seychelles](https://en.wikipedia.org/wiki/Seychelles "Seychelles")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Flag_of_Sierra_Leone.svg/23px-Flag_of_Sierra_Leone.svg.png) [Sierra Leone](https://en.wikipedia.org/wiki/Sierra_Leone "Sierra Leone")[[95]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-100)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_South_Africa_%281928%E2%80%931994%29.svg/23px-Flag_of_South_Africa_%281928%E2%80%931994%29.svg.png) [South Africa](https://en.wikipedia.org/wiki/South_Africa "South Africa")[[31]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Captor-31)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Flag_of_Sri_Lanka.svg/23px-Flag_of_Sri_Lanka.svg.png) [Sri Lanka](https://en.wikipedia.org/wiki/Sri_Lanka "Sri Lanka"): Used by [Ceylon Defence Force](https://en.wikipedia.org/wiki/Ceylon_Defence_Force "Ceylon Defence Force") in World War II[[96]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-101)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Flag_of_Suriname.svg/23px-Flag_of_Suriname.svg.png) [Suriname](https://en.wikipedia.org/wiki/Suriname "Suriname")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Flag_of_Eswatini.svg/23px-Flag_of_Eswatini.svg.png) [Swaziland](https://en.wikipedia.org/wiki/Eswatini "Eswatini")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Flag_of_Tibet.svg/23px-Flag_of_Tibet.svg.png) [Tibet](https://en.wikipedia.org/wiki/Tibet "Tibet"): 294 guns were purchased by the [Tibetan Army](https://en.wikipedia.org/wiki/Tibetan_Army "Tibetan Army") in 1950.[[97]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Shakya-102)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Tonga.svg/23px-Flag_of_Tonga.svg.png) [Tonga](https://en.wikipedia.org/wiki/Tonga "Tonga")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_][[98]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Capie-103)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Flag_of_Trinidad_and_Tobago.svg/23px-Flag_of_Trinidad_and_Tobago.svg.png) [Trinidad and Tobago](https://en.wikipedia.org/wiki/Trinidad_and_Tobago "Trinidad and Tobago")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Flag_of_Uganda.svg/23px-Flag_of_Uganda.svg.png) [Uganda](https://en.wikipedia.org/wiki/Uganda "Uganda")[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/23px-Flag_of_the_United_Kingdom.svg.png) [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom"): British and Commonwealth forces, and cadet forces until the introduction of the [L98 Cadet Rifle](https://en.wikipedia.org/wiki/L98_Cadet_Rifle "L98 Cadet Rifle")
-   [![South Vietnam](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Flag_of_South_Vietnam.svg/23px-Flag_of_South_Vietnam.svg.png)](https://en.wikipedia.org/wiki/South_Vietnam "South Vietnam") [State of Vietnam](https://en.wikipedia.org/wiki/State_of_Vietnam "State of Vietnam")[[70]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-Indochina-75)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Vietnam.svg/23px-Flag_of_Vietnam.svg.png) [Vietnam](https://en.wikipedia.org/wiki/Vietnam "Vietnam"): used by [Viet-Minh](https://en.wikipedia.org/wiki/Viet-Minh "Viet-Minh")[[99]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEWindrow199824-104) acquired from China and the Soviet Union or by capture
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_the_Soviet_Union.svg/23px-Flag_of_the_Soviet_Union.svg.png) [Soviet Union](https://en.wikipedia.org/wiki/Soviet_Union "Soviet Union"): Supplied by the United Kingdom during the Lend-Lease program.[[100]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201345-105)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Flag_of_Yugoslavia_%281946-1992%29.svg/23px-Flag_of_Yugoslavia_%281946-1992%29.svg.png) [Yugoslavia](https://en.wikipedia.org/wiki/Socialist_Federal_Republic_of_Yugoslavia "Socialist Federal Republic of Yugoslavia"): [Yugoslav Partisans](https://en.wikipedia.org/wiki/Yugoslav_Partisans "Yugoslav Partisans") and [Chetniks](https://en.wikipedia.org/wiki/Chetniks "Chetniks") during World War II;[[101]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEGrant201343-106)[[102]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-107)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Flag_of_Zimbabwe.svg/23px-Flag_of_Zimbabwe.svg.png) [Zimbabwe](https://en.wikipedia.org/wiki/Zimbabwe "Zimbabwe"):[[50]](https://en.wikipedia.org/wiki/Bren_light_machine_gun#cite_note-FOOTNOTEJonesNess2009-55)[_[page needed](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources")_]

## Gallery

-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Bundesarchiv_Bild_101I-720-0318-04%2C_Frankreich%2C_Parade_der_Milice_Francaise.jpg/351px-Bundesarchiv_Bild_101I-720-0318-04%2C_Frankreich%2C_Parade_der_Milice_Francaise.jpg)](https://en.wikipedia.org/wiki/File:Bundesarchiv_Bild_101I-720-0318-04,_Frankreich,_Parade_der_Milice_Francaise.jpg)
    
    Members of the [Milice](https://en.wikipedia.org/wiki/Milice "Milice") with captured Bren guns.
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/8th_Royal_Scots_halted_27-10-1944.jpg/240px-8th_Royal_Scots_halted_27-10-1944.jpg)](https://en.wikipedia.org/wiki/File:8th_Royal_Scots_halted_27-10-1944.jpg)
    
    Bren near [Tilburg](https://en.wikipedia.org/wiki/Tilburg "Tilburg"), October 1944
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Twin_Bren_gun_anti-aircraft_mounting_at_RAF_Duxford.jpg/173px-Twin_Bren_gun_anti-aircraft_mounting_at_RAF_Duxford.jpg)](https://en.wikipedia.org/wiki/File:Twin_Bren_gun_anti-aircraft_mounting_at_RAF_Duxford.jpg)
    
    Twin Bren anti-aircraft mounting
    
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/The_British_Army_in_the_United_Kingdom_1939-45_H37157.jpg/242px-The_British_Army_in_the_United_Kingdom_1939-45_H37157.jpg)](https://en.wikipedia.org/wiki/File:The_British_Army_in_the_United_Kingdom_1939-45_H37157.jpg)
    
    [Universal Carrier](https://en.wikipedia.org/wiki/Universal_Carrier "Universal Carrier") with Vickers machine gun and two Brens
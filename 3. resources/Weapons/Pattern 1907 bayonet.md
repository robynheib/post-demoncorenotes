# Pattern 1907 bayonet

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#mw-head) [Jump to search](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#searchInput)

Sword bayonet, Pattern 1907

[![Bayonet, knife-sword (and scabbard) (AM 697056-1).jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Bayonet%2C_knife-sword_%28and_scabbard%29_%28AM_697056-1%29.jpg/300px-Bayonet%2C_knife-sword_%28and_scabbard%29_%28AM_697056-1%29.jpg)](https://en.wikipedia.org/wiki/File:Bayonet,_knife-sword_(and_scabbard)_(AM_697056-1).jpg)

Pattern 1907 bayonet with scabbard.

Type

[Bayonet](https://en.wikipedia.org/wiki/Bayonet "Bayonet")

Place of origin

[United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom_of_Great_Britain_and_Ireland "United Kingdom of Great Britain and Ireland")

Service history

In service

20th century

Used by

[British Empire](https://en.wikipedia.org/wiki/British_Empire "British Empire")

Wars

[World War I](https://en.wikipedia.org/wiki/World_War_I "World War I")  
[World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")

Production history

Designed

1906–1907

Manufacturer

James A. Chapman,  
Robert Mole & Sons,  
Sanderson Bros & Newbould Ltd,  
[Vickers Ltd](https://en.wikipedia.org/wiki/Vickers_Ltd "Vickers Ltd") and  
[Wilkinson Sword](https://en.wikipedia.org/wiki/Wilkinson_Sword "Wilkinson Sword")

Produced

1908–1945

No. built

More than 5,000,000

Specifications

Mass

16+1⁄2 oz (470 g)

Length

21+3⁄4 in (550 mm)

Blade length

17 in (430 mm)

References

[Australian War Memorial](https://en.wikipedia.org/wiki/Australian_War_Memorial "Australian War Memorial")[[1]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-AWM-1) and  
Ballard & Bennett[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)

The **Pattern 1907 bayonet**, officially called the **Sword bayonet, pattern 1907 (Mark I)**, was a British [bayonet](https://en.wikipedia.org/wiki/Bayonet "Bayonet") designed to be used with the [Short Magazine Lee Enfield](https://en.wikipedia.org/wiki/Short_Magazine_Lee_Enfield "Short Magazine Lee Enfield") (SMLE) rifle. The Pattern 1907 bayonet was used by the British and Commonwealth forces throughout both the [First](https://en.wikipedia.org/wiki/First_World_War "First World War") and [Second World Wars](https://en.wikipedia.org/wiki/Second_World_War "Second World War").

## Contents

-   [1 Design](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#Design)
    -   [1.1 Markings](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#Markings)
-   [2 History](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#History)
-   [3 Variants](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#Variants)
    -   [3.1 Pattern 1913 bayonet](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#Pattern_1913_bayonet)
    -   [3.2 Model 1917 bayonet](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#Model_1917_bayonet)
    -   [3.3 India Pattern bayonets](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#India_Pattern_bayonets)
-   [4 See also](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#See_also)
-   [5 References](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#References)

## Design

The Pattern 1907 bayonet consisted of a one-piece steel [blade](https://en.wikipedia.org/wiki/Blade "Blade") and [tang](https://en.wikipedia.org/wiki/Tang_(tools) "Tang (tools)"), with a [crossguard](https://en.wikipedia.org/wiki/Crossguard "Crossguard") and [pommel](https://en.wikipedia.org/wiki/Pommel_(sword) "Pommel (sword)") made from [wrought iron](https://en.wikipedia.org/wiki/Wrought_iron "Wrought iron") or [mild steel](https://en.wikipedia.org/wiki/Mild_steel "Mild steel"), and a wooden [grip](https://en.wikipedia.org/wiki/Grip_(sword) "Grip (sword)") usually of walnut secured to the tang by two screws. The entire bayonet was 21+3⁄4 inches (550 mm) long and weighed 16+1⁄2 ounces (470 g), although the weight of production models varied from 16 to 18 ounces (450 to 510 g). Originally the bayonet featured a hooked lower quillion intended for trapping an enemy's bayonet and possibly disarming opponents when grappling. This was later deemed impractical and replaced with a simpler design from 1913. Often unit armourers subsequently removed the hooked quillion when the bayonet was sent for repair, although there is no evidence that this was officially directed.[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)[[3]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-McNab-3)

The Pattern 1907 bayonet's blade was 17 inches (430 mm) long. A shallow [fuller](https://en.wikipedia.org/wiki/Fuller_(weapon) "Fuller (weapon)") was machined into both sides of the blade, 12 inches (300 mm) long and extending to within 3 inches (76 mm) of the tip, with variations due to the judgement of individual machinists.[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)[[3]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-McNab-3)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Bayonet%2C_knife-sword_%28and_scabbard%29_%28AM_697055-4%29.jpg/200px-Bayonet%2C_knife-sword_%28and_scabbard%29_%28AM_697055-4%29.jpg)](https://en.wikipedia.org/wiki/File:Bayonet,_knife-sword_(and_scabbard)_(AM_697055-4).jpg)

Original hooked quillion of the Pattern 1907 bayonet.

The Pattern 1907 bayonet was supplied with a simple leather [scabbard](https://en.wikipedia.org/wiki/Scabbard "Scabbard") flitted with a steel top-mount and [chape](https://en.wikipedia.org/wiki/Chape "Chape"), and usually carried from the belt by a simple frog. The Pattern 1907 bayonet attached to the SMLE by a boss located below the barrel on the nose of the rifle and a mortise groove on the pommel of the bayonet.[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)[[4]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Doyle&Foster-4)

The combined length of the SMLE and Pattern 1907 bayonet was 5 feet 2 inches (1.57 m).[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)

### Markings

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/SMLE_rifle_with_bayonet_fixed.jpg/250px-SMLE_rifle_with_bayonet_fixed.jpg)](https://en.wikipedia.org/wiki/File:SMLE_rifle_with_bayonet_fixed.jpg)

Pattern 1907 bayonet fitted to SMLE rifle.

Official marks were stamped onto the Pattern 1907 bayonet's [ricasso](https://en.wikipedia.org/wiki/Ricasso "Ricasso"). On British manufactured bayonets the right side included an 'X' bend-test mark, a [broad arrow](https://en.wikipedia.org/wiki/Broad_arrow "Broad arrow") government acceptance mark, and one or more [Royal Small Arms Factory](https://en.wikipedia.org/wiki/Royal_Small_Arms_Factory "Royal Small Arms Factory") appointed inspector's marks, on the left side was the date of the bayonet's official inspection and the maker's name and the reigning monarch's crown and [royal cypher](https://en.wikipedia.org/wiki/Royal_cypher "Royal cypher"), 'ER' ([Edward Rex](https://en.wikipedia.org/wiki/Edward_VII "Edward VII")) or after 1910 'GR' ([George Rex](https://en.wikipedia.org/wiki/George_V "George V")).[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)

Indian bayonets were marked similarly to British bayonets except the royal cypher read 'GRI' (George Rex Imperator) and the manufacturer's mark was 'R.F.I.' ([Rifle Factory Ishapore](https://en.wikipedia.org/wiki/Rifle_Factory_Ishapore "Rifle Factory Ishapore")). Australian bayonets differed in the manufacturer's marks, with 'Lithgow' ([Lithgow Small Arms Factory](https://en.wikipedia.org/wiki/Lithgow_Small_Arms_Factory "Lithgow Small Arms Factory")), 'MA' (Mangrovite Arsenal) and 'OA' (Orange Arsenal). The wooden grips of World War II era Australian bayonets were often marked 'SLAZ' for [Slazenger](https://en.wikipedia.org/wiki/Slazenger "Slazenger"), who made the grips during that war.[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)

## History

When the British military adopted the Short Magazine Lee-Enfield rifle, its barrel was shortened to 25.2 inches (640 mm), 5 inches (130 mm) shorter than the preceding Magazine Lee-Enfield. British military strategists were fearful that the British infantry would be at a disadvantage when engaged in a bayonet duel with enemy soldiers who retained a longer reach. Bayonet fighting drills formed a significant part of a contemporary British infantryman’s training. Soldiers were drilled in various stances and parrying techniques against an enemy also armed with rifle and bayonet. The combined length of the SMLE and the in-service Pattern 1903 bayonet, which had a 12-inch (300 mm) blade, was 4 feet 9 inches (1.45 m), shorter than the contemporary French [Lebel Model 1886](https://en.wikipedia.org/wiki/Lebel_Model_1886_rifle "Lebel Model 1886 rifle") at 6 feet (1.8 m) and the German [Mauser 1898](https://en.wikipedia.org/wiki/Gewehr_98 "Gewehr 98") at 5 feet 10 inches (1.77 m).[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)[[4]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Doyle&Foster-4)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/77-127-B_Bayonet%2C_Japanese%2C_Type_30.jpg/200px-77-127-B_Bayonet%2C_Japanese%2C_Type_30.jpg)](https://en.wikipedia.org/wiki/File:77-127-B_Bayonet,_Japanese,_Type_30.jpg)

Japanese Ariska, Type 30 bayonet.

In 1906–7 the British Army conducted trials to find a new longer standard issue bayonet. Experiments were conducted with a number of foreign bayonet designs, including a modified version of the American [Model 1905 bayonet](https://en.wikipedia.org/wiki/M1905_bayonet "M1905 bayonet") and the Japanese [Type 30 bayonet](https://en.wikipedia.org/wiki/Type_30_bayonet "Type 30 bayonet"). The trials resulted in the British Army adopting its own version of the Type 30 bayonet. The new design was designated **Sword bayonet, pattern 1907 (Mark I)** and was officially introduced on 30 January 1908.[[1]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-AWM-1)[[2]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Ballard&Bennett-2)[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)[[6]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-royalarmouries-6)

Approximately 5,000,000 Pattern 1907 bayonets were made in Britain during World War I. The makers were [Wilkinson Sword](https://en.wikipedia.org/wiki/Wilkinson_Sword "Wilkinson Sword"), Sanderson Brothers & Newbould Ltd, James A. Chapman, Robert Mole & Sons, and [Vickers Ltd](https://en.wikipedia.org/wiki/Vickers_Ltd "Vickers Ltd"). Additionally, [Remington UMC](https://en.wikipedia.org/wiki/Remington_UMC "Remington UMC") produced approximately 100,000 during the war. The Pattern 1907 bayonet was manufactured in [India](https://en.wikipedia.org/wiki/India "India") from 1911 to 1940 at the Rifle Factory Ishapore and in [Australia](https://en.wikipedia.org/wiki/Australia "Australia") from 1913 to 1927, and then again between 1940 and 1945 at the [Lithgow Small Arms Factory](https://en.wikipedia.org/wiki/Lithgow_Small_Arms_Factory "Lithgow Small Arms Factory").[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)

The Pattern 1907 bayonet was adopted by most of the [British Commonwealth](https://en.wikipedia.org/wiki/British_Commonwealth "British Commonwealth") along with the SMLE. It saw broad front line service until 1945, seeing service in both [World War I](https://en.wikipedia.org/wiki/World_War_I "World War I") and [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II"). It remained in Australian and Indian service for some time after 1945.[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)[[7]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-IWM-7)

In 1926 the 1907 bayonet was reclassified as the 'Bayonet, No.1, Mk.1'

## Variants

### Pattern 1913 bayonet

The **Pattern 1913 bayonet** was designed to be used with the experimental [Pattern 1913 Enfield](https://en.wikipedia.org/wiki/Pattern_1913_Enfield "Pattern 1913 Enfield"). The Pattern 1913 bayonet's only functional difference from the Pattern 1907 bayonet was a longer cross guard for the muzzle ring, to fit the Pattern 1913 Enfield rifle. Upon the outbreak of World War I the British authorities adapted the Pattern 1913 Enfield to the [.303 British](https://en.wikipedia.org/wiki/.303_British ".303 British") cartridge, creating the [Pattern 1914 Enfield](https://en.wikipedia.org/wiki/Pattern_1914_Enfield "Pattern 1914 Enfield") rifle, and contracts were awarded to the United States arms manufacturers [Winchester](https://en.wikipedia.org/wiki/Winchester_Repeating_Arms_Company "Winchester Repeating Arms Company"), Remington and [Eddystone](https://en.wikipedia.org/wiki/Eddystone_Arsenal "Eddystone Arsenal") for the rifle's production. To accompany those rifles, Remington manufactured the 1,243,000 Pattern 1913 bayonets and Winchester produced 225,000.[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)[[8]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-AWM-1917-8)[[9]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-IWM-P1913-9)[[10]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cunningham-10)

The Pattern 1917 bayonet cannot be fixed to the Lee-Enfield rifle (because of the different muzzle ring heights), so to avoid confusion with the Pattern 1907 bayonet, two deep vertical grooves were cut into the wooden grips of the Pattern 1913 bayonet.[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)[[9]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-IWM-P1913-9)

### Model 1917 bayonet

Main article: [M1917 bayonet](https://en.wikipedia.org/wiki/M1917_bayonet "M1917 bayonet")

Upon their entry into World War I, the United States military adapted the Pattern 1914 Enfield rifle to the [.30-06 Springfield](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield") cartridge to make up for shortfalls in production of the [Model 1903 Springfield](https://en.wikipedia.org/wiki/M1903_Springfield "M1903 Springfield") rifles, creating the substitute standard [Model 1917 Enfield](https://en.wikipedia.org/wiki/M1917_Enfield "M1917 Enfield") rifle. To accompany the M1917 rifle, the United States simply adopted the Pattern 1913 bayonet as the **Model 1917 bayonet**. Over 2,000,000 Model 1917 bayonets were manufactured in the United States during the war, including 545,000 Pattern 1913 bayonets manufactured for but not delivered to the British military, that were simply re-stamped as Model 1917 bayonets. The Model 1917 bayonet was adopted unchanged to be used with United States Army [combat shotguns](https://en.wikipedia.org/wiki/Combat_shotgun "Combat shotgun"). After the war, the M1917 bayonet was retained for use with combat shotguns, and remained in United States service until the 1980s.[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)[[10]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cunningham-10)[[11]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Rottman-11)

### India Pattern bayonets

From 1941, India began cutting down Pattern 1907 bayonets to 12.2 in (310 mm) and grinding a point into the remaining blade, creating the **India Pattern No. 1 Mk. I***. The **India Pattern No. 1 Mk. I**** is almost identical except a false edge 2 in (51 mm) long is ground into the top of the blade. Both were recognisable by the fuller, which now ran the length of the blade. The **India Pattern No. 1 Mk. II** and the **India Pattern No. 1 Mk. II*** were newly manufactured versions with 12.2 inch blades that had no fuller, the latter having a false edge on top side. Both retained the Pattern 1907 hilt and grip. The **India Pattern No. 1 Mk. III** and the **India Pattern No. 1 Mk. III*** were similar to the No. 1 Mk. II and No. 1 Mk. II* except they had crude squared pommels and rectangular grips, and were finished with black paint.[[5]](https://en.wikipedia.org/wiki/Pattern_1907_bayonet#cite_note-Cobb-5)
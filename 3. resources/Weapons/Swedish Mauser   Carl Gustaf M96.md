  
# Swedish Mauser  
Carl Gustaf M96
Calibre 6.5x55mm  
Date: 1905  
Serial No: 168712  

## Very Good Condition

The Model 1896 rifle in 6.5×55mm (6,5 mm Gevär m/96) was adopted in 1896 for infantry use, replacing the Model 1867–1889 Remington rolling block rifle in 8×58mmR Danish Krag.

Swedish production started in 1898 at Carl Gustafs, but additional rifles were produced by Mauser during 1899 and 1900 because of delays in shipping additional production machinery from Germany to Sweden.

Standard production at Carl Gustafs continued until 1925, but approximately 18,000 m/96 rifles were manufactured by Husqvarna Vapenfabriks AB during World War II for civilian marksmanship training.

Carl Gustafs Stads Gevärsfaktori manufactured 475,000 m/1896 rifles between 1896 and 1925 & latter examples upto 1932 were assembled from existing spares

## This is a nice example of a Swedish manufactured m96 Mauser Rifle  
  
Original Walnut Stock  
  
The left side of the action is marked with the initials H.R. - Erik Herman Ribbing  
who was the Swedish Army inspection officer at the factory between 1 Jan 1903 - 31 Dec 1906

The butt has the water transfer decal painted over with protective shellac, the rear sight has been converted post war with the U-Notch  
Additionally the squared blade has been retrofitted, replacing the older "inverted V" blade.  
  
Thankfully this example has not had the muzzle converted for use with the "Blank Firing Adaptor"  
  
The magazine plate is unmodified & still acts as a bolt stop on an empty magazine

Rear ladder sight has "Metallverken Vasteras Micrometer" micrometer sight installed

Mounted between the rear trigger guard tang and the rear sling swivel mount is a wooden pistol grip on a rail.  
  
Also a removable sniper front sight protector has been fitted as well

## All matching with the exception of the cleaning rod

## This example is in Very Good Condition for a rifle that is 115 years old and has a good shooting grade bore

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/1.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/2.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/3.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/4.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/5.JPG)  

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/6.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/7.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/8.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/9.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/10.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/12.JPG)![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/11.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/13.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/14.JPG)![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/15.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/16.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/17.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/18.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/19.JPG)![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/20.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/22.JPG)![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/21.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/23.JPG)

![](https://www.highwoodclassicarms.co.uk/Firerms%20For%20Sale/0145/24.JPG)
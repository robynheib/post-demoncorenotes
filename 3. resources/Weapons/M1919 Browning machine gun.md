# M1919 Browning machine gun

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#mw-head) [Jump to search](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#searchInput)

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This article **needs additional citations for [verification](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and removed.  
_Find sources:_ ["M1919 Browning machine gun"](https://www.google.com/search?as_eq=wikipedia&q=%22M1919+Browning+machine+gun%22) – [news](https://www.google.com/search?tbm=nws&q=%22M1919+Browning+machine+gun%22+-wikipedia) **·** [newspapers](https://www.google.com/search?&q=%22M1919+Browning+machine+gun%22&tbs=bkt:s&tbm=bks) **·** [books](https://www.google.com/search?tbs=bks:1&q=%22M1919+Browning+machine+gun%22+-wikipedia) **·** [scholar](https://scholar.google.com/scholar?q=%22M1919+Browning+machine+gun%22) **·** [JSTOR](https://www.jstor.org/action/doBasicSearch?Query=%22M1919+Browning+machine+gun%22&acc=on&wc=on) _(March 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

Machine Gun, Caliber .30, Browning, M1919

[![Browning M1919a.png](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Browning_M1919a.png/300px-Browning_M1919a.png)](https://en.wikipedia.org/wiki/File:Browning_M1919a.png)

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/9dc93bb0-0d78-4fd5-8e2a-749e510e963c/dbls0mj-098ce56a-271a-446a-8d3e-709025322974.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzlkYzkzYmIwLTBkNzgtNGZkNS04ZTJhLTc0OWU1MTBlOTYzY1wvZGJsczBtai0wOThjZTU2YS0yNzFhLTQ0NmEtOGQzZS03MDkwMjUzMjI5NzQuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.fRt-YOaH9L5XIYJrbJDWvSwUxNbbFVY2N5agysH8qyY)

Type

[Medium machine gun](https://en.wikipedia.org/wiki/Medium_machine_gun "Medium machine gun")  
[General-purpose machine gun](https://en.wikipedia.org/wiki/General-purpose_machine_gun "General-purpose machine gun") (M1919A6)

Place of origin

United States

Service history

In service

1919–present

Used by

See _[users](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Current_and_former_users "M1919 Browning machine gun")_

Wars

[World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")  
[Chinese Civil War](https://en.wikipedia.org/wiki/Chinese_Civil_War "Chinese Civil War")  
[Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War")  
[First Indochina War](https://en.wikipedia.org/wiki/First_Indochina_War "First Indochina War")  
[Indonesian National Revolution](https://en.wikipedia.org/wiki/Indonesian_National_Revolution "Indonesian National Revolution")  
[1958 Lebanon crisis](https://en.wikipedia.org/wiki/1958_Lebanon_crisis "1958 Lebanon crisis")  
[Cuban Revolution](https://en.wikipedia.org/wiki/Cuban_Revolution "Cuban Revolution")  
[Algerian War](https://en.wikipedia.org/wiki/Algerian_War "Algerian War")  
[Second Taiwan Strait Crisis](https://en.wikipedia.org/wiki/Second_Taiwan_Strait_Crisis "Second Taiwan Strait Crisis")  
[Greek Civil War](https://en.wikipedia.org/wiki/Greek_Civil_War "Greek Civil War")  
[Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War")  
[Laotian Civil War](https://en.wikipedia.org/wiki/Laotian_Civil_War "Laotian Civil War")  
[Cambodian Civil War](https://en.wikipedia.org/wiki/Cambodian_Civil_War "Cambodian Civil War")  
[Portuguese Colonial War](https://en.wikipedia.org/wiki/Portuguese_Colonial_War "Portuguese Colonial War")  
[Lebanese Civil War](https://en.wikipedia.org/wiki/Lebanese_Civil_War "Lebanese Civil War")  
[Rhodesian Bush War](https://en.wikipedia.org/wiki/Rhodesian_Bush_War "Rhodesian Bush War")  
[1982 Lebanon War](https://en.wikipedia.org/wiki/1982_Lebanon_War "1982 Lebanon War")  
[South African Border War](https://en.wikipedia.org/wiki/South_African_Border_War "South African Border War")  
other conflicts

Production history

Designer

[John M. Browning](https://en.wikipedia.org/wiki/John_M._Browning "John M. Browning")

Designed

1919

Manufacturer

Buffalo Arms Corporation  
[Rock Island Arsenal](https://en.wikipedia.org/wiki/Rock_Island_Arsenal "Rock Island Arsenal")  
[Saginaw Steering Gear](https://en.wikipedia.org/wiki/Nexteer_Automotive "Nexteer Automotive") division of [General Motors](https://en.wikipedia.org/wiki/General_Motors "General Motors")

Produced

1919–1945

No. built

438,971[[1]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-Production-1)

Variants

A1; A2; A3; A4; A5; A6; M37 and AN/M2

Specifications

Mass

31 lb (14 kg) (M1919A4)

Length

-   37.94 in (964 mm) (M1919A4)
-   53 in (1,346 mm) (M1919A6)

[Barrel](https://en.wikipedia.org/wiki/Gun_barrel "Gun barrel") length

24 in (610 mm)

---

[Cartridge](https://en.wikipedia.org/wiki/Cartridge_(firearms) "Cartridge (firearms)")

-   [.30-06 Springfield](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield") (U.S.)
-   [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO")
-   [.303 British](https://en.wikipedia.org/wiki/.303_British ".303 British")
-   [7.92×57mm Mauser](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser "7.92×57mm Mauser")
-   [6.5×55mm](https://en.wikipedia.org/wiki/6.5%C3%9755mm "6.5×55mm")
-   [.22 Long Rifle](https://en.wikipedia.org/wiki/.22_lr ".22 lr") (Mini)
-   [7.62×54mmR](https://en.wikipedia.org/wiki/7.62%C3%9754mmR "7.62×54mmR")
-   [8×63mm patron m/32](https://en.wikipedia.org/wiki/8%C3%9763mm_patron_m/32 "8×63mm patron m/32")
-   [7.65×53mm Argentine](https://en.wikipedia.org/wiki/7.65%C3%9753mm_Argentine "7.65×53mm Argentine")
-   [7.5×54mm French](https://en.wikipedia.org/wiki/7.5%C3%9754mm_French "7.5×54mm French")

[Action](https://en.wikipedia.org/wiki/Action_(firearms) "Action (firearms)")

[Recoil-operated](https://en.wikipedia.org/wiki/Recoil_operation "Recoil operation")/short-recoil operation. [Closed bolt](https://en.wikipedia.org/wiki/Closed_bolt "Closed bolt").

[Rate of fire](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire")

400–600 [round/min](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire") (1200–1500 for AN/M2 variant)

[Muzzle velocity](https://en.wikipedia.org/wiki/Muzzle_velocity "Muzzle velocity")

2,800 ft/s (850 m/s)

Effective firing range

1,500 yd (1,400 m) (maximum effective range)

Feed system

250-round [belt](https://en.wikipedia.org/wiki/Belt_(firearm) "Belt (firearm)")

The **M1919 Browning** is a [.30 caliber](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield") [medium machine gun](https://en.wikipedia.org/wiki/Medium_machine_gun "Medium machine gun") that was widely used during the 20th century, especially during [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II"), the [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War"), and the [Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War"). The M1919 saw service as a light [infantry](https://en.wikipedia.org/wiki/Infantry "Infantry"), [coaxial](https://en.wikipedia.org/wiki/Coaxial_weapon "Coaxial weapon"), mounted, [aircraft](https://en.wikipedia.org/wiki/Aircraft "Aircraft"), and [anti-aircraft](https://en.wikipedia.org/wiki/Anti-aircraft "Anti-aircraft") machine gun by the U.S. and many other countries.

The M1919 was an air-cooled development of the standard US machine gun of [World War I](https://en.wikipedia.org/wiki/World_War_I "World War I"), the [John M. Browning](https://en.wikipedia.org/wiki/John_Browning "John Browning")-designed water-cooled [M1917](https://en.wikipedia.org/wiki/M1917_Browning_machine_gun "M1917 Browning machine gun"). The emergence of [general-purpose machine guns](https://en.wikipedia.org/wiki/General-purpose_machine_gun "General-purpose machine gun") in the 1950s pushed the M1919 into secondary roles in many cases, especially after the arrival of the [M60](https://en.wikipedia.org/wiki/M60_machine_gun "M60 machine gun") in [US Army](https://en.wikipedia.org/wiki/US_Army "US Army") service. The [United States Navy](https://en.wikipedia.org/wiki/United_States_Navy "United States Navy") also converted many to [7.62 mm NATO](https://en.wikipedia.org/wiki/7.62_mm_NATO "7.62 mm NATO"), and designated them Mk 21 Mod 0; they were commonly used on riverine craft in the 1960s and 1970s in Vietnam. Many [NATO](https://en.wikipedia.org/wiki/NATO "NATO") countries also converted their examples to 7.62 mm caliber, and these remained in service well into the 1990s, as well as up to the present day in some countries.

## Contents

-   [1 Operation](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Operation)
    -   [1.1 Loading](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Loading)
    -   [1.2 Firing](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Firing)
-   [2 Operational use](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Operational_use)
    -   [2.1 Infantry](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Infantry)
    -   [2.2 Aircraft](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Aircraft)
-   [3 Other calibers](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Other_calibers)
    -   [3.1 On Lend-Lease British aircraft provided to the Soviets](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#On_Lend-Lease_British_aircraft_provided_to_the_Soviets)
-   [4 Production](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Production)
-   [5 Variants and derivatives](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Variants_and_derivatives)
    -   [5.1 M1919 variants](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#M1919_variants)
    -   [5.2 M1919A2](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#M1919A2)
    -   [5.3 M1919A3](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#M1919A3)
    -   [5.4 M1919A4](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#M1919A4)
    -   [5.5 M1919A6](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#M1919A6)
    -   [5.6 T66](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#T66)
    -   [5.7 .30 AN/M2](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#.30_AN/M2)
    -   [5.8 _Flygplanskulspruta_ m/22](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Flygplanskulspruta_m/22)
    -   [5.9 Browning .303 Mark II](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Browning_.303_Mark_II)
    -   [5.10 M37](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#M37)
    -   [5.11 Mk 21 Mod 0](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Mk_21_Mod_0)
-   [6 International variants and derivatives](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#International_variants_and_derivatives)
    -   [6.1 Commercial variants and derivatives](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Commercial_variants_and_derivatives)
        -   [6.1.1 Colt MG40](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Colt_MG40)
-   [7 Civilian ownership in the US](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Civilian_ownership_in_the_US)
-   [8 Current and former users](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Current_and_former_users)
-   [9 See also](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#See_also)
-   [10 References](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#References)
-   [11 Further reading](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#Further_reading)
-   [12 External links](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#External_links)

## Operation

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/GI_machine_gun_crew_in_Aachen_%28Correct_orientation%29.jpg/220px-GI_machine_gun_crew_in_Aachen_%28Correct_orientation%29.jpg)](https://en.wikipedia.org/wiki/File:GI_machine_gun_crew_in_Aachen_(Correct_orientation).jpg)

US soldiers fire a M1919A4 in [Aachen](https://en.wikipedia.org/wiki/Aachen "Aachen")

### Loading

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **does not [cite](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources") any [sources](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this section](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and [removed](https://en.wikipedia.org/wiki/Wikipedia:Verifiability#Burden_of_evidence "Wikipedia:Verifiability"). _(January 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

The M1919 originally fired the .30 cal M1906 ([.30-06](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield")) [ball](https://en.wikipedia.org/wiki/Full_metal_jacket_(ammunition) "Full metal jacket (ammunition)") [cartridge](https://en.wikipedia.org/wiki/Cartridge_(firearms) "Cartridge (firearms)"), and later the .30 caliber M2 ball cartridge, contained in a woven cloth [belt](https://en.wikipedia.org/wiki/Belt_(firearm) "Belt (firearm)"), feeding from left to right. A metal [M1 link](https://en.wikipedia.org/wiki/M1_link "M1 link") was later adopted, forming a ["disintegrating" belt](https://en.wikipedia.org/wiki/Disintegrating_link "Disintegrating link").

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Browning_M1919A4_Marine_Namur_Island.jpg/220px-Browning_M1919A4_Marine_Namur_Island.jpg)](https://en.wikipedia.org/wiki/File:Browning_M1919A4_Marine_Namur_Island.jpg)

Two [Marines](https://en.wikipedia.org/wiki/United_States_Marine_Corps "United States Marine Corps") with a M1919A4 on [Namur Island](https://en.wikipedia.org/wiki/Namur_Island "Namur Island") during [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")

Loading was accomplished by inserting the pull tab on the ammunition belt from the left side of the gun - either metal links or metal tab on cloth belts - until the feeding pawl at the entrance of the feed way engaged the first round in the belt and held it in place. The cocking handle was then pulled back with the right hand, palm facing up (to protect the thumb from injury if the weapon fired unexpectedly, which could happen if the barrel was very hot), and then released. This advanced the first round of the belt in front of the bolt for the extractor/ejector on the bolt to grab the first cartridge. The cocking handle was then pulled and released a second time. This caused the extractor to remove the first cartridge from the belt and chamber it (load it into the barrel ready to fire). As the bolt slid forward into battery, the extractor engaged the next round on the now-advanced belt resting in the feedway, preparing to draw it from the belt in the next firing cycle. Every time the gun fired a shot, the gun performed the sequence of extracting the spent round from the chamber and extracting the following round from the belt as the bolt came rearward, the fresh round ejecting the spent one when the bolt was to the rear and the fresh round was cycled in front of the bolt, then on the forward stroke chambering the next round to be fired, advancing the belt, and engaging the next round in preparation for loading. Once the bolt closed, the firing pin dropped and the round was fired, and the sequence was repeated (at a rate of roughly ten cycles per second) until the trigger was released or the ammunition belt was exhausted.

The gun's original design was as a water-cooled machine gun (see the [M1917 Browning machine gun](https://en.wikipedia.org/wiki/M1917_Browning_machine_gun "M1917 Browning machine gun")). When it was decided to try to lighten the gun and make it air-cooled, its design as a [closed bolt](https://en.wikipedia.org/wiki/Closed_bolt "Closed bolt") weapon created a potentially dangerous situation. If the gun was very hot from prolonged firing, the cartridge ready to be fired could be resting in a red-hot barrel, causing the propellant in the cartridge to heat up to the point that it would ignite and fire the cartridge on its own (a cook-off). With each further shot heating the barrel even more, the gun would continue to fire uncontrollably until the ammunition ran out, since depressing the trigger was not what was causing the gun to fire (although rarely as full rate automatic fire; it takes time for heat to soak into a cartridge, so usually it would manifest as a series of unexpected random discharges, the frequency increasing with the temperature of the barrel). Gunners were taught to cock the gun with the palm facing up, so that in the event of a cook-off, their thumb would not be dislocated by the charging handle, and to seize the ammunition belt and pull to prevent it from feeding, if the gun ever started an uncontrollable cycle of cooking off. Gunners were trained to manage the barrel heat by firing in controlled bursts of three to five rounds, to delay heating. Most other air cooled machine gun designs were fired in the same way, even those featuring quick-change barrels and which fired from an [open bolt](https://en.wikipedia.org/wiki/Open_bolt "Open bolt"), two features that make air-cooled machine guns capable of somewhat more sustained fire, both features that the M1919 design lacked.

### Firing

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **does not [cite](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources") any [sources](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this section](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and [removed](https://en.wikipedia.org/wiki/Wikipedia:Verifiability#Burden_of_evidence "Wikipedia:Verifiability"). _(January 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

When the gun was ready to fire, a round would be in the chamber and the bolt and barrel group would be locked together, with the locking block at the rear of the bolt. When the rear of the trigger was pivoted upwards by the operator, the front of the trigger tipped downward, pulling the sear out of engagement with the spring-loaded firing pin, allowing it to move forward and strike the primer of the cartridge.

As the assembly of bolt, barrel and barrel extension [recoiled to the rear of the gun upon firing](https://en.wikipedia.org/wiki/Short-recoil "Short-recoil"), the locking block was drawn out of engagement by a cam in the bottom of the gun's receiver. The recoiling barrel extension struck the "accelerator" assembly, a half-moon shaped spring-loaded piece of metal pivoting from the receiver below the bolt and behind the barrel extension. The tips of the accelerator's two curving fingers engaged the bottom of the bolt and caused it to move rapidly to the rear. The extractor-ejector was a mechanism that pivoted over the front of the bolt, with a claw that gripped the base of the next round in the belt. A camming track in the left side of the receiver caused this to move down as the bolt moved back, lowering the next round down on top of the fired case, pushing it straight down out of the extraction grooves of the bolt face through the ejection port. A spring in the feed tray cover pushed the extractor-ejector down onto the next round, so if the feed tray cover was opened, the extractor-ejector would be pulled upwards if the belt needed to be removed.

The belt feed lever was connected to the belt feeding pawl at the front end, had a cam pin at the rear end which ran through a track in the top of the bolt, and a pin in the feed tray cover acted as the pivot between the two ends. The rearward movement of the bolt caused the rear end of the feed lever to pull to the right, causing the feeding pawl at the other end to move left over the belt. The pawl would pull the belt further to the right as the bolt came forward again, also sending the loose [M1 link](https://en.wikipedia.org/wiki/M1_link "M1 link") of the previous round to be taken out of the belt to fly out the right side of the receiver. A recoil buffer tube extended from the back of the receiver to make the cycle of the bolt smoother than previous designs, to absorb some of the recoil of the bolt, and formed a place for the pistol grip to be installed.

Except for the M1919A6, all other variants had to be mounted on a tripod or other type of mount to be used effectively. The tripod used by infantry allowed traverse and elevation. To aim the gun along its vertical axis, the adjustment screw needed to be operated. This allowed the point of aim to be moved upwards or downwards, with free traverse to either side, allowing the gunner to set an elevation and sweep a wide band of fire across it by simply moving the gun from side to side. There was no need to control barrel climb or keep careful track of the fall of shots to make sure the fire was falling at the proper range. The gun was aimed using iron sights, a small folding post at the front end of the receiver and a rear aperture sight on a sliding leaf with range graduations from 200 to 1,800 meters in 200 meter increments. When folded down, the aperture formed a notch that could be used to fire the gun immediately without flipping up the leaf. The rear sight also had windage adjustment with a dial on the right side.

## Operational use

### Infantry

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **needs additional citations for [verification](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and removed. _(January 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Resting_on_Peleliu_Island.jpg/220px-Resting_on_Peleliu_Island.jpg)](https://en.wikipedia.org/wiki/File:Resting_on_Peleliu_Island.jpg)

A Marine cradles his M1919 Browning machine gun in his lap in [Peleliu](https://en.wikipedia.org/wiki/Battle_of_Peleliu "Battle of Peleliu")

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/US_ARMY_M1919A4_Korea%2C_1953.jpg/220px-US_ARMY_M1919A4_Korea%2C_1953.jpg)](https://en.wikipedia.org/wiki/File:US_ARMY_M1919A4_Korea,_1953.jpg)

A US soldier takes aim with a tripod-mounted M1919A4 in Korea, 1953

As a [company](https://en.wikipedia.org/wiki/Company_(military_unit) "Company (military unit)") support weapon, the M1919 required a five-man crew: the squad leader; the gunner (who fired the gun and when advancing carried the tripod and box of ammunition); the assistant gunner (who helped feed the gun and carried it, and a box of spare parts and tools); two ammunition carriers.[[2]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-2) The original idea of the M1919 was to allow it to be more easily packed for transport, and featured a light barrel and bipod when first introduced as the M1919A1. Unfortunately, it quickly became clear that the gun was too heavy to be easily moved, while at the same time, too light for sustained fire. This led to the M1919A2, which included a heavier barrel and tripod, and could sustain fire for longer periods.

The M1919A4 weighed about 31 pounds (14 kg), and was ordinarily mounted on a "lightweight" (14lb), low-slung tripod for [infantry](https://en.wikipedia.org/wiki/Infantry "Infantry") use (light and low compared to the previous M1917 tripod). Fixed vehicle mounts were also employed. It saw wide use in [World War II](https://en.wikipedia.org/wiki/World_War_II "World War II") mounted on [jeeps](https://en.wikipedia.org/wiki/Jeep "Jeep"), [half-tracks](https://en.wikipedia.org/wiki/Half-tracks "Half-tracks"), [armored cars](https://en.wikipedia.org/wiki/Armored_car_(military) "Armored car (military)"), [tanks](https://en.wikipedia.org/wiki/Tanks "Tanks"), amphibious vehicles, and landing craft. The M1919A4 played a key role in the firepower of the World War II U.S. Army. Each infantry company normally had a weapons platoon in addition to its other organic units. The presence of M1919A4 weapons in the weapons platoon gave company commanders additional automatic fire support at the company level, whether in the assault or on defense.[[3]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-3)

The M1919A5 was an adaptation of the M1919A4 with a forward mounting point to allow it to be mounted in tanks and [armored cars](https://en.wikipedia.org/wiki/Armored_car_(military) "Armored car (military)"). This, along with the M37 (another M1919 variant) and the [Browning M2 machine gun](https://en.wikipedia.org/wiki/M2_Browning_machine_gun "M2 Browning machine gun"), was the most common secondary armament during World War II for the [Allies](https://en.wikipedia.org/wiki/Allies_of_World_War_II "Allies of World War II"). The [coaxial](https://en.wikipedia.org/wiki/Coaxial_weapon "Coaxial weapon") M37 variant had the ability to feed from either the left or the right of the weapon, and featured an extended charging handle similar to those on the M1919A4E1 and A5. A trial variant fitted with special sighting equipment was designated M37F.

Another version of the M1919A4, the M1919A6, was an attempt to make the weapon into a true light machine gun by attaching a bipod, buttstock, carrying handle, and lighter barrel (4 lb (1.8 kg) instead of 7 lb (3.2 kg)). The M1919A6, with a wooden buttstock, handle, pistol grip and bipod directly mounted to the body of the weapon was in fact one pound heavier than the M1919A4 without its tripod, at 32 lb (15 kg), though its bipod made for faster deployment and enabled the machine gun team to dispense with one man (the tripod bearer).[[4]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-4) The A6 version saw increasing service in the latter days of World War II and was used extensively in [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War"). While the modifications were intended to make the weapon more useful as a squad [light machine gun](https://en.wikipedia.org/wiki/Light_machine_gun "Light machine gun"), it was a stopgap solution. Even though it was reliable, it proved somewhat impractical for its intended role.[[5]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-5)

In the late 1950s, an M1919 designed for remote firing via a solenoid trigger was developed for use in the [XM1/E1](https://en.wikipedia.org/wiki/U.S._Helicopter_Armament_Subsystems#OH-13_Sioux_and_OH-23_Raven "U.S. Helicopter Armament Subsystems") armament subsystem was designated M37C. The US Navy later converted a number of M1919A4s to 7.62mm NATO chambering and designated them Mk 21 Mod 0; some of these weapons were employed in Vietnam War in riverine warfare patrols.

From the 1960s until the 1990s, the [Israel Defense Forces](https://en.wikipedia.org/wiki/Israel_Defense_Forces "Israel Defense Forces") (IDF) used ground tripod and vehicle-mounted M1919A4 guns converted to 7.62 mm NATO on many of their armored vehicles and M3 personnel carriers. Israel developed a modified link for these guns due to feeding problems with the original US M1 link design. The improved Israeli link worked with .30 caliber, 7.62 mm NATO and [8×57 mm](https://en.wikipedia.org/wiki/8%C3%9757_mm "8×57 mm") cartridges.

### Aircraft

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/PBY_Gun_Blister.jpg/220px-PBY_Gun_Blister.jpg)](https://en.wikipedia.org/wiki/File:PBY_Gun_Blister.jpg)

An [Aviation Ordnanceman](https://en.wikipedia.org/wiki/Aviation_Ordnanceman "Aviation Ordnanceman") stationed at the [Naval Air Station Corpus Christi](https://en.wikipedia.org/wiki/Naval_Air_Station_Corpus_Christi "Naval Air Station Corpus Christi") installing an AN-M2 Browning machine gun in a [PBY](https://en.wikipedia.org/wiki/PBY_Catalina "PBY Catalina") [flying boat](https://en.wikipedia.org/wiki/Flying_boat "Flying boat"), ca. 1942

With assistance from firearms engineers at [Fabrique Nationale de Herstal](https://en.wikipedia.org/wiki/Fabrique_Nationale_de_Herstal "Fabrique Nationale de Herstal"),[[6]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-Goldsmith,_Dolf_L._2006-6) Belgium, the Model 1919 was completely re-engineered into the .30 caliber M2/AN (Army-Navy) aircraft machine gun (not to be confused with the [.50 caliber M2/AN](https://en.wikipedia.org/wiki/M2_Browning "M2 Browning") or the [20mm AN/M2](https://en.wikipedia.org/wiki/Hispano-Suiza_HS.404 "Hispano-Suiza HS.404"), the two other primary US aircraft weapons of WWII). The .30 in M2/AN Browning was widely adopted as both a fixed (offensive) and flexible (defensive) weapon on aircraft. Aircraft machine guns required light weight, firepower, and reliability, and achieving all three goals proved a difficult challenge, with the mandate for a closed bolt firing cycle to enable the gun to be safely and properly [synchronized](https://en.wikipedia.org/wiki/Synchronization_gear "Synchronization gear") for fixed-mount, forward-aimed guns firing through a spinning propeller, a necessity on many single-engined [fighter aircraft](https://en.wikipedia.org/wiki/Fighter_aircraft "Fighter aircraft") designs through to nearly the end of World War II. The receiver walls and operating components of the M2 were made thinner and lighter, and with air cooling provided by the speed of the aircraft, designers were able to reduce the barrel's weight and profile. As a result, the M2 weighed two-thirds that of the 1919A4, and the lightened mechanism gave it a rate of fire approaching 1,200 rpm (some variants could achieve 1,500 rpm),[[6]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-Goldsmith,_Dolf_L._2006-6) a necessity for engaging fast-moving aircraft. The M2's feed mechanism had to lift its own loaded belt out of the ammunition box and feed it into the gun, equivalent to a weight of 11 lb (5 kg).[[7]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-Dunlap,_Roy_F._1948_pp._4-5-7) In Ordnance circles, the .30 M2/AN Browning had the reputation of being the most difficult-to-repair weapon in the entire US small arms inventory.[[7]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-Dunlap,_Roy_F._1948_pp._4-5-7)

The M2 also appeared in a twin-mount version which paired two M2 guns with opposing feed chutes in one unit for operation by a single gunner, with a combined rate of fire of 2,400 rpm. All of the various .30 M2 models saw service in the early stages of World War II, but were phased out beginning in 1943, as hand-trained rifle-caliber defensive machine guns became obsolete for air warfare (the .50 in/12.7 mm M2/AN Browning and 20 mm [AN/M2](https://en.wikipedia.org/wiki/Hispano_HS.404 "Hispano HS.404") automatic cannon had replaced the .30 in as offensive air armament as well). The .30 in M2 aircraft gun was widely distributed to other US allies during and after World War II, and in British and Commonwealth service saw limited use as a vehicle-mounted anti-aircraft or anti-personnel machine gun.[[8]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-8)

## Other calibers

The same basic weapon (albeit modified to fire from an open bolt and therefore not compatible with sychronization) was also chambered for the British [.303](https://en.wikipedia.org/wiki/.303_British ".303 British") round, and was used as the [United Kingdom's](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom") primary offensive (fixed forward firing) aircraft gun in fighters such as the [Supermarine Spitfire](https://en.wikipedia.org/wiki/Supermarine_Spitfire "Supermarine Spitfire") and [Hawker Hurricane](https://en.wikipedia.org/wiki/Hawker_Hurricane "Hawker Hurricane") and as fixed armament in bombers like the [Bristol Blenheim](https://en.wikipedia.org/wiki/Bristol_Blenheim "Bristol Blenheim"), the [Fairey Battle](https://en.wikipedia.org/wiki/Fairey_Battle "Fairey Battle"), [Handley Page Hampden](https://en.wikipedia.org/wiki/Handley_Page_Hampden "Handley Page Hampden") and [Martin Maryland](https://en.wikipedia.org/wiki/Martin_Maryland "Martin Maryland"), until the widespread introduction of the larger 20mm caliber [Hispano-Suiza Mk.II](https://en.wikipedia.org/wiki/Hispano-Suiza_HS.404 "Hispano-Suiza HS.404") cannon, and throughout the war and defensive turret weapons in bombers. British night fighter [Mosquitoes](https://en.wikipedia.org/wiki/De_Havilland_Mosquito "De Havilland Mosquito") used quartets of .303 Brownings in the nose and [Beaufighters](https://en.wikipedia.org/wiki/Bristol_Beaufighter "Bristol Beaufighter") used six in the wings, supplementing the main armament of four 20mm Hispano cannon in ventral fuselage mounts. The British modification to open bolt firing meant that it was now impossible to synchronize the guns to fire through the propeller arc, but wing-mounted armament was the norm on British single-engine fighters.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

Similar versions for a variety of European calibers were delivered by the Belgian gun maker [Fabrique Nationale](https://en.wikipedia.org/wiki/Fabrique_Nationale "Fabrique Nationale") (FN), notably German-standard [7.92×57mm Mauser](https://en.wikipedia.org/wiki/7.92%C3%9757mm_Mauser "7.92×57mm Mauser") which was widely used in [Eastern Europe](https://en.wikipedia.org/wiki/Eastern_Europe "Eastern Europe"); and by Swedish gun maker [Carl Gustaf SGF](https://en.wikipedia.org/wiki/Carl_Gustafs_Stads_Gev%C3%A4rsfaktori "Carl Gustafs Stads Gevärsfaktori") in [6.5×55mm](https://en.wikipedia.org/wiki/6.5%C3%9755mm "6.5×55mm") and [8×63mm](https://en.wikipedia.org/w/index.php?title=8%C3%9763mm&action=edit&redlink=1 "8×63mm (page does not exist)") calibers.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

Argentina used Colt-manufactured guns chambered for the standard Argentine [7.65×53mm](https://en.wikipedia.org/wiki/7.65%C3%9753mm_Argentine "7.65×53mm Argentine") cartridge.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

### On Lend-Lease British aircraft provided to the Soviets

The .303 variant equipped the [Hawker Hurricanes](https://en.wikipedia.org/wiki/Hawker_Hurricane "Hawker Hurricane") delivered to [Soviet Air Forces](https://en.wikipedia.org/wiki/Soviet_Air_Force "Soviet Air Force"), during World War II (in both eight and twelve-gun variants). Soviet airmen compared them to their own, rapid-firing (at up to 1,800 rounds/min) [ShKAS machine gun](https://en.wikipedia.org/wiki/ShKAS_machine_gun "ShKAS machine gun") in terms of reliability: "But they often failed due to dust," recalled pilot Nikolai G. Golodnikov. "We tackled the problem gluing [percale](https://en.wikipedia.org/wiki/Percale "Percale") on all the machine-gun holes, and when you opened fire, bullets went right through. The machine guns became reliable then. They were of low efficiency when fired from distances of 150–300 m."[[9]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-Drabkin,_p._126.-9)

## Production

The M1919 was manufactured during World War II by three different companies in the United States; Buffalo Arms Corporation, [Rock Island Arsenal](https://en.wikipedia.org/wiki/Rock_Island_Arsenal "Rock Island Arsenal"), and the Saginaw Steering Gear division of [General Motors](https://en.wikipedia.org/wiki/General_Motors "General Motors"). In the UK, production was chiefly by [BSA](https://en.wikipedia.org/wiki/Birmingham_Small_Arms_Company "Birmingham Small Arms Company"). Originally unit priced at $667 each, mass production lowered the price to $141.44.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

## Variants and derivatives

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **needs additional citations for [verification](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and removed. _(January 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

### M1919 variants

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/HAFm_Browning_30_cal_7089.JPG/220px-HAFm_Browning_30_cal_7089.JPG)](https://en.wikipedia.org/wiki/File:HAFm_Browning_30_cal_7089.JPG)

M1919A6 mounted on the tripod for an M1917

The original **M1919** was designed for use with tanks.[[10]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-m1919tech-10) The water-cooled M1917 was inappropriate due to weight and the vulnerability of the water jacket. Browning modified the M1917 to be air-cooled by making changes that included dropping the water jacket and using a heavier barrel.[[10]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-m1919tech-10)

In total, there were six variants of the basic M1919 machine gun.

M1919A1

The **M1919A1** featured a lighter barrel and a bipod. It was distinguished from the "M1919" because it also had sights, which the M1919 did not.

### M1919A2

The **M1919A2** was another lightweight development specifically for mounted cavalry units, utilizing a shorter 18-inch barrel and a special tripod, though it could be fitted to either the M1917 or M2 tripods. This weapon was designed to allow greater mobility to cavalry units over the existing M1917 machine gun. The M1919A2 was introduced in 1922, and was used for a short period between World War I and World War II after the cavalry had converted from horses to wheeled and tracked vehicles.

### M1919A3

The **M1919E1**, commonly known as the **M1919A3**, was introduced in 1931 as an improved version of the M1919A2 for the infantry.[[11]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201415-11)

### M1919A4

The most common variant of the series was the M1919A4. Production blueprints of the new variant were complete in late 1936, and slow-scale production soon followed.[[11]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201415-11) The driving force behind the development of this variant was the lack of reliability in the previous 18-inch barrel versions, which did not produce enough recoil to cycle the action reliably. The gun was given a heavier "bull barrel", much thicker and was lengthened to 24 in (0.61 m) like the M1917, for cooling purposes, and a [recoil booster](https://en.wikipedia.org/wiki/Muzzle_booster "Muzzle booster") to enhance cycling performance, even with the heavier barrel. Various other small adjustments to the design were made, such as moving the front sight from the barrel jacket to the receiver, which made it easier to mount the gun on vehicles. The design of the barrel jacket was changed to include circular holes instead of long slits of earlier models. The recoil buffer assembly was also a new addition to the design between A3 and A4 development, designed to reduce the impact of the bolt hitting the backplate.

The M1919A4 was used in both fixed and flexible mounts, by infantry and on vehicles. It was also widely exported after World War II and continues to be used in small numbers around the world. Two variants were developed specifically for vehicular use, the M1919A5, with an extended charging handle, and the M1919A4E1, a sub-variant of the M1919A4 refitted with an extended charging handle developed in the 1950s.[[12]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201427-12)

### M1919A6

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/M1919A6_Medium_machine_Gun.jpg/220px-M1919A6_Medium_machine_Gun.jpg)](https://en.wikipedia.org/wiki/File:M1919A6_Medium_machine_Gun.jpg)

M1919A6 mounted on its bipod

During the war it became clear to the US military that the [M1918 Browning Automatic Rifle](https://en.wikipedia.org/wiki/M1918_Browning_Automatic_Rifle "M1918 Browning Automatic Rifle"), while portable, was not sufficient as a sustained fire weapon due to its fixed barrel and 20-round magazine. The M1919A4 was faster and cheaper to produce, but did not have the portability of a rifle. Realizing that producing an entirely new replacement machine gun would take time, the military decided that a stop-gap solution would be best and adapted an existing design. The M1919A6 was an attempt at such a solution, to parallel the designs of the German [MG 34](https://en.wikipedia.org/wiki/MG_34 "MG 34") and [MG 42](https://en.wikipedia.org/wiki/MG_42 "MG 42") machine guns, each of which were portable for a squad weapon and were very effective at sustained fire.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

The M1919A6 first saw combat service in the fall of 1943.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] It had a metal buttstock assembly that clamped to the backplate of the gun, and a front barrel bearing that incorporated both a muzzle booster and a bipod similar to that used on the BAR. A lighter barrel than that of the M1919A4 was fitted, and a carrying handle was attached to the barrel jacket to make it easier to carry. Previous M1919 designs could change the barrel, but it required essentially field stripping the gun to pull the barrel out from the rear – the pistol grip back plate, bolt group and the trigger group all had to be removed before the barrel could be replaced, and this put the gun out of action for minutes, and risked losing and damaging parts in the field.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] The M1919A6 muzzle device allowed the gun crew to replace the barrel from the front; an improvement, but still an awkward procedure compared to other machine guns of the day. The M1919A6 was a heavy (32 pounds, 15 kg) and awkward weapon in comparison with the MG34 (26 pounds, 12 kg) and MG42 (25 pounds, 11 kg) and was eventually replaced in US service by the [M60 machine gun](https://en.wikipedia.org/wiki/M60_machine_gun "M60 machine gun") (23.15 pounds, 10.50 kg) in the 1960s.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

### T66

The M1919A6 was used by [Springfield Armory](https://en.wikipedia.org/wiki/Springfield_Armory "Springfield Armory") in the late 1940s and early 1950s as a testbed for an interim general-purpose machine gun. It was rechambered for the experimental T65 series cartridges, culminating in 1951 with the T66 machine gun chambered for the T65E3 cartridge (one of the forerunners to the 7.62mm NATO cartridge). It had a new barrel with a flash-hider attachment, a shorter action, and modified M1 disintegrating belt links to feed the new cartridge. It was deemed still too heavy for field use and was not adopted.

### .30 AN/M2

A specific aircraft version of the .30 caliber Model 1919A4 was manufactured by Browning as the .30 AN/M2. It had a thinner barrel and receiver walls to keep down weight. Compared to the M1919A4, the AN/M2 had a substantially higher rate of fire (1,200 to 1,500 rounds per minute). It was used on US aircraft early in World War II, but the lighter .30-caliber weapon was increasingly relegated to training duties as the war progressed. A derivative of this weapon was built by Colt as the civilian market MG40.

It was later replaced by the larger caliber – and is not to be confused with – the [Browning Machine Gun, Cal. .50, M2, Aircraft](https://en.wikipedia.org/wiki/M2_Browning#AN/M2 "M2 Browning"), with the smaller-calibre ordance bearing the official designation of "Browning Machine Gun, Cal. **.30**, M2, Aircraft." The [.50 AN/M2 "light barrel" version](https://en.wikipedia.org/wiki/M2_Browning#Aircraft_guns "M2 Browning"), used in the majority of [fixed and flexible/turreted](https://en.wikipedia.org/wiki/Aircraft_gun_turret "Aircraft gun turret") mounts on U.S. World War II-era aircraft as the war progressed, lacked the massive "cooling collar" of the heavy barrel M2HB version, which is still in service with the ground forces of the U.S. military in the 21st century.

The AN/M2 was responsible for seriously wounding "one of the best Japanese fighter pilots of the war" flying ace [Saburō Sakai](https://en.wikipedia.org/wiki/Sabur%C5%8D_Sakai "Saburō Sakai") when he attacked eight [SBD Dauntlesses](https://en.wikipedia.org/wiki/SBD_Dauntless "SBD Dauntless") from behind mistaking them for [F4F Wildcat](https://en.wikipedia.org/wiki/F4F_Wildcat "F4F Wildcat") fighters.[[13]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-13)

The ["Stinger"](https://en.wikipedia.org/wiki/M2_Stinger "M2 Stinger") was a field modification by marines in the [Pacific Theater](https://en.wikipedia.org/wiki/Pacific_War "Pacific War") during World War II and used on the ground as a light machine gun. These were salvaged from crashed and disabled aircraft and fitted with a bipod (spade grips still attached). Later more extensive modifications led to six being fitted with a custom trigger, [M1 Garand](https://en.wikipedia.org/wiki/M1_Garand "M1 Garand") buttstock, [M1918 Browning Automatic Rifle](https://en.wikipedia.org/wiki/M1918_Browning_Automatic_Rifle "M1918 Browning Automatic Rifle") bipod and rear sights to allow for use without a tripod or other mount.[[14]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-14) The resulting weapon was a belt-fed, 40 in (1.0 m) long, 25 lb (11 kg) gun and fired three times as fast as the M1919A6's of the day. The Stinger was recommended as a replacement for the BAR in squads however the war ended just six months later.[[15]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-15) Marine Corporal [Tony Stein](https://en.wikipedia.org/wiki/Tony_Stein "Tony Stein") used a "Stinger" during the invasion of [Iwo Jima](https://en.wikipedia.org/wiki/Iwo_Jima "Iwo Jima"). Stein would posthumously receive the [Medal of Honor](https://en.wikipedia.org/wiki/Medal_of_Honor "Medal of Honor") for his actions during the battle."[[16]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-navalorder-16)

### _Flygplanskulspruta_ m/22

_Flygplanskulspruta_ m/22, (fpl)ksp m/22 for short, was a Swedish variant of the .30 AN/M2 aircraft machine gun.[[17]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-17) The name literally translates to "airplane machine gun model 22". It was originally used by the Swedish army's aviation branch but moved over to the Swedish air force when it was formed in 1926. The first guns delivered were built by Colt but Sweden later got a license to produce the weapon domestically. The _ksp_ m/22 stayed in active service all the way to 1957, although by then only in a [gunpod](https://en.wikipedia.org/wiki/Gunpod "Gunpod") for ground strafing.[[18]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-18) Originally the _ksp_ m/22 was chambered in [6.5x55 mm](https://en.wikipedia.org/wiki/6.5x55_mm "6.5x55 mm") but in 1932 almost all guns where re chambered to [8x63 mm](https://en.wikipedia.org/wiki/8%C3%9763mm_patron_m/32 "8×63mm patron m/32").[[19]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-19)

### Browning .303 Mark II

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Avro_Lancaster_VR-A_tail_turret_2.jpg/220px-Avro_Lancaster_VR-A_tail_turret_2.jpg)](https://en.wikipedia.org/wiki/File:Avro_Lancaster_VR-A_tail_turret_2.jpg)

The Browning .303 four-gun [FN-20](https://en.wikipedia.org/wiki/Nash_%26_Thompson "Nash & Thompson") [tail gun](https://en.wikipedia.org/wiki/Tail_gunner "Tail gunner") turret on an [Avro Lancaster](https://en.wikipedia.org/wiki/Avro_Lancaster "Avro Lancaster")

The Browning was adopted by the [Royal Air Force](https://en.wikipedia.org/wiki/Royal_Air_Force "Royal Air Force") as a replacement for the .303 [Vickers machine gun](https://en.wikipedia.org/wiki/Vickers_machine_gun "Vickers machine gun") and manufactured by [Vickers Armstrong](https://en.wikipedia.org/wiki/Vickers_Armstrong "Vickers Armstrong") and [BSA](https://en.wikipedia.org/wiki/Birmingham_Small_Arms_Company "Birmingham Small Arms Company") to fire the [British .303 inch (7.7 mm)](https://en.wikipedia.org/wiki/.303_British ".303 British") round and named "Browning .303 Mk II" in British Service. It was essentially the 1930 Pattern belt-fed Colt–Browning machine gun with a few modifications for British use, such as firing from an open [bolt](https://en.wikipedia.org/wiki/Bolt_(firearms) "Bolt (firearms)"), hence prohibiting the use of synchronization to fire them through a spinning propeller and a lighter bolt, increasing the rate of fire, much like the US .30 M2/AN aircraft variant. It was designed to fire hydraulically or pneumatically as a wing mounted machine gun but was also adopted as hand-fired mount for use in bombers and reconnaissance aircraft. It had a rate of fire of 1,150 rounds per minute.[[20]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-20)[[21]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-21) The licence was issued to BSA by July 1935.[[22]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-22)

The Browning .303 was used as the RAF and [FAA](https://en.wikipedia.org/wiki/Fleet_Air_Arm "Fleet Air Arm")'s primary fixed forward firing aircraft armament before the war, both on pre-war fighters ([Gloster Gladiator](https://en.wikipedia.org/wiki/Gloster_Gladiator "Gloster Gladiator"), [Hawker Fury](https://en.wikipedia.org/wiki/Hawker_Fury "Hawker Fury")) and on the UK's new 'eight-gun fighters' the [Hawker Hurricane](https://en.wikipedia.org/wiki/Hawker_Hurricane "Hawker Hurricane") and [Supermarine Spitfire](https://en.wikipedia.org/wiki/Supermarine_Spitfire "Supermarine Spitfire") and the naval [Fairey Fulmar](https://en.wikipedia.org/wiki/Fairey_Fulmar "Fairey Fulmar"), and as secondary weapons in mid-war variants of the Spitfire, as well as being fitted in single, double or quadruple mounts as offensive weapons for the [Bristol Blenheim](https://en.wikipedia.org/wiki/Bristol_Blenheim "Bristol Blenheim"), the [Fairey Battle](https://en.wikipedia.org/wiki/Fairey_Battle "Fairey Battle"), [Handley Page Hampden](https://en.wikipedia.org/wiki/Handley_Page_Hampden "Handley Page Hampden"), [Martin Maryland](https://en.wikipedia.org/wiki/Martin_Maryland "Martin Maryland")/[Baltimore](https://en.wikipedia.org/wiki/Martin_Baltimore "Martin Baltimore"), [Fairey Swordfish](https://en.wikipedia.org/wiki/Fairey_Swordfish "Fairey Swordfish"), [Lockheed Hudson](https://en.wikipedia.org/wiki/Lockheed_Hudson "Lockheed Hudson"), [Douglas Boston](https://en.wikipedia.org/wiki/Douglas_A-20_Havoc "Douglas A-20 Havoc"), [Blackburn Skua](https://en.wikipedia.org/wiki/Blackburn_Skua "Blackburn Skua") and [Bristol Beaufort](https://en.wikipedia.org/wiki/Bristol_Beaufort "Bristol Beaufort"). It was also used as a [turret gun](https://en.wikipedia.org/wiki/Gun_turret#Aircraft "Gun turret") in various [Boulton Paul](https://en.wikipedia.org/wiki/Boulton_Paul_Aircraft "Boulton Paul Aircraft") or [Nash & Thompson](https://en.wikipedia.org/wiki/Nash_%26_Thompson "Nash & Thompson") turrets on bombers, and flying boats. Even after the introduction of autocannon as primary fighter armament .303 Brownings were retained as supplementary weapons on many aircraft including later versions of the Spitfire, as well as fighter-bomber and night fighter versions of the [de Havilland Mosquito](https://en.wikipedia.org/wiki/De_Havilland_Mosquito "De Havilland Mosquito"), among others.

For hand-held moveable mount use the [Vickers K machine gun](https://en.wikipedia.org/wiki/Vickers_K_machine_gun "Vickers K machine gun") was preferred. There is pictorial evidence of the .303 Browning being placed on improvised bipods for ground use during the early campaigns in Burma and Malaysia.

### M37

In the late 1940s and early 1950s the US military was looking for an upgrade to the M1919 that could feed from either side for use as an improved [coaxial](https://en.wikipedia.org/wiki/Coaxial_weapon "Coaxial weapon") machine gun. Saco-Lowell developed a model that had the driving spring attached to the back plate (eliminating the need for a mainspring and driving rod protruding out the back of the bolt), a solenoid trigger for remote firing, a feed cover that could open from either side, a bolt with dual tracks that could feed from either side, and a reversible belt feed pawl, ejector, and feed chute. The experimental T151 had a flat backplate, the T152 had spade grips and a "butterfly" trigger like the M2HB, and the T153 had a pistol grip and back-up trigger like the M1919A4 and an extended charging handle similar to those on the M1919A5. The T153 was adopted as the M37 and was produced by SACO-Lowell and Rock Island Arsenal from 1955 to 1957. It was in regular service from 1955 until it was replaced by the M37E1 in the late 1960s and the M73A1 in the early 1970s.

The M37 was used mostly on the M47 and M48 Patton medium tanks. The **M37F** was a trial variant fitted with special sighting equipment. The **M37C** was a variant without a sight bracket designed for use in aircraft armament (like the skid-mounted [XM1/E1](https://en.wikipedia.org/wiki/U.S._Helicopter_Armament_Subsystems#OH-13_Sioux_and_OH-23_Raven "U.S. Helicopter Armament Subsystems") helicopter armament subsystem).

The **M37E1** was a M37 machine gun converted by Rock Island Arsenal and Springfield Armory to chamber the 7.62×51mm NATO cartridge and feed the M13 disintegrating belt. They were designed for interim use until the [M73 machine gun](https://en.wikipedia.org/wiki/M73_machine_gun "M73 machine gun") could be fielded.[[23]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-23) The M37E1 was to be standardized as the M37A1 but development of the improved M73A1 precluded this.[[24]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-24)

### Mk 21 Mod 0

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Browning_M1919_Cal_.30.jpg/220px-Browning_M1919_Cal_.30.jpg)](https://en.wikipedia.org/wiki/File:Browning_M1919_Cal_.30.jpg)

Mk 21 in [Vietnam](https://en.wikipedia.org/wiki/Vietnam_war "Vietnam war") being fed by an upside-down M-13 link belt (the links are not visible)

The increasing American involvement in Vietnam created a demand for small arms, especially the new M60 machine gun. The Navy had surplus machine guns left over from World War 2 and Korea, but they were chambered for the earlier .30-06 Springfield cartridge rather than the new standard 7.62mm NATO cartridge. The **Mk 21 Mod 0** was a US Navy conversion of the .30 M1919A4 to fire the [7.62mm NATO](https://en.wikipedia.org/wiki/7.62mm_NATO "7.62mm NATO") cartridge. This was accomplished by replacing the barrel, bolt, and feed cover and adding a chamber bushing, a link-stripper, and a second belt-holding pawl to allow it to feed and fire the new cartridge.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25)[[26]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-NATO'_pg.11-26) Spacer blocks were added to the front and back of the feedway to guide the shorter round and block the use of the longer .30-06 Springfield ammunition.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25)[[27]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-27) A six-inch flash hider was also added to the barrel to reduce the muzzle flash.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25)

The conversions were performed from 1966 through 1967 at [Naval Ordnance Station Louisville](https://en.wikipedia.org/wiki/Naval_Ordnance_Station_Louisville "Naval Ordnance Station Louisville").[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25) Modified M1919A4s had the designation "_Machine Gun, 7.62mm / Mk 21 Mod 0_" stamped on the receiver sideplate in 1/4-inch lettering. The replacement barrels had "_7.62mm NATO-G_" stamped on them in 1/8-inch letters to differentiate them from M1919A4 or M60 barrels;[[28]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-28) the letter _G_ indicated it used a grooved barrel bushing.[[26]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-NATO'_pg.11-26)

It used the standard 7.62mm NATO [M13 link](https://en.wikipedia.org/wiki/M13_link "M13 link") "strip-out" disintegrating link,[[26]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-NATO'_pg.11-26) in which the bolt pushes the round out of the bottom of the two-part link and then forwards into the breech.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25) The old [M1 link](https://en.wikipedia.org/wiki/M1_link "M1 link") "pull-out" disintegrating links, which are pulled backwards out of the one-piece link by the extractor towards the bolt and then forwards into the breech, would not feed through the new mechanism.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25) The M1 links, which were designed for the longer and thinner .30-06 Springfield, would also be too narrow to fit the shorter and thicker 7.62mm NATO round. The US Navy, because of their narrower inventory of 7.62mm NATO ammunition, used linked belts of either 7.62mm M80 Ball or a 4:1 ratio mix of 7.62mm M80 Ball and 7.62mm M62 Tracer.

The refurbished feed mechanism was left-hand feed only. It was different from the one in the M60 GPMG in that the open end of the belt had to be on top so it could be stripped out.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25)[[29]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-29) To prepare the ammo, gunners had to take out both of the 100-round belts from an M19A1 ammo can, had to link them both together, and then loaded the resultant 200-round belt back into the M19A1 can upside-down so it would feed correctly.[[25]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-warboats.org-25)

## International variants and derivatives

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Fokker_D.XXI_%28FR-110%29.jpg/220px-Fokker_D.XXI_%28FR-110%29.jpg)](https://en.wikipedia.org/wiki/File:Fokker_D.XXI_(FR-110).jpg)

Fokker D.XXI of [Finnish Air Force](https://en.wikipedia.org/wiki/Finnish_Air_Force "Finnish Air Force")

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Belgian_paratrooper_vehicle_IMG_1521.jpg/220px-Belgian_paratrooper_vehicle_IMG_1521.jpg)](https://en.wikipedia.org/wiki/File:Belgian_paratrooper_vehicle_IMG_1521.jpg)

Belgian paratrooper vehicle

The M1919 pattern has been used in countries all over the world in a variety of forms and under a number of different designations.

-   The Browning Mk 1 and Mk 2 were older-style Commonwealth designations for the .303 caliber Browning machine guns used on the vast majority of British aircraft of World War II.[[30]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-30) The difference between the Mk 1 and Mk 2 versions is unknown, but the weapon visually is quite similar to the AN/M2 aircraft gun. The post-war designations for these weapons was L3, and they were used by the United Kingdom, Canada, and Australia to designate the fixed (A1) and flexible (A2) versions of the M1919A4 in .30-06 caliber. L3A3 and L3A4 denoted sear hold-open conversion of previous L3A1s and L3A2s. The A3 is the modified version of the A1, and the A4 is the modified version of the A2. The Canadians later adopted a separate designation for 7.62×51mm rechambered M1919A4s for fixed (C1) and flexible (C1A1) applications. The C5 and C5A1 were product improvements of the previous C1 and C1A1 respectively.
-   The [Rhodesian Air Force](https://en.wikipedia.org/wiki/Rhodesian_Air_Force "Rhodesian Air Force") used twin Browning Mk 2 models, chambered in the British [.303](https://en.wikipedia.org/wiki/.303_British ".303 British") cartridge, mounted on [Alouette III](https://en.wikipedia.org/wiki/Alouette_III "Alouette III") G-Car helicopters[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] as well as modified variants fitted with [FN MAG](https://en.wikipedia.org/wiki/FN_MAG "FN MAG") bipods, pistol grips and stocks for ground use.[[31]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-britains-smallwars.com-31)
-   The Browning was produced by FN-Herstal in Belgium as well, being used in, among others, the [Fokker D.XXI](https://en.wikipedia.org/wiki/Fokker_D.XXI "Fokker D.XXI") and IAR-80/81 fighters.
-   MG A4 is the [Austrian](https://en.wikipedia.org/wiki/Austria "Austria") designation for the M1919A4.[[32]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201467-32)
-   MG4 is a South African upgrade of the M1919 in current use with the [South African National Defence Force](https://en.wikipedia.org/wiki/South_African_National_Defence_Force "South African National Defence Force"). The MG4 upgrade was done by [Lyttleton Engineering Works](https://en.wikipedia.org/wiki/Lyttleton_Engineering_Works "Lyttleton Engineering Works"), [Pretoria](https://en.wikipedia.org/wiki/Pretoria "Pretoria").[[32]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201467-32)
-   MG m/52-1 and MG m/52-11 were Danish designations for the M1919A4 and M1919A5 respectively.[[32]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201467-32)
-   The [Israel Defense Forces](https://en.wikipedia.org/wiki/Israel_Defense_Forces "Israel Defense Forces") (IDF) used vehicle-mounted M1919A4 guns converted to 7.62mm NATO on many of their armored vehicles.[[32]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201467-32)
-   [Ksp m/22](https://en.wikipedia.org/wiki/Ksp_m/22 "Ksp m/22") is the Swedish designation for license-built M1919s chambered for [8×63mm patron m/22](https://en.wikipedia.org/w/index.php?title=8%C3%9763mm_patron_m/22&action=edit&redlink=1 "8×63mm patron m/22 (page does not exist)") cartridges, for aircraft use.
-   [Ksp m/39](https://en.wikipedia.org/wiki/Ksp_m/39 "Ksp m/39") is the Swedish designation for M1919A4 license-built by [Carl Gustafs Stads Gevärsfaktori](https://en.wikipedia.org/wiki/Bofors_Carl_Gustaf "Bofors Carl Gustaf") chambered in [6.5×55mm](https://en.wikipedia.org/wiki/6.5%C3%9755mm "6.5×55mm") and [8×63mm patron m/32](https://en.wikipedia.org/wiki/8%C3%9763mm_patron_m/32 "8×63mm patron m/32"), and from about 1975 rebarreled in [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO"). Intended for use in tanks and armoured vehicles, it's available with both left- and right hand feeding, the former is used in [CV 90](https://en.wikipedia.org/wiki/CV_90 "CV 90").
-   [Ksp m/42](https://en.wikipedia.org/wiki/Ksp_m/42 "Ksp m/42") was the Swedish designation for license-built M1919A6 used for infantry support, normally chambered in [6.5×55mm](https://en.wikipedia.org/wiki/6.5%C3%9755mm "6.5×55mm") but occasionally in [8×63mm patron m/32](https://en.wikipedia.org/wiki/8%C3%9763mm_patron_m/32 "8×63mm patron m/32"), and from about 1975, mostly fitted with barrels in [7.62×51mm NATO](https://en.wikipedia.org/wiki/7.62%C3%9751mm_NATO "7.62×51mm NATO"). The Ksp m/42B was a lighter version with bipod and shoulder stock (used in a similar way as the M1919A6), chambered in 6.5×55mm and later in 7.62×51mm. Even the ksp m/42B proved too heavy, and was replaced by the ksp m/58 (FN MAG). In the late 1980s, most remaining ksp m/42 was rebuilt into ksp m/39 to be installed into the CV 90s.
-   The Poles developed a copy of the Browning M1919 chambered for 7.92×57mm Mauser, designated [Ckm wz.32](https://en.wikipedia.org/wiki/Ckm_wz.32 "Ckm wz.32"), similar to the earlier [Ckm wz.30](https://en.wikipedia.org/wiki/Ckm_wz.30 "Ckm wz.30").[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]

### Commercial variants and derivatives

#### Colt MG40

Colt produced a derivative of the M2 aircraft machine gun, the Colt MG40. It shipped in a variety of calibers, including the basic [.30-06 Springfield](https://en.wikipedia.org/wiki/.30-06_Springfield ".30-06 Springfield") and popular [7mm Spanish Mauser](https://en.wikipedia.org/wiki/7mm_Mauser "7mm Mauser"), and was available in left- or right-hand feed.  
The MG40-2 Light Aircraft Machine Gun could be used in flexible- (pintle-mounted), fixed- (wing-mounted), or synchronized- (through the propeller) models.[[33]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-biggerhammer.net-33) The Flexible mount machine gun came with grips and a "butterfly" trigger plate like the standard ground model. The Fixed model had a backplate. It used a cable connected to an operating slide connected to a stud on the bolt to fire it; tension in the cable causes the trigger to activate and slack in the cable causes it to stop.[[33]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-biggerhammer.net-33) The synchronized variant of the Fixed model had a trigger motor for through-propeller, gun synchronizing needs.[[33]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-biggerhammer.net-33)

## Civilian ownership in the US

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **does not [cite](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources") any [sources](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this section](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and [removed](https://en.wikipedia.org/wiki/Wikipedia:Verifiability#Burden_of_evidence "Wikipedia:Verifiability"). _(November 2014)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

The Browning M1919 remains popular with civilian enthusiasts in the United States, though changes in 1986 to the [National Firearms Act of 1934](https://en.wikipedia.org/wiki/National_Firearms_Act_of_1934 "National Firearms Act of 1934") (the US Federal law regulating private ownership of machine guns) prohibited the registration of new machine guns for sales to civilians, thus freezing the number of "transferable" machine guns in private ownership. The inflation of prices that followed, and the availability of parts from surplussed and scrapped machine guns, led to the development of semi-automatic versions of the Browning M1919. Typically, these are built using a new right sideplate (the portion legally considered the "firearm" under US law), which has a raised "island" protruding into the interior of the receiver. This requires the use of a modified bolt, barrel extension and lock frame which have been designed to allow only semi-automatic firing. The "island" prevents the insertion of unmodified full-automatic parts. A number of small gun companies have produced these "semi-auto machine guns" for commercial sales. The fairly simple modifications necessary to convert M1919 parts to the semi-automatic version, and the relatively easy process of riveting used in the assembly of the Browning machine gun's receiver, have also made it a popular gun for hobbyists to build at home.

Similar "semi-auto machine guns" have been built using parts from other Browning pattern machine guns, to include the AN/M2 aircraft gun and FN30, and variations that never saw military use such as extremely short (8") barreled guns.

## Current and former users

[![](https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png)](https://en.wikipedia.org/wiki/File:Question_book-new.svg)

This section **needs additional citations for [verification](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=M1919_Browning_machine_gun&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and removed. _(January 2021)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Algeria.svg/23px-Flag_of_Algeria.svg.png) [Algeria](https://en.wikipedia.org/wiki/Algeria "Algeria")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Flag_of_Angola.svg/23px-Flag_of_Angola.svg.png) [Angola](https://en.wikipedia.org/wiki/Angola "Angola")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Flag_of_Argentina.svg/23px-Flag_of_Argentina.svg.png) [Argentina](https://en.wikipedia.org/wiki/Argentina "Argentina")[[34]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-34)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/23px-Flag_of_Australia_%28converted%29.svg.png) [Australia](https://en.wikipedia.org/wiki/Australia "Australia")[[32]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201467-32)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/23px-Flag_of_Austria.svg.png) [Austria](https://en.wikipedia.org/wiki/Austria "Austria"): 1,605 M1919A4s, known as MG-A4. Used into the 1970s.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/0/05/Flag_of_Brazil.svg/22px-Flag_of_Brazil.svg.png) [Brazil](https://en.wikipedia.org/wiki/Brazil "Brazil")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Bolivia.svg/22px-Flag_of_Bolivia.svg.png) [Bolivia](https://en.wikipedia.org/wiki/Bolivia "Bolivia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_Botswana.svg/23px-Flag_of_Botswana.svg.png) [Botswana](https://en.wikipedia.org/wiki/Botswana "Botswana")[[35]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-35)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Flag_of_Burundi.svg/23px-Flag_of_Burundi.svg.png) [Burundi](https://en.wikipedia.org/wiki/Burundi "Burundi")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Flag_of_Cambodia.svg/23px-Flag_of_Cambodia.svg.png) [Cambodia](https://en.wikipedia.org/wiki/Cambodia "Cambodia")[[37]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-37)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/23px-Flag_of_Canada_%28Pantone%29.svg.png) [Canada](https://en.wikipedia.org/wiki/Canada "Canada")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Flag_of_the_Central_African_Republic.svg/23px-Flag_of_the_Central_African_Republic.svg.png) [Central African Republic](https://en.wikipedia.org/wiki/Central_African_Republic "Central African Republic")[[38]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-38)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Flag_of_Chile.svg/23px-Flag_of_Chile.svg.png) [Chile](https://en.wikipedia.org/wiki/Chile "Chile")[[39]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-gander1995-39)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Flag_of_the_Republic_of_China.svg/23px-Flag_of_the_Republic_of_China.svg.png) [China](https://en.wikipedia.org/wiki/Republic_of_China_(1912%E2%80%931949) "Republic of China (1912–1949)"): M1919A4 Browning were used by the [X Force](https://en.wikipedia.org/wiki/X_Force "X Force") and M1919A6 Browning were later supplied by the [OSS](https://en.wikipedia.org/wiki/Office_of_Strategic_Services "Office of Strategic Services") to American trained Chinese commandos in 1945.[[40]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTEJowett200545-40)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colombia.svg/23px-Flag_of_Colombia.svg.png) [Colombia](https://en.wikipedia.org/wiki/Colombia "Colombia")[[41]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201466-41)[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Costa_Rica.svg/23px-Flag_of_Costa_Rica.svg.png) [Costa Rica](https://en.wikipedia.org/wiki/Costa_Rica "Costa Rica")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-   [![Cuba](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Flag_of_Cuba.svg/23px-Flag_of_Cuba.svg.png)](https://en.wikipedia.org/wiki/Cuba "Cuba") [Brigade 2506](https://en.wikipedia.org/wiki/Brigade_2506 "Brigade 2506")[[42]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-42)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Flag_of_Denmark.svg/20px-Flag_of_Denmark.svg.png) [Denmark](https://en.wikipedia.org/wiki/Denmark "Denmark"): _7.62 MG M/52_[[43]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTESmith1969326-43)[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_the_Dominican_Republic.svg/23px-Flag_of_the_Dominican_Republic.svg.png) [Dominican Republic](https://en.wikipedia.org/wiki/Dominican_Republic "Dominican Republic")[[44]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTESmith1969345-44)[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Egypt.svg/23px-Flag_of_Egypt.svg.png) [Egypt](https://en.wikipedia.org/wiki/Egypt "Egypt")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Flag_of_El_Salvador.svg/23px-Flag_of_El_Salvador.svg.png) [El Salvador](https://en.wikipedia.org/wiki/El_Salvador "El Salvador")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Flag_of_Estonia.svg/23px-Flag_of_Estonia.svg.png) [Estonia](https://en.wikipedia.org/wiki/Estonia "Estonia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Flag_of_Ethiopia_%281897-1936%3B_1941-1974%29.svg/23px-Flag_of_Ethiopia_%281897-1936%3B_1941-1974%29.svg.png) [Ethiopian Empire](https://en.wikipedia.org/wiki/Ethiopian_Empire "Ethiopian Empire")[[45]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-45)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/23px-Flag_of_Finland.svg.png) [Finland](https://en.wikipedia.org/wiki/Finland "Finland")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/23px-Flag_of_France.svg.png) [France](https://en.wikipedia.org/wiki/France "France")[[46]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-46)
-   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Flagge_FDLR.svg/23px-Flagge_FDLR.svg.png) [Democratic Forces for the Liberation of Rwanda](https://en.wikipedia.org/wiki/Democratic_Forces_for_the_Liberation_of_Rwanda "Democratic Forces for the Liberation of Rwanda")[[47]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FDLR-47)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Flag_of_Gabon.svg/20px-Flag_of_Gabon.svg.png) [Gabon](https://en.wikipedia.org/wiki/Gabon "Gabon")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Greece.svg/23px-Flag_of_Greece.svg.png) [Greece](https://en.wikipedia.org/wiki/Greece "Greece")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Flag_of_Guatemala.svg/23px-Flag_of_Guatemala.svg.png) [Guatemala](https://en.wikipedia.org/wiki/Guatemala "Guatemala")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Flag_of_Haiti.svg/23px-Flag_of_Haiti.svg.png) [Haiti](https://en.wikipedia.org/wiki/Haiti "Haiti")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Flag_of_Hong_Kong_%281959%E2%80%931997%29.svg/23px-Flag_of_Hong_Kong_%281959%E2%80%931997%29.svg.png) [Hong Kong](https://en.wikipedia.org/wiki/Hong_Kong "Hong Kong"): Used by the [Royal Hong Kong Regiment](https://en.wikipedia.org/wiki/Royal_Hong_Kong_Regiment "Royal Hong Kong Regiment").[[48]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-RHKR_Weapon-48)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/23px-Flag_of_India.svg.png) [India](https://en.wikipedia.org/wiki/India "India")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/23px-Flag_of_Indonesia.svg.png) [Indonesia](https://en.wikipedia.org/wiki/Indonesia "Indonesia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Flag_of_Iran.svg/23px-Flag_of_Iran.svg.png) [Iran](https://en.wikipedia.org/wiki/Iran "Iran")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Flag_of_Ireland.svg/23px-Flag_of_Ireland.svg.png) [Ireland](https://en.wikipedia.org/wiki/Republic_of_Ireland "Republic of Ireland")[[49]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-49)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/21px-Flag_of_Israel.svg.png) [Israel](https://en.wikipedia.org/wiki/Israel "Israel")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)[[50]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-50)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/0/03/Flag_of_Italy.svg/23px-Flag_of_Italy.svg.png) [Italy](https://en.wikipedia.org/wiki/Italy "Italy")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/9/9e/Flag_of_Japan.svg/23px-Flag_of_Japan.svg.png) [Japan](https://en.wikipedia.org/wiki/Japan "Japan"): used post-war[[51]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTESmith1969497-51)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Flag_of_Katanga.svg/23px-Flag_of_Katanga.svg.png) [Katanga](https://en.wikipedia.org/wiki/State_of_Katanga "State of Katanga")[[52]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-52)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Flag_of_South_Korea.svg/23px-Flag_of_South_Korea.svg.png) [South Korea](https://en.wikipedia.org/wiki/South_Korea "South Korea")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Flag_of_Laos_%281952%E2%80%931975%29.svg/23px-Flag_of_Laos_%281952%E2%80%931975%29.svg.png) [Kingdom of Laos](https://en.wikipedia.org/wiki/Kingdom_of_Laos "Kingdom of Laos"): Received M1919A4 and M1919A6 from US Government during [Vietnam War](https://en.wikipedia.org/wiki/Vietnam_War "Vietnam War") and [Laotian Civil War](https://en.wikipedia.org/wiki/Laotian_Civil_War "Laotian Civil War").[[53]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-53)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Flag_of_Lebanon.svg/23px-Flag_of_Lebanon.svg.png) [Lebanon](https://en.wikipedia.org/wiki/Lebanon "Lebanon")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Flag_of_Liberia.svg/23px-Flag_of_Liberia.svg.png) [Liberia](https://en.wikipedia.org/wiki/Liberia "Liberia")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Libya.svg/23px-Flag_of_Libya.svg.png) [Libya](https://en.wikipedia.org/wiki/Libya "Libya")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Flag_of_Luxembourg.svg/23px-Flag_of_Luxembourg.svg.png) [Luxembourg](https://en.wikipedia.org/wiki/Luxembourg "Luxembourg")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Madagascar.svg/23px-Flag_of_Madagascar.svg.png) [Madagascar](https://en.wikipedia.org/wiki/Madagascar "Madagascar")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Flag_of_Malta.svg/23px-Flag_of_Malta.svg.png) [Malta](https://en.wikipedia.org/wiki/Malta "Malta")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Flag_of_Mauritania.svg/23px-Flag_of_Mauritania.svg.png) [Mauritania](https://en.wikipedia.org/wiki/Mauritania "Mauritania")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/23px-Flag_of_Mexico.svg.png) [Mexico](https://en.wikipedia.org/wiki/Mexico "Mexico")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Flag_of_Mongolia.svg/23px-Flag_of_Mongolia.svg.png) [Mongolia](https://en.wikipedia.org/wiki/Mongolia "Mongolia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Morocco.svg/23px-Flag_of_Morocco.svg.png) [Morocco](https://en.wikipedia.org/wiki/Morocco "Morocco")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/23px-Flag_of_Myanmar.svg.png) [Myanmar](https://en.wikipedia.org/wiki/Myanmar "Myanmar")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Flag_of_Nepal.svg/16px-Flag_of_Nepal.svg.png) [Nepal](https://en.wikipedia.org/wiki/Nepal "Nepal")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/23px-Flag_of_New_Zealand.svg.png) [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand")[[54]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-54)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Nicaragua.svg/23px-Flag_of_Nicaragua.svg.png) [Nicaragua](https://en.wikipedia.org/wiki/Nicaragua "Nicaragua")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Flag_of_Nigeria.svg/23px-Flag_of_Nigeria.svg.png) [Nigeria](https://en.wikipedia.org/wiki/Nigeria "Nigeria")[[55]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-55)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Flag_of_Panama.svg/23px-Flag_of_Panama.svg.png) [Panama](https://en.wikipedia.org/wiki/Panama "Panama")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Flag_of_the_Philippines.svg/23px-Flag_of_the_Philippines.svg.png) [Philippines](https://en.wikipedia.org/wiki/Philippines "Philippines")[[56]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FOOTNOTERottman201476-56)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/23px-Flag_of_Portugal.svg.png) [Portugal](https://en.wikipedia.org/wiki/Portugal "Portugal")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Flag_of_Romania.svg/23px-Flag_of_Romania.svg.png) [Romania](https://en.wikipedia.org/wiki/Romania "Romania")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Flag_of_Rhodesia_%281968%E2%80%931979%29.svg/23px-Flag_of_Rhodesia_%281968%E2%80%931979%29.svg.png) [Rhodesia](https://en.wikipedia.org/wiki/Rhodesia "Rhodesia")[[31]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-britains-smallwars.com-31)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Flag_of_Sierra_Leone.svg/23px-Flag_of_Sierra_Leone.svg.png) [Sierra Leone](https://en.wikipedia.org/wiki/Sierra_Leone "Sierra Leone")[[57]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-57)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Flag_of_Somalia.svg/23px-Flag_of_Somalia.svg.png) [Somalia](https://en.wikipedia.org/wiki/Somalia "Somalia")[[58]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-SAS_2012_10-58)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Flag_of_South_Africa.svg/23px-Flag_of_South_Africa.svg.png) [South Africa](https://en.wikipedia.org/wiki/South_Africa "South Africa")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)[[59]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-59)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Flag_of_Spain.svg/23px-Flag_of_Spain.svg.png) [Spain](https://en.wikipedia.org/wiki/Spain "Spain")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/4/4c/Flag_of_Sweden.svg/23px-Flag_of_Sweden.svg.png) [Sweden](https://en.wikipedia.org/wiki/Sweden "Sweden")[[60]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-globalsecurity-60)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Flag_of_Syria_2011%2C_observed.svg/23px-Flag_of_Syria_2011%2C_observed.svg.png) [Syrian National Coalition](https://en.wikipedia.org/wiki/Syrian_opposition "Syrian opposition")[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Flag_of_the_Republic_of_China.svg/23px-Flag_of_the_Republic_of_China.svg.png) [Taiwan](https://en.wikipedia.org/wiki/Taiwan "Taiwan")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_Thailand.svg/23px-Flag_of_Thailand.svg.png) [Thailand](https://en.wikipedia.org/wiki/Thailand "Thailand")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Flag_of_Tunisia.svg/23px-Flag_of_Tunisia.svg.png) [Tunisia](https://en.wikipedia.org/wiki/Tunisia "Tunisia")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Flag_of_Turkey.svg/23px-Flag_of_Turkey.svg.png) [Turkey](https://en.wikipedia.org/wiki/Turkey "Turkey")[[61]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-61)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/23px-Flag_of_the_United_Kingdom.svg.png) [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom")[[62]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-62)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/23px-Flag_of_the_United_States.svg.png) [United States](https://en.wikipedia.org/wiki/United_States "United States")[[63]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-63)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Uruguay.svg/23px-Flag_of_Uruguay.svg.png) [Uruguay](https://en.wikipedia.org/wiki/Uruguay "Uruguay")[[36]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-jones2009-36)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Flag_of_South_Vietnam.svg/23px-Flag_of_South_Vietnam.svg.png) [South Vietnam](https://en.wikipedia.org/wiki/South_Vietnam "South Vietnam")[[64]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-64)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Vietnam.svg/23px-Flag_of_Vietnam.svg.png) [Vietnam](https://en.wikipedia.org/wiki/Vietnam "Vietnam")[[65]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-NVA-65)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Flag_of_Zaire_%281971%E2%80%931997%29.svg/23px-Flag_of_Zaire_%281971%E2%80%931997%29.svg.png) [Zaire](https://en.wikipedia.org/wiki/Zaire "Zaire")[[47]](https://en.wikipedia.org/wiki/M1919_Browning_machine_gun#cite_note-FDLR-47)
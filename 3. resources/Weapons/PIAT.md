# PIAT

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/PIAT#mw-head) [Jump to search](https://en.wikipedia.org/wiki/PIAT#searchInput)

Projector, Infantry, Anti Tank

[![PIAT cropped.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/PIAT_cropped.jpg/300px-PIAT_cropped.jpg)](https://en.wikipedia.org/wiki/File:PIAT_cropped.jpg)

PIAT at the [Museum of Army Flying](https://en.wikipedia.org/wiki/Museum_of_Army_Flying "Museum of Army Flying")

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/9dc93bb0-0d78-4fd5-8e2a-749e510e963c/dblrynq-976ad2fb-e398-4d99-8d0e-da020398018f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzlkYzkzYmIwLTBkNzgtNGZkNS04ZTJhLTc0OWU1MTBlOTYzY1wvZGJscnlucS05NzZhZDJmYi1lMzk4LTRkOTktOGQwZS1kYTAyMDM5ODAxOGYuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.uGCB3X5Al4HdXUYiSnHBw3VYNdNY7m3DpHMbnic4akg)

Type

Anti-tank weapon

Place of origin

United Kingdom

Service history

In service

1942–1950

Used by

[British Empire](https://en.wikipedia.org/wiki/British_Empire "British Empire") & [Commonwealth](https://en.wikipedia.org/wiki/Commonwealth_of_Nations "Commonwealth of Nations")

Wars

-   [Second World War](https://en.wikipedia.org/wiki/Second_World_War "Second World War")
-   [First Indochina War](https://en.wikipedia.org/wiki/First_Indochina_War "First Indochina War")
-   [1948 Arab–Israeli War](https://en.wikipedia.org/wiki/1948_Arab%E2%80%93Israeli_War "1948 Arab–Israeli War")
-   [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War")

Production history

Designer

[Major](https://en.wikipedia.org/wiki/Major_(United_Kingdom) "Major (United Kingdom)") [Millis Jefferis](https://en.wikipedia.org/wiki/Millis_Jefferis "Millis Jefferis")

Designed

1942

Manufacturer

[Imperial Chemical Industries](https://en.wikipedia.org/wiki/Imperial_Chemical_Industries "Imperial Chemical Industries") Ltd., various others.

Produced

August 1942[[1]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg44-1)

No. built

115,000[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2)

Specifications

Mass

32 lb (15 kg)[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2)

Length

39 in (0.99 m)[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2)

---

[Calibre](https://en.wikipedia.org/wiki/Caliber "Caliber")

83 mm (3.3 in)

[Muzzle velocity](https://en.wikipedia.org/wiki/Muzzle_velocity "Muzzle velocity")

250 ft/s (76 m/s)[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2)

Effective firing range

115 yd (105 m)[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3)

Maximum firing range

350 yd (320 m)[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3)

Sights

[Aperture sight](https://en.wikipedia.org/wiki/Aperture_sight "Aperture sight")

Filling

[Shaped charge](https://en.wikipedia.org/wiki/Shaped_charge "Shaped charge")[[4]](https://en.wikipedia.org/wiki/PIAT#cite_note-4)

Filling weight

2.5 lb (1.1 kg)[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2)

Detonation  
mechanism

Impact

The **Projector, Infantry, Anti Tank** (**PIAT**) Mk I was a British man-portable [anti-tank](https://en.wikipedia.org/wiki/Anti-tank "Anti-tank") weapon developed during the [Second World War](https://en.wikipedia.org/wiki/Second_World_War "Second World War"). The PIAT was designed in 1942 in response to the [British Army](https://en.wikipedia.org/wiki/British_Army "British Army")'s need for a more effective infantry anti-tank weapon and entered service in 1943.

The PIAT was based on the [spigot mortar](https://en.wikipedia.org/wiki/Spigot_mortar "Spigot mortar") system, and projected (launched) a 2.5 pound (1.1 kg) [shaped charge](https://en.wikipedia.org/wiki/Shaped_charge "Shaped charge") bomb using a cartridge in the tail of the projectile. It possessed an effective range of approximately 115 yards (105 m)[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3) in a [direct fire](https://en.wikipedia.org/wiki/Direct_fire "Direct fire") anti-tank role, and 350 yards (320 m)[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3) in an [indirect fire](https://en.wikipedia.org/wiki/Indirect_fire "Indirect fire") role. The PIAT had several advantages over other infantry anti-tank weapons of the period: it had greatly increased penetration power over the previous anti-tank rifles, it had no back-blast which might reveal the position of the user or accidentally injure friendly soldiers around the user, and it was simple in construction. However, the device also had some disadvantages: powerful recoil, a difficulty in cocking the weapon, and early problems with ammunition reliability.

The PIAT was first used during the [Tunisia Campaign](https://en.wikipedia.org/wiki/Tunisian_campaign "Tunisian campaign") in 1943, and remained in use with British and other Commonwealth forces until the early 1950s. PIATs were supplied to or obtained by other nations and forces, including the [Soviet Union](https://en.wikipedia.org/wiki/Soviet_Union "Soviet Union") (through [Lend Lease](https://en.wikipedia.org/wiki/Lend_Lease "Lend Lease")), the [French resistance](https://en.wikipedia.org/wiki/French_resistance "French resistance"), the [Polish Underground](https://en.wikipedia.org/wiki/Polish_Underground_State "Polish Underground State"), and the Israeli [Haganah](https://en.wikipedia.org/wiki/Haganah "Haganah") (which used PIATs during the [1948 Arab–Israeli War](https://en.wikipedia.org/wiki/1948_Arab%E2%80%93Israeli_War "1948 Arab–Israeli War")). Six members of the British and other Commonwealth armed forces received [Victoria Crosses](https://en.wikipedia.org/wiki/Victoria_Cross "Victoria Cross") for their use of the PIAT in combat.[[5]](https://en.wikipedia.org/wiki/PIAT#cite_note-5)

## Contents

-   [1 Development](https://en.wikipedia.org/wiki/PIAT#Development)
-   [2 Design](https://en.wikipedia.org/wiki/PIAT#Design)
-   [3 Ammunition and effect](https://en.wikipedia.org/wiki/PIAT#Ammunition_and_effect)
-   [4 Operational history](https://en.wikipedia.org/wiki/PIAT#Operational_history)
    -   [4.1 World War II](https://en.wikipedia.org/wiki/PIAT#World_War_II)
    -   [4.2 After World War II](https://en.wikipedia.org/wiki/PIAT#After_World_War_II)
-   [5 Users](https://en.wikipedia.org/wiki/PIAT#Users)
-   [6 Combat use](https://en.wikipedia.org/wiki/PIAT#Combat_use)
-   [7 See also](https://en.wikipedia.org/wiki/PIAT#See_also)
-   [8 Notes](https://en.wikipedia.org/wiki/PIAT#Notes)
-   [9 References](https://en.wikipedia.org/wiki/PIAT#References)
-   [10 Bibliography](https://en.wikipedia.org/wiki/PIAT#Bibliography)
-   [11 External links](https://en.wikipedia.org/wiki/PIAT#External_links)

## Development

At the beginning of the Second World War, the British Army possessed two primary anti-tank weapons for its infantry: the [Boys anti-tank rifle](https://en.wikipedia.org/wiki/Boys_anti-tank_rifle "Boys anti-tank rifle")[[6]](https://en.wikipedia.org/wiki/PIAT#cite_note-6) and the [No. 68 AT Rifle Grenade](https://en.wikipedia.org/wiki/No._68_AT_grenade "No. 68 AT grenade").[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2) However, neither of these was particularly effective as an anti-tank weapon. The No. 68 anti-tank grenade was designed to be fired from a discharger fitted onto the muzzle of an infantryman's rifle, but this meant that the grenade was too light to deal significant damage, resulting in its rarely being used in action.[[7]](https://en.wikipedia.org/wiki/PIAT#cite_note-Weeks-7) The Boys was also inadequate in the anti-tank role. It was heavy, which meant that it was difficult for infantry to handle effectively, and was outdated; by 1940 it was effective only at short ranges, and then only against [armoured cars](https://en.wikipedia.org/wiki/Armored_car_(military) "Armored car (military)") and [light tanks](https://en.wikipedia.org/wiki/Light_tank "Light tank"). In November 1941 during [Operation Crusader](https://en.wikipedia.org/wiki/Operation_Crusader "Operation Crusader"), part of the [North African Campaign](https://en.wikipedia.org/wiki/North_African_Campaign "North African Campaign"), staff officers of the [British Eighth Army](https://en.wikipedia.org/wiki/Eighth_Army_(United_Kingdom) "Eighth Army (United Kingdom)") were unable to find even a single instance of a Boys knocking out a German tank.[[8]](https://en.wikipedia.org/wiki/PIAT#cite_note-French-8)

Due to these limitations, a new infantry anti-tank weapon was required, and this ultimately came in the form of the Projector, Infantry, Anti-Tank, commonly abbreviated to PIAT. The origins of the PIAT can be traced back as far as 1888, when an American engineer by the name of [Charles Edward Munroe](https://en.wikipedia.org/wiki/Charles_Edward_Munroe "Charles Edward Munroe") was experimenting with [guncotton](https://en.wikipedia.org/wiki/Guncotton "Guncotton"); he discovered that the explosive would yield a great deal more damage if there were a recess in it facing the target. This phenomenon is known as the '[Munroe effect](https://en.wikipedia.org/wiki/Munroe_effect "Munroe effect")'. The German scientist Egon Neumann found that lining the recess with metal enhanced the damage dealt even more.[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2) By the 1930s [Henry Mohaupt](https://en.wikipedia.org/wiki/Henry_Mohaupt "Henry Mohaupt"), a Swiss engineer, had developed this technology even further and created shaped charge ammunition. This consisted of a recessed metal cone placed into an explosive warhead; when the warhead hit its target, the explosive detonated and turned the cone into an extremely high-speed spike. The speed of the spike, and the immense pressure it caused on impact allowed it to create a small hole in armour plating and send a large pressure wave and large amounts of fragments into the interior of the target. It was this technology that was utilized in the No. 68 anti-tank grenade.[[2]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan2-2)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/PIAT_AT-SPG_CDN_WWII.jpg/220px-PIAT_AT-SPG_CDN_WWII.jpg)](https://en.wikipedia.org/wiki/File:PIAT_AT-SPG_CDN_WWII.jpg)

PIAT and ammunition case at the [Canadian War Museum](https://en.wikipedia.org/wiki/Canadian_War_Museum "Canadian War Museum")

Although the technology existed, it remained for British designers to develop a system that could deliver shaped charge ammunition in a larger size and with a greater range than that possessed by the No. 68. At the same time that Mohaupt was developing shaped charge ammunition, [Lieutenant Colonel](https://en.wikipedia.org/wiki/Lieutenant_Colonel "Lieutenant Colonel") [Stewart Blacker](https://en.wikipedia.org/wiki/Stewart_Blacker "Stewart Blacker") of the [Royal Artillery](https://en.wikipedia.org/wiki/Royal_Artillery "Royal Artillery") was investigating the possibility of developing a lightweight [platoon](https://en.wikipedia.org/wiki/Platoon "Platoon") [mortar](https://en.wikipedia.org/wiki/Mortar_(weapon) "Mortar (weapon)").[[9]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg42-9) However, rather than using the conventional system of firing the mortar shell from a barrel fixed to a baseplate, Blacker wanted to use the [spigot mortar](https://en.wikipedia.org/wiki/Spigot_mortar "Spigot mortar") system. Instead of a barrel, there was a steel rod known as a 'spigot' fixed to a baseplate, and the bomb itself had a propellant charge inside its tail. When the mortar was to be fired, the bomb was pushed down onto the spigot, which exploded the propellant charge and blew the bomb into the air.[[9]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg42-9) By effectively putting the barrel on the inside of the weapon, the barrel diameter was no longer a limitation on the warhead size.[[10]](https://en.wikipedia.org/wiki/PIAT#cite_note-ODNB-10) Blacker eventually designed a lightweight mortar that he named the 'Arbalest' and submitted it to the [War Office](https://en.wikipedia.org/wiki/War_Office "War Office"),[[11]](https://en.wikipedia.org/wiki/PIAT#cite_note-FOOTNOTEEdgerton2011261-11) but it was turned down in favour of a Spanish design. Undeterred, however, Blacker continued with his experiments and decided to try to invent a hand-held anti-tank weapon based on the spigot design, but found that the spigot could not generate sufficient velocity needed to penetrate armour. But he did not abandon the design, and eventually came up with the [Blacker Bombard](https://en.wikipedia.org/wiki/Blacker_Bombard "Blacker Bombard"), a swivelling spigot-style system that could launch a 20-pound (9 kg) bomb approximately 100 yards (90 m). Although the bombs it fired could not actually penetrate armour, they could still severely damage tanks, and in 1940 a large number of Blacker Bombards were issued to the [Home Guard](https://en.wikipedia.org/wiki/Home_Guard_(United_Kingdom) "Home Guard (United Kingdom)") as anti-tank weapons.[[12]](https://en.wikipedia.org/wiki/PIAT#cite_note-12)

PIAT ammunition

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/PIAT_projectile_diagram.jpg/134px-PIAT_projectile_diagram.jpg)](https://en.wikipedia.org/wiki/File:PIAT_projectile_diagram.jpg)

1945 diagram of a PIAT bomb

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Hohlladungsgeschoss.jpg/70px-Hohlladungsgeschoss.jpg)](https://en.wikipedia.org/wiki/File:Hohlladungsgeschoss.jpg)

A round on museum display

When Blacker became aware of the existence of shaped charge ammunition, he realized that it was exactly the kind of ammunition he was looking for to develop a hand-held anti-tank weapon, as it depended upon the energy contained within itself, and not the sheer velocity at which it was fired.[[13]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg43-13) Blacker then developed a shaped charge bomb with a propellant charge in its tail, which fitted into a shoulder-fired launcher that consisted of a metal casing containing a large spring and a spigot; the bomb was placed into a trough at the front of the casing, and when the trigger was pulled the spigot rammed into the tail of the bomb and fired it out of the casing and up to approximately 140 metres (150 yd) away. Blacker called the weapon the 'Baby Bombard', and presented it to the War Office in 1941.[[13]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg43-13) However, when the weapon was tested it proved to have a host of problems; a War Office report of June 1941 stated that the casing was flimsy and the spigot itself did not always fire when the trigger was pulled, and none of the bombs provided exploded upon contact with the target.[[14]](https://en.wikipedia.org/wiki/PIAT#cite_note-14)

At the time that he developed the Baby Bombard and sent it off the War Office, Blacker was working for a government department known as [MD1](https://en.wikipedia.org/wiki/MD1 "MD1"), which was given the task of developing and delivering weapons for use by guerrilla and resistance groups in Occupied Europe.[[1]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg44-1) Shortly after the trial of the Baby Bombard, Blacker was posted to other duties, and left the anti-tank weapon in the hands of a colleague in the department, [Major](https://en.wikipedia.org/wiki/Major_(United_Kingdom) "Major (United Kingdom)") [Millis Jefferis](https://en.wikipedia.org/wiki/Millis_Jefferis "Millis Jefferis").[[1]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg44-1)

Jefferis took the prototype Baby Bombard apart on the floor of his office in MD1 and rebuilt it, and then combined it with a shaped charge mortar bomb to create what he called the 'Jefferis Shoulder Gun'. Jefferis then had a small number of prototype armour-piercing [HEAT](https://en.wikipedia.org/wiki/High_explosive_anti-tank_warhead "High explosive anti-tank warhead") rounds made, and took the weapon to be tested at the Small Arms School at [Bisley](https://en.wikipedia.org/wiki/Bisley,_Surrey#Rifle_range "Bisley, Surrey").[[15]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan23-15) A [Warrant Officer](https://en.wikipedia.org/wiki/Warrant_Officer "Warrant Officer") took the Shoulder Gun down to a firing range, aimed it at an armoured target, and pulled the trigger; the Shoulder Gun pierced a hole in the target, but unfortunately also wounded the Warrant Officer when a piece of metal from the exploding round flew back and hit him.[[15]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan23-15) Jefferis himself then took the place of the Warrant Officer and fired off several more rounds, all of which pierced the armoured target but without wounding him. Impressed with the weapon, the Ordnance Board of the [Small Arms School](https://en.wikipedia.org/wiki/Small_Arms_School_Corps "Small Arms School Corps") had the faults with the ammunition corrected, renamed the Shoulder Gun as the Projector, Infantry, Anti Tank, and ordered that it be issued to infantry units as a hand-held anti-tank weapon.[[16]](https://en.wikipedia.org/wiki/PIAT#cite_note-16) Production of the PIAT began at the end of August 1942.[[1]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg44-1)

There was disagreement over the name to be given to the new weapon. A press report in 1944 gave credit for both the PIAT and the Blacker Bombard to Jefferis. Blacker took exception to this and suggested to Jefferis that they should divide any award equally after his expenses had been deducted.[[17]](https://en.wikipedia.org/wiki/PIAT#cite_note-FOOTNOTEEdgerton2011160-17) The Ministry of Supply had already paid Blacker £50,000 for his expenses in relation to the Bombard and PIAT.[[18]](https://en.wikipedia.org/wiki/PIAT#cite_note-FOOTNOTEEdgerton2011161-18) Churchill himself got involved in the argument; writing to the Secretary of State for war in January 1943 he asked "Why should the name _Jefferis shoulder gun_ be changed to PIAT? Nobody objected to the Boys rifle, although that had a rather odd ring." [[18]](https://en.wikipedia.org/wiki/PIAT#cite_note-FOOTNOTEEdgerton2011161-18) Churchill supported Jefferis claims, but he did not get his way.[[18]](https://en.wikipedia.org/wiki/PIAT#cite_note-FOOTNOTEEdgerton2011161-18) For his part Blacker received £25,000 from the [Royal Commission on Awards to Inventors](https://en.wikipedia.org/wiki/Royal_Commission_on_Awards_to_Inventors "Royal Commission on Awards to Inventors").[[10]](https://en.wikipedia.org/wiki/PIAT#cite_note-ODNB-10)[[a]](https://en.wikipedia.org/wiki/PIAT#cite_note-20)

## Design

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/PIAT_Projectile_CMHM_Brantford.jpg/220px-PIAT_Projectile_CMHM_Brantford.jpg)](https://en.wikipedia.org/wiki/File:PIAT_Projectile_CMHM_Brantford.jpg)

PIAT [HEAT](https://en.wikipedia.org/wiki/HEAT "HEAT") Projectile, Canadian Military Heritage Museum, Brantford, Ontario (2007)

The PIAT was 39 inches (0.99 m) long and weighed 32 pounds (15 kg), with an effective direct fire range of approximately 115 yards (105 m) and a maximum indirect fire range of 350 yards (320 m).[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3) It could be carried and operated by one man,[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3) but was usually assigned to a two-man team,[[20]](https://en.wikipedia.org/wiki/PIAT#cite_note-Bishop,_p._211-21) the second man acting as an ammunition carrier and loader. The body of the PIAT launcher was a tube constructed out of thin sheets of steel, containing the spigot mechanism, trigger mechanism and firing spring. At the front of the launcher was a small trough in which the bomb was placed, and the movable spigot ran along the axis of the launcher and into the trough.[[7]](https://en.wikipedia.org/wiki/PIAT#cite_note-Weeks-7) Padding for the user's shoulder was fitted to the other end of the launcher, and rudimentary [aperture sights](https://en.wikipedia.org/wiki/Peep_sight "Peep sight") were fitted on top for aiming; the bombs launched by the PIAT possessed hollow tubular tails, into which a small propellant cartridge was inserted, and shaped charge warheads.[[7]](https://en.wikipedia.org/wiki/PIAT#cite_note-Weeks-7)

Conventional spigot mortar designs have a fixed spigot rod, for example the [Blacker Bombard](https://en.wikipedia.org/wiki/Blacker_Bombard "Blacker Bombard"). The moving spigot rod in the PIAT design was unusual, and served to help reduce recoil sufficiently to make it a viable shoulder fired weapon.[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3)

The PIAT was a little lighter (15kg vs 16kg) and smaller (0.99m long vs 1.57m) than its predecessor, the [Boys anti-tank rifle](https://en.wikipedia.org/wiki/Boys_anti-tank_rifle "Boys anti-tank rifle"), although it was heavier than the contemporary [Bazooka](https://en.wikipedia.org/wiki/Bazooka "Bazooka") (18 lbs/8.2 kg).

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/A_PIAT_%28Projectile_Infantry_Anti-Tank%29_in_action_at_a_firing_range_in_Tunisia%2C_19_February_1943._NA756.jpg/220px-A_PIAT_%28Projectile_Infantry_Anti-Tank%29_in_action_at_a_firing_range_in_Tunisia%2C_19_February_1943._NA756.jpg)](https://en.wikipedia.org/wiki/File:A_PIAT_(Projectile_Infantry_Anti-Tank)_in_action_at_a_firing_range_in_Tunisia,_19_February_1943._NA756.jpg)

A PIAT team at a firing range in Tunisia, 19 February 1943; part of a demonstration team. Note the cardboard three-round ammunition case

To prepare the weapon for firing the spigot mechanism, which was operated by a large spring, had to be cocked, and to do this was a difficult and awkward process. The user had to first place the PIAT on its [butt](https://en.wikipedia.org/wiki/Stock_(firearm) "Stock (firearm)"), then place two feet on the shoulder padding and turn the weapon to unlock the body and simultaneously lock the spigot rod to the butt; the user would then have to bend over and pull the body of the weapon upwards, thereby pulling the spring back until it attached to the trigger and cocking the weapon. Once this was achieved, the body was then lowered and turned to reattach it to the rest of the weapon, and the PIAT could then be fired.[[21]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg45-22) Users of a small stature often found the cocking sequence challenging, as they did not have the sufficient height required to pull the body up far enough to cock the weapon; it was also difficult to do when lying in a [prone position](https://en.wikipedia.org/wiki/Prone_position "Prone position"), as was often the case when using the weapon in action.[[22]](https://en.wikipedia.org/wiki/PIAT#cite_note-23)

Note, however, that troops were trained to cock the PIAT before expected use, and "in action the projector will always be carried cocked" (but unloaded).[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3) Unless a stoppage occurred, it would not normally be necessary to manually re-cock the weapon in action.

When the trigger was pulled, the spring pushed the spigot rod (which has a fixed firing pin on the end) forwards into the bomb, which aligned the bomb, ignited the propellant cartridge in the bomb and launched it along the rod and into the air. The recoil caused by the detonation of the propellant blew the spigot rod backwards onto the spring; this reduced the shock of recoil and automatically cocked the weapon for subsequent shots, eliminating the need to manually re-cock.[[7]](https://en.wikipedia.org/wiki/PIAT#cite_note-Weeks-7)[[21]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hogg45-22)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Australian_PIAT_team_Balikpapan.jpg/220px-Australian_PIAT_team_Balikpapan.jpg)](https://en.wikipedia.org/wiki/File:Australian_PIAT_team_Balikpapan.jpg)

An Australian PIAT team during the [Battle of Balikpapan](https://en.wikipedia.org/wiki/Battle_of_Balikpapan_(1945) "Battle of Balikpapan (1945)"), 1945

Tactical training emphasized that it was best utilized with surprise and concealment on the side of the PIAT team, and where possible enemy armoured vehicles should be engaged from the flank or rear.[[23]](https://en.wikipedia.org/wiki/PIAT#cite_note-Bull-24) Due to the short engagement distances and the power of the bomb, the crew could be in the bomb blast zone so hard cover was desirable; on open training grounds this might be a [slit trench](https://en.wikipedia.org/wiki/Slit_trench "Slit trench").[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3) The PIAT was often also used in combat to knock out enemy positions located in houses and bunkers.[[23]](https://en.wikipedia.org/wiki/PIAT#cite_note-Bull-24) It was possible to use the PIAT as a crude [mortar](https://en.wikipedia.org/wiki/Mortar_(weapon) "Mortar (weapon)") by placing the shoulder pad of the weapon on the ground and supporting it.

Despite the difficulties in cocking and firing the weapon, it did have several advantages. The Spigot mortar design allowed a large calibre powerful shaped charge bomb[[7]](https://en.wikipedia.org/wiki/PIAT#cite_note-Weeks-7) giving greatly increased penetration power over the previous anti-tank rifles, allowing it to remain effective for the rest of the war; its construction was relatively simple and robust without a conventional barrel; there was no back-blast (unlike the contemporary American [bazooka](https://en.wikipedia.org/wiki/Bazooka "Bazooka")) that might endanger friendly troops and give the user's position away, this also meant that the PIAT could be used in confined spaces as in urban warfare; compared to the previous anti-tank rifles the muzzle blast was minimal, also a potential concealment issue. However, the weapon did have drawbacks. It was very heavy and bulky, which meant that it was often unpopular with infantry required to carry it.[[20]](https://en.wikipedia.org/wiki/PIAT#cite_note-Bishop,_p._211-21) There were also problems with early ammunition reliability and accuracy. Although the PIAT was theoretically able to penetrate approximately 100 millimetres (4 in) of armour, field experience during the [Allied invasion of Sicily](https://en.wikipedia.org/wiki/Allied_invasion_of_Sicily "Allied invasion of Sicily"), which was substantiated by trials conducted during 1944, demonstrated that this capability was often nullified by problems of accuracy and round reliability. During these trials, a skilled user was unable to hit a target more than 60% of the time at 100 yards (90 m), and faulty fuses meant that only 75% of the bombs fired detonated on-target.[[8]](https://en.wikipedia.org/wiki/PIAT#cite_note-French-8)

## Ammunition and effect

The PIATs' ammunition used the shaped charge principle, which, if the often unreliable early round design delivered it correctly to the target, allowed the warhead to penetrate almost all enemy armour types at close range.[[24]](https://en.wikipedia.org/wiki/PIAT#cite_note-25)

The following ammunition types were available in 1943.[[3]](https://en.wikipedia.org/wiki/PIAT#cite_note-War_Office_Manual-3)

-   Service Bomb - "Bomb, HE/AT"
    -   Manual says green, but museum examples seem to be brown.
    -   AT shaped charge warhead design. Supplied with the propellant cartridge fitted and the fuse separate.
    -   Versions:
        -   Mark I, 1942, Nobels 808 plastic explosive filling, green band
        -   Mark IA, Reinforced central tube
        -   Mark II, Revised nose fuse
        -   Mark III, Revised nose fuse, TNT filling, blue band
        -   Mark IV, July 1944, Revised construction to reduce rearward fragmentation and "back blast" of warhead explosion.
    -   Also useful as a general-purpose HE blast type round.
-   Drill -"Bomb, Drill/AT"
    -   Black, marked "Drill"
    -   Same shape as a live round, for dry loading practice. Cannot be fired or dry fired.
-   Practice Bomb - "Shot, Practice/AT"
    -   White
    -   Cylindrical thick steel construction, effectively a sub-calibre practice round. The PIAT requires a trough-like adapter to use it. Economical as it may be fired many times with new propellant cartridges. Trajectory slightly different to service bomb.
-   Inert - "Bomb, Practice Inert/AT"
    -   Black, yellow ring, marked "Inert"
    -   Same size and weight as a live round, no warhead, but has a live propellant cartridge. It can be fired once from a standard PIAT, it is not re-usable.

Rounds were supplied in three-round ammunition cases with the propellant cartridge fitted and the fuses separate.

Getting the bomb to detonate reliably against angled targets was troublesome and was addressed with revised fusing. See also the [bazooka](https://en.wikipedia.org/wiki/Bazooka "Bazooka"), which had similar early problems.

The 1943 manual simply describes the service bomb as "H.E." or "HE/AT" and does not mention shaped charge as such. It notes that the bomb has "Excellent penetration. The bomb can penetrate the armour of the latest known types of enemy A.F.Vs. and a considerable thickness of reinforced concrete". It also notes that it may be used "as a house-breaker".

## Operational history

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Warsaw_Uprising_-_Baon_Czata_with_PIAT_guns.jpg/220px-Warsaw_Uprising_-_Baon_Czata_with_PIAT_guns.jpg)](https://en.wikipedia.org/wiki/File:Warsaw_Uprising_-_Baon_Czata_with_PIAT_guns.jpg)

[Warsaw Uprising](https://en.wikipedia.org/wiki/Warsaw_Uprising "Warsaw Uprising") combatants display PIAT weapons.

### World War II

The PIAT was used in all theatres in which British and other [Commonwealth](https://en.wikipedia.org/wiki/Commonwealth_of_Nations "Commonwealth of Nations") forces served.[[15]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan23-15)

It entered service in mid-1943, and was first used in action during the Tunisia Campaign.[[25]](https://en.wikipedia.org/wiki/PIAT#cite_note-26)[[26]](https://en.wikipedia.org/wiki/PIAT#cite_note-27) The 1944 war establishment for a British [platoon](https://en.wikipedia.org/wiki/Platoon "Platoon"), which contained 36 men, had a single PIAT attached to the platoon headquarters, alongside a [2-inch (51 mm) mortar](https://en.wikipedia.org/wiki/Two-inch_mortar "Two-inch mortar") detachment.[[27]](https://en.wikipedia.org/wiki/PIAT#cite_note-28) Three PIATs were issued to every company at the headquarters level for issuing at the CO discretion – allowing one weapon for each platoon.[[23]](https://en.wikipedia.org/wiki/PIAT#cite_note-Bull-24) British Army and [Royal Marines](https://en.wikipedia.org/wiki/Royal_Marines "Royal Marines") [commandos](https://en.wikipedia.org/wiki/British_Commandos "British Commandos") were also issued with PIATs and used them in action.[[28]](https://en.wikipedia.org/wiki/PIAT#cite_note-29)

In [Australian Army](https://en.wikipedia.org/wiki/Australian_Army "Australian Army") service, the PIAT was also known as "Projector Infantry Tank Attack" (**PITA**). From 1943, one PIAT team was allocated to each infantry platoon in a [jungle division](https://en.wikipedia.org/wiki/Jungle_Division "Jungle Division")[[29]](https://en.wikipedia.org/wiki/PIAT#cite_note-Kuring_173-30) – the tropical light infantry formation that was the standard front-line Australian division in the [South West Pacific theatre](https://en.wikipedia.org/wiki/South_West_Pacific_theatre "South West Pacific theatre"). It was used against Japanese tanks, other vehicles and fortifications during the [Borneo campaign of 1945](https://en.wikipedia.org/wiki/Borneo_campaign_(1945) "Borneo campaign (1945)").

A contemporary (1944–45) Canadian Army survey questioned 161 army officers, who had recently left combat, about the effectiveness of 31 different infantry weapons. In that survey the PIAT was ranked the number one most “outstandingly effective” weapon, followed by the [Bren gun](https://en.wikipedia.org/wiki/Bren_gun "Bren gun") in second place.[[30]](https://en.wikipedia.org/wiki/PIAT#cite_note-Battle_Experience_Questionnaires,_Vol._10,450-31)

An analysis by British staff officers of the initial period of the [Normandy campaign](https://en.wikipedia.org/wiki/Invasion_of_Normandy "Invasion of Normandy") found that 7% of all German tanks destroyed by British forces were knocked out by PIATs, compared to 6% by rockets fired by aircraft. However, they also found that once German tanks had been fitted with [armoured skirts](https://en.wikipedia.org/wiki/Vehicle_armour#Spaced "Vehicle armour") that detonated shaped charge ammunition before it could penetrate the tank's armour, the weapon became much less effective.[[8]](https://en.wikipedia.org/wiki/PIAT#cite_note-French-8)

As part of the [Anglo-Soviet Military Supplies Agreement](https://en.wikipedia.org/w/index.php?title=Anglo-Soviet_Military_Supplies_Agreement&action=edit&redlink=1 "Anglo-Soviet Military Supplies Agreement (page does not exist)"), by 31 March 1946 the Soviet Union had been supplied with 1,000 PIATs and 100,000 rounds of ammunition.[[31]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hansard-32) The PIAT was also utilized by resistance groups in [Occupied Europe](https://en.wikipedia.org/wiki/Occupied_Europe "Occupied Europe"). During the [Warsaw Uprising](https://en.wikipedia.org/wiki/Warsaw_Uprising "Warsaw Uprising"), it was one of many weapons that [Polish Underground resistance fighters](https://en.wikipedia.org/wiki/Polish_Underground_State "Polish Underground State") used against German forces.[[32]](https://en.wikipedia.org/wiki/PIAT#cite_note-bruce145-33) And in occupied France, the [French resistance](https://en.wikipedia.org/wiki/French_resistance "French resistance") used the PIAT in the absence of mortars or artillery.[[33]](https://en.wikipedia.org/wiki/PIAT#cite_note-crowdy63-34)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/The_British_Army_in_North-west_Europe_1944-45_B11928.jpg/220px-The_British_Army_in_North-west_Europe_1944-45_B11928.jpg)](https://en.wikipedia.org/wiki/File:The_British_Army_in_North-west_Europe_1944-45_B11928.jpg)

A soldier of the [Duke of Cornwall's Light Infantry](https://en.wikipedia.org/wiki/Duke_of_Cornwall%27s_Light_Infantry "Duke of Cornwall's Light Infantry") carrying a PIAT, November 1944

Six [Victoria Crosses](https://en.wikipedia.org/wiki/Victoria_Cross "Victoria Cross") were awarded to members of the British and other Commonwealth armed forces for actions using the PIAT:[[34]](https://en.wikipedia.org/wiki/PIAT#cite_note-35)

-   On 16 May 1944, during the [Italian Campaign](https://en.wikipedia.org/wiki/Italian_Campaign_(World_War_II) "Italian Campaign (World War II)"), [Fusilier](https://en.wikipedia.org/wiki/Fusilier "Fusilier") [Frank Jefferson](https://en.wikipedia.org/wiki/Frank_Jefferson "Frank Jefferson") used a PIAT to destroy a [Panzer IV](https://en.wikipedia.org/wiki/Panzer_IV "Panzer IV") tank and repel a German counterattack launched against his unit as they assaulted a section of the [Gustav Line](https://en.wikipedia.org/wiki/Winter_Line "Winter Line").[[35]](https://en.wikipedia.org/wiki/PIAT#cite_note-36)
-   On 6 June 1944, [Company Sergeant Major](https://en.wikipedia.org/wiki/Company_Sergeant_Major "Company Sergeant Major") [Stanley Hollis](https://en.wikipedia.org/wiki/Stanley_Hollis "Stanley Hollis"), in one of several actions that day, used a PIAT in an attack against a German field gun.[[36]](https://en.wikipedia.org/wiki/PIAT#cite_note-37)
-   On 12 June 1944 Rifleman [Ganju Lama](https://en.wikipedia.org/wiki/Ganju_Lama "Ganju Lama") of the [7th Gurkha Rifles](https://en.wikipedia.org/wiki/7th_Gurkha_Rifles "7th Gurkha Rifles") used a PIAT to knock out two Japanese tanks attacking his unit at [Ningthoukhong](https://en.wikipedia.org/wiki/Ningthoukhong "Ningthoukhong"), [Manipur](https://en.wikipedia.org/wiki/Manipur_(princely_state) "Manipur (princely state)") (given as Burma in the official citation). Despite sustaining injuries, Ganju Lama approached within thirty yards (27 m) of the enemy tanks, and having knocked them out moved on to attack the crews as they tried to escape.[[37]](https://en.wikipedia.org/wiki/PIAT#cite_note-38) When asked by his Army Commander, [William Slim](https://en.wikipedia.org/wiki/William_Slim "William Slim"), why he went so close, he replied he was not certain of hitting with a PIAT beyond thirty yards (27 m).[[38]](https://en.wikipedia.org/wiki/PIAT#cite_note-39)
-   Between 19 and 25 September 1944, during the [Battle of Arnhem](https://en.wikipedia.org/wiki/Battle_of_Arnhem "Battle of Arnhem"), [Major](https://en.wikipedia.org/wiki/Major "Major") [Robert Henry Cain](https://en.wikipedia.org/wiki/Robert_Henry_Cain "Robert Henry Cain") used a PIAT to disable an [assault gun](https://en.wikipedia.org/wiki/Stug_III "Stug III") that was advancing on his company position, and to force another three German [Panzer IV](https://en.wikipedia.org/wiki/Panzer_IV "Panzer IV") tanks to retreat during a later assault.[[39]](https://en.wikipedia.org/wiki/PIAT#cite_note-40)
-   On the night of 21/22 October 1944, [Private](https://en.wikipedia.org/wiki/Private_(rank) "Private (rank)") [Ernest Alvia ("Smokey") Smith](https://en.wikipedia.org/wiki/Ernest_Smith "Ernest Smith") used a PIAT to destroy a German [Panther tank](https://en.wikipedia.org/wiki/Panther_tank "Panther tank"), one of three Panthers and two self-propelled guns attacking his small group. The self-propelled vehicles were also knocked out. He then used a [Thompson submachine gun](https://en.wikipedia.org/wiki/Thompson_submachine_gun "Thompson submachine gun") to kill or repel about 30 enemy soldiers. His actions secured a bridgehead on the [Savio River](https://en.wikipedia.org/wiki/Savio_River "Savio River") in Italy.[[40]](https://en.wikipedia.org/wiki/PIAT#cite_note-41)
-   On 9 December 1944, while defending positions in [Faenza](https://en.wikipedia.org/wiki/Faenza "Faenza"), Italy, [Captain](https://en.wikipedia.org/wiki/Captain_(British_Army_and_Royal_Marines) "Captain (British Army and Royal Marines)") [John Henry Cound Brunt](https://en.wikipedia.org/wiki/John_Brunt "John Brunt") used a PIAT, amongst other weapons, to help repel an attack by the German [90th Panzergrenadier Division](https://en.wikipedia.org/wiki/90th_Light_Infantry_Division_(Wehrmacht) "90th Light Infantry Division (Wehrmacht)").[[41]](https://en.wikipedia.org/wiki/PIAT#cite_note-42)

### After World War II

The PIAT remained in service until the early 1950s, when it was replaced initially by the [ENERGA anti-tank rifle grenade](https://en.wikipedia.org/wiki/ENERGA_anti-tank_rifle_grenade "ENERGA anti-tank rifle grenade") and then the [American](https://en.wikipedia.org/wiki/United_States_Army "United States Army") [M20 "Super Bazooka"](https://en.wikipedia.org/wiki/Bazooka "Bazooka").[[15]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan23-15) The Australian Army briefly used PIATs at the start of the [Korean War](https://en.wikipedia.org/wiki/Korean_War "Korean War") alongside [2.36-inch (60 mm) bazookas](https://en.wikipedia.org/wiki/Bazooka#Rocket_Launcher,_M1_"Bazooka" "Bazooka"), but quickly replaced both weapons with 3.5-inch (89 mm) M20 "Super Bazookas".[[42]](https://en.wikipedia.org/wiki/PIAT#cite_note-43)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Beyt-gidi-381.jpg/220px-Beyt-gidi-381.jpg)](https://en.wikipedia.org/wiki/File:Beyt-gidi-381.jpg)

PIAT in the [Etzel](https://en.wikipedia.org/wiki/Irgun "Irgun") Museum ([Beit Gidi](https://en.wikipedia.org/wiki/Etzel_House "Etzel House")), [Tel Aviv](https://en.wikipedia.org/wiki/Tel_Aviv "Tel Aviv"), Israel

The [Haganah](https://en.wikipedia.org/wiki/Haganah "Haganah") and the emerging [Israel Defence Force](https://en.wikipedia.org/wiki/Israel_Defence_Force "Israel Defence Force") (IDF) used PIATs against Arab armour during the [1947–1949 Palestine war](https://en.wikipedia.org/wiki/1947%E2%80%931949_Palestine_war "1947–1949 Palestine war").[[43]](https://en.wikipedia.org/wiki/PIAT#cite_note-laffin30-44)

PIATs were also used by French and [Việt Minh](https://en.wikipedia.org/wiki/Vi%E1%BB%87t_Minh "Việt Minh") forces during the [First Indochina War](https://en.wikipedia.org/wiki/First_Indochina_War "First Indochina War").[[44]](https://en.wikipedia.org/wiki/PIAT#cite_note-Viet_Minh-45)

## Users

Some of the users of the PIAT included:

-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/23px-Flag_of_Australia_%28converted%29.svg.png) [Australia](https://en.wikipedia.org/wiki/Australia "Australia")[[29]](https://en.wikipedia.org/wiki/PIAT#cite_note-Kuring_173-30)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_Belgium_%28civil%29.svg/23px-Flag_of_Belgium_%28civil%29.svg.png) [Belgium](https://en.wikipedia.org/wiki/Belgium "Belgium")[[45]](https://en.wikipedia.org/wiki/PIAT#cite_note-46)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Flag_of_Canada_%281921%E2%80%931957%29.svg/23px-Flag_of_Canada_%281921%E2%80%931957%29.svg.png) [Canada](https://en.wikipedia.org/wiki/Canada "Canada")[[15]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan23-15)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Flag_of_Free_France_%281940-1944%29.svg/23px-Flag_of_Free_France_%281940-1944%29.svg.png) [Free French Forces](https://en.wikipedia.org/wiki/Free_France "Free France")[[33]](https://en.wikipedia.org/wiki/PIAT#cite_note-crowdy63-34)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Flag_of_Greece_%281822-1978%29.svg/23px-Flag_of_Greece_%281822-1978%29.svg.png) [Kingdom of Greece](https://en.wikipedia.org/wiki/Kingdom_of_Greece "Kingdom of Greece")
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/23px-Flag_of_India.svg.png) [India](https://en.wikipedia.org/wiki/India "India")[[46]](https://en.wikipedia.org/wiki/PIAT#cite_note-47)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/21px-Flag_of_Israel.svg.png) [Israel](https://en.wikipedia.org/wiki/Israel "Israel")[[43]](https://en.wikipedia.org/wiki/PIAT#cite_note-laffin30-44)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/0/03/Flag_of_Italy.svg/23px-Flag_of_Italy.svg.png) [Italy](https://en.wikipedia.org/wiki/Italy "Italy") ([Co-Belligerent Army](https://en.wikipedia.org/wiki/Italian_Co-Belligerent_Army "Italian Co-Belligerent Army") and [partisans](https://en.wikipedia.org/wiki/Italian_resistance_movement "Italian resistance movement"))[[47]](https://en.wikipedia.org/wiki/PIAT#cite_note-48)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Flag_of_Luxembourg.svg/23px-Flag_of_Luxembourg.svg.png) [Luxembourg](https://en.wikipedia.org/wiki/Luxembourg "Luxembourg")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/23px-Flag_of_the_Netherlands.svg.png) [The Netherlands](https://en.wikipedia.org/wiki/Netherlands "Netherlands") known in Dutch service as _granaatwerper tp (tegen pantser)_ ("grenade launcher anti-tank") entered service in 1943, with Dutch forces fighting under British command. It served into the 1950s.[[48]](https://en.wikipedia.org/wiki/PIAT#cite_note-49)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/23px-Flag_of_New_Zealand.svg.png) [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand")[[49]](https://en.wikipedia.org/wiki/PIAT#cite_note-50)
-   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Flaga_PPP.svg/23px-Flaga_PPP.svg.png) [Polish Underground](https://en.wikipedia.org/wiki/Polish_Underground_State "Polish Underground State")[[32]](https://en.wikipedia.org/wiki/PIAT#cite_note-bruce145-33)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_the_Soviet_Union.svg/23px-Flag_of_the_Soviet_Union.svg.png) [Soviet Union](https://en.wikipedia.org/wiki/Soviet_Union "Soviet Union")[[31]](https://en.wikipedia.org/wiki/PIAT#cite_note-Hansard-32)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/23px-Flag_of_the_United_Kingdom.svg.png) [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom")[[15]](https://en.wikipedia.org/wiki/PIAT#cite_note-Khan23-15)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Flag_of_Yugoslavia_%281946-1992%29.svg/23px-Flag_of_Yugoslavia_%281946-1992%29.svg.png) [Yugoslavia](https://en.wikipedia.org/wiki/Socialist_Federal_Republic_of_Yugoslavia "Socialist Federal Republic of Yugoslavia") Used by [Yugoslav partisans](https://en.wikipedia.org/wiki/Yugoslav_partisans "Yugoslav partisans")[[50]](https://en.wikipedia.org/wiki/PIAT#cite_note-51)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Flag_of_Malaysia.svg/23px-Flag_of_Malaysia.svg.png) [Malaysia](https://en.wikipedia.org/wiki/Malaysia "Malaysia")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Flag_of_the_Philippines.svg/23px-Flag_of_the_Philippines.svg.png) [Philippines](https://en.wikipedia.org/wiki/Philippines "Philippines")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_North_Vietnam_%281955%E2%80%931976%29.svg/23px-Flag_of_North_Vietnam_%281955%E2%80%931976%29.svg.png) [North Vietnam](https://en.wikipedia.org/wiki/North_Vietnam "North Vietnam") Captured from [France](https://en.wikipedia.org/wiki/France "France")[[51]](https://en.wikipedia.org/wiki/PIAT#cite_note-52)

## Combat use

World War II:

-   [Battle of Normandy](https://en.wikipedia.org/wiki/Operation_Overlord "Operation Overlord") (France 1944)
-   [Battle of Arnhem](https://en.wikipedia.org/wiki/Battle_of_Arnhem "Battle of Arnhem") (Netherlands, 1944)
-   [Battle of Ortona](https://en.wikipedia.org/wiki/Battle_of_Ortona "Battle of Ortona") (Italy, 1943)
-   [Battle of Villers-Bocage](https://en.wikipedia.org/wiki/Battle_of_Villers-Bocage "Battle of Villers-Bocage") (Normandy, France, 1944)
-   [Operation Epsom](https://en.wikipedia.org/wiki/Operation_Epsom "Operation Epsom") (Normandy, France, 1944)
-   [Operation Perch](https://en.wikipedia.org/wiki/Operation_Perch "Operation Perch") (Normandy, France, 1944)
-   [Warsaw Uprising](https://en.wikipedia.org/wiki/Warsaw_Uprising "Warsaw Uprising") (1944)

1948 Arab–Israeli War:

-   [Battle of Yad Mordechai](https://en.wikipedia.org/wiki/Battle_of_Yad_Mordechai "Battle of Yad Mordechai") (Israel, 1948)
-   [Battles of the Kinarot Valley](https://en.wikipedia.org/wiki/Battles_of_the_Kinarot_Valley "Battles of the Kinarot Valley") (Israel, 1948)

Indo-Pakistani War of 1971

-   [Battle of Longewala](https://en.wikipedia.org/wiki/Battle_of_Longewala "Battle of Longewala") (India, 1971)[[52]](https://en.wikipedia.org/wiki/PIAT#cite_note-p.42,_Sharma-53)
# Lanchester submachine gun - MP 18

# Lanchester submachine gun

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#mw-head) [Jump to search](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#searchInput)

Lanchester

[![Lanchester5.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Lanchester5.jpg/300px-Lanchester5.jpg)](https://en.wikipedia.org/wiki/File:Lanchester5.jpg)

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/9dc93bb0-0d78-4fd5-8e2a-749e510e963c/dbls5rp-435058d5-7bc6-447d-8e21-0b67bd8be855.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzlkYzkzYmIwLTBkNzgtNGZkNS04ZTJhLTc0OWU1MTBlOTYzY1wvZGJsczVycC00MzUwNThkNS03YmM2LTQ0N2QtOGUyMS0wYjY3YmQ4YmU4NTUuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.5sID1_JbuXH_QgwREwMxPzNv2Ce70nHBZTFx4pEzhYw)

Type

[Submachine gun](https://en.wikipedia.org/wiki/Submachine_gun "Submachine gun")

Place of origin

United Kingdom

Service history

In service

1941–1960

Used by

See [Users](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#Users)

Wars

[World War II](https://en.wikipedia.org/wiki/World_War_II "World War II")  
[Indonesian National Revolution](https://en.wikipedia.org/wiki/Indonesian_National_Revolution "Indonesian National Revolution")[[1]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-mcnab2002-1)  
[Suez Crisis](https://en.wikipedia.org/wiki/Suez_Crisis "Suez Crisis")[[2]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-wwiiafterwwii.wordpress.com-2)  
[Nigerian Civil War](https://en.wikipedia.org/wiki/Nigerian_Civil_War "Nigerian Civil War")

Production history

Designer

George Lanchester

Designed

1940

Manufacturer

[Sterling Armaments Company](https://en.wikipedia.org/wiki/Sterling_Armaments_Company "Sterling Armaments Company")

Produced

1941–1945

Variants

Mk.I, Mk.I*

Specifications

Mass

9.57 lb (4.3 kg)

Length

33.5 in (850.9 mm)

[Barrel](https://en.wikipedia.org/wiki/Gun_barrel "Gun barrel") length

8 in (203.2 mm)

---

[Cartridge](https://en.wikipedia.org/wiki/Cartridge_(firearms) "Cartridge (firearms)")

[9×19mm Parabellum](https://en.wikipedia.org/wiki/9%C3%9719mm_Parabellum "9×19mm Parabellum")

[Action](https://en.wikipedia.org/wiki/Action_(firearms) "Action (firearms)")

[Blowback](https://en.wikipedia.org/wiki/Blowback_(firearms) "Blowback (firearms)"), [Open bolt](https://en.wikipedia.org/wiki/Open_bolt "Open bolt")

[Rate of fire](https://en.wikipedia.org/wiki/Rate_of_fire "Rate of fire")

600 round/min

[Muzzle velocity](https://en.wikipedia.org/wiki/Muzzle_velocity "Muzzle velocity")

1,245 ft/s, 380 m/s

Effective firing range

150m

Feed system

32- or 50-round detachable [box magazine](https://en.wikipedia.org/wiki/Magazine_(firearms)#Box "Magazine (firearms)")

Sights

Front blade; rear adjustable

The **Lanchester** is a [submachine gun](https://en.wikipedia.org/wiki/Submachine_gun "Submachine gun") (SMG) manufactured by the [Sterling Armaments Company](https://en.wikipedia.org/wiki/Sterling_Armaments_Company "Sterling Armaments Company") between 1941 and 1945. It is a copy of the German [MP28/II](https://en.wikipedia.org/wiki/MP_18#Evolution "MP 18") and was manufactured in two versions, Mk.1 and Mk.1*; the latter was a simplified version of the original Mk.1, with no fire selector and simplified sights. It was primarily used by the British [Royal Navy](https://en.wikipedia.org/wiki/Royal_Navy "Royal Navy") during the [Second World War](https://en.wikipedia.org/wiki/World_War_II "World War II"), and to a lesser extent by the [Royal Air Force Regiment](https://en.wikipedia.org/wiki/Royal_Air_Force_Regiment "Royal Air Force Regiment") (for airfield protection). It was given the general designation of Lanchester after [George Herbert Lanchester](https://en.wikipedia.org/wiki/George_Lanchester "George Lanchester"), who was charged with producing the weapon at the Sterling Armaments Company.

## Contents

-   [1 History](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#History)
-   [2 Production](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#Production)
-   [3 Markings](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#Markings)
-   [4 Operation](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#Operation)
-   [5 Service](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#Service)
-   [6 Users](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#Users)
-   [7 See also](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#See_also)
-   [8 References](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#References)
-   [9 External links](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#External_links)

## History

Following the [Dunkirk](https://en.wikipedia.org/wiki/Battle_of_Dunkirk "Battle of Dunkirk") evacuation in 1940, the [Royal Air Force](https://en.wikipedia.org/wiki/Royal_Air_Force "Royal Air Force") decided it required a [submachine gun](https://en.wikipedia.org/wiki/Submachine_gun "Submachine gun") for [airfield](https://en.wikipedia.org/wiki/Airfield "Airfield") defence. With no time available for the usual research and development of a new weapon, it was decided to create a direct copy of the German [MP 28](https://en.wikipedia.org/wiki/MP_18#Evolution "MP 18"). The [British Admiralty](https://en.wikipedia.org/wiki/British_Admiralty "British Admiralty") decided to join with the RAF in adopting the new weapon, and played a key role in its design. Ultimately, it was within the Royal Navy that most of the Lanchesters that were produced went into service.[[3]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-Atlas-fr-3)

The British copy of the MP28 was given the general designation of Lanchester after George Herbert Lanchester, who was charged with producing the weapon at the [Sterling Armaments Company](https://en.wikipedia.org/wiki/Sterling_Armaments_Company "Sterling Armaments Company"), the same company that later produced the [Sterling submachine gun](https://en.wikipedia.org/wiki/Sterling_submachine_gun "Sterling submachine gun").[[3]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-Atlas-fr-3)

The Lanchester was envisioned as a weapon used for guarding prisoners and accompanying naval landing and assault parties. It was a very solid, well-made submachine gun of high-quality materials, in many ways the complete opposite of its direct contemporary, the [Sten](https://en.wikipedia.org/wiki/Sten_gun "Sten gun").[[3]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-Atlas-fr-3)

The Lanchester had a heavy wooden butt and [stock](https://en.wikipedia.org/wiki/Stock_(firearms) "Stock (firearms)"), a machined-steel action and breech block, a [magazine](https://en.wikipedia.org/wiki/Magazine_(firearms) "Magazine (firearms)") housing made from solid [brass](https://en.wikipedia.org/wiki/Brass "Brass")[[3]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-Atlas-fr-3) (later steel) and a mounting on the muzzle for use of a long-bladed 1907 [bayonet](https://en.wikipedia.org/wiki/Bayonet "Bayonet"). The rifling differed from the German original in details to accommodate various lots of 9mm ammunition then being acquired for service use.

Produced in two versions, Mk.1 and Mk.1*. The Mk.1* was a simplified version of the original Mk.1, which omitted the fire mode selector (full automatic only) and used simplified sights.[[3]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-Atlas-fr-3)

## Production

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Fighting_in_the_Dark._2_January_1943%2C_Liverpool%2C_the_Navy%27s_Lanchester_Gun_Fitted_With_Illumination_Attachment_For_Night_Operation._A13831.jpg/220px-Fighting_in_the_Dark._2_January_1943%2C_Liverpool%2C_the_Navy%27s_Lanchester_Gun_Fitted_With_Illumination_Attachment_For_Night_Operation._A13831.jpg)](https://en.wikipedia.org/wiki/File:Fighting_in_the_Dark._2_January_1943,_Liverpool,_the_Navy%27s_Lanchester_Gun_Fitted_With_Illumination_Attachment_For_Night_Operation._A13831.jpg)

A Lanchester gun fitted with an early version of a [Tactical light](https://en.wikipedia.org/wiki/Tactical_light "Tactical light") in a Royal Navy experiment from 1943.

The first contract of 13 June 1941 produced an initial 50,000 Lanchesters that were nearly all for the Royal Navy use. The British Army by now had supplies of the US-produced [Thompson SMG](https://en.wikipedia.org/wiki/Thompson_submachine_gun "Thompson submachine gun"). The final contract was issued on 9 October 1943. Production averaged 3,410 units per month over 28 months. According to contract records, Sterling was to have made guns serially numbered from 1 to 9999, then (S) A1 to about A64580.

Certain numbers of Mk.1 were modified latter in the war and designated Mk.1*, the key differences being the removal of the fire-selector switch and addition of simplified rear sights. This modification makes it difficult to ascertain exactly the production runs for each model.

There were four Lanchester assembly plants, though Lanchester assembly contracts were actually awarded to only three firms. Sterling assembly of the Lanchester was split between the Sterling Engineering Company Ltd in Dagenham (code S109), and the Sterling Armaments Company in Northampton (code M619).

Quantities produced:

-   Sterling (two factories; codes S109 and M619): approx. 74,579.
-   [Greener](https://en.wikipedia.org/wiki/W._W._Greener "W. W. Greener") (code M94): approx. 16,990.
-   Boss (code S156): approx. 3,900.

Some early version do not appear to be code marked at all except by the serial number prefix of "S", "A", or "SA". Pieces marked with the "A" prefix lacked perfect interchangeability and often required hand fitting of components.

## Markings

The year of manufacture of any particular Lanchester can be found stamped in small almost indistinguishable numbers next to the crossed flags military proof mark on the top of the rearmost magazine housing flange.

Sterling-made MK.1 Lanchester guns are marked on top of the magazine housing as follows:

-   LANCHESTER
-   MK.I
-   SA.
-   XXXX

("S" indicates Sterling manufacture and "A" indicates serial number prefix)

## Operation

The Lanchester is an open-bolt, self-loading [blowback](https://en.wikipedia.org/wiki/Blowback_(firearms) "Blowback (firearms)")-operated weapon with a [selective-fire](https://en.wikipedia.org/wiki/Selective_fire "Selective fire") option (located in front of the [trigger](https://en.wikipedia.org/wiki/Trigger_(firearms) "Trigger (firearms)")) on early versions. A tubular receiver was attached to the front of the wooden stock, which could be pivoted [barrel](https://en.wikipedia.org/wiki/Gun_barrel "Gun barrel") down for maintenance and disassembly. The wooden stock was patterned after that of Lee–Enfield rifle, and a bayonet lug centred below the [muzzle](https://en.wikipedia.org/wiki/Muzzle_(firearms) "Muzzle (firearms)") accepted the Pattern 1907 [sword-bayonet](https://en.wikipedia.org/wiki/Sword_bayonet "Sword bayonet") as used on the Lee–Enfield No. 1 Mk. III* (previously called the S.M.L.E.)

It used a straight 50-round magazine containing [9×19mm Parabellum](https://en.wikipedia.org/wiki/9%C3%9719mm_Parabellum "9×19mm Parabellum") cartridges (special pouches were produced to hold three magazines each) which fit into the magazine housing from the left, with spent cartridges ejected on the right. It was interchangeable with the shorter 32-round Sten magazine. A magazine loading tool was needed to load both 32- and 50-round magazines more easily. One of the two magazine pouches had a special pocket on the front for this loader.

Mk.1s featured a front blade sight with adjustable rifle-type sights, marked between 100 and 600 yards. Mk.1* featured a much simplified flip-up sight marked 100 or 200 yards.

Manual safety is made in the form of locking cut, made in the receiver, which engages the bolt handle to lock bolt in [open (cocked) position](https://en.wikipedia.org/wiki/Open_bolt "Open bolt"). It proved notoriously susceptible to accidental discharge if the weapon were dropped.[[3]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-Atlas-fr-3) For cleaning, the weapon had a brass oiler bottle and pull through held inside the butt stock (similar to the Lee–Enfield rifle).

## Service

The Lanchester gave good service to the Royal Navy, [Royal Canadian Navy](https://en.wikipedia.org/wiki/Royal_Canadian_Navy "Royal Canadian Navy") and other Commonwealth navies throughout the war and for some decades after. The last examples left Royal Naval service in the 1970s and are now collectors' items.

A large number of Lanchesters were subsequently sold off to foreign nations. These are often marked with two [broad arrows](https://en.wikipedia.org/wiki/Broad_arrow "Broad arrow"), point-to-point (appearing as a six-pointed star), stamped just before the serial number. This symbol is sometimes accompanied by the letter "S" and denotes "Sold out of Service".

## Users

-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Flag_of_Argentina.svg/23px-Flag_of_Argentina.svg.png) [Argentina](https://en.wikipedia.org/wiki/Argentina "Argentina")[[2]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-wwiiafterwwii.wordpress.com-2)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/23px-Flag_of_Australia_%28converted%29.svg.png) [Australia](https://en.wikipedia.org/wiki/Australia "Australia")[[2]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-wwiiafterwwii.wordpress.com-2)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Flag_of_Biafra.svg/23px-Flag_of_Biafra.svg.png) [Biafra](https://en.wikipedia.org/wiki/Biafra "Biafra")[[4]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-4)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Flag_of_Chile.svg/23px-Flag_of_Chile.svg.png) [Chile](https://en.wikipedia.org/wiki/Chile "Chile")[[2]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-wwiiafterwwii.wordpress.com-2)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Cyprus.svg/23px-Flag_of_Cyprus.svg.png) [Cyprus](https://en.wikipedia.org/wiki/Cyprus "Cyprus")
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_the_Dominican_Republic.svg/23px-Flag_of_the_Dominican_Republic.svg.png) [Dominican Republic](https://en.wikipedia.org/wiki/Dominican_Republic "Dominican Republic"): Used by the Constitutionalists in the [Dominican Civil War](https://en.wikipedia.org/wiki/Dominican_Civil_War "Dominican Civil War") of 1965.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Egypt.svg/23px-Flag_of_Egypt.svg.png) [Egypt](https://en.wikipedia.org/wiki/Egypt "Egypt")[[2]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-wwiiafterwwii.wordpress.com-2)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Greece.svg/23px-Flag_of_Greece.svg.png) [Greece](https://en.wikipedia.org/wiki/Greece "Greece") Hellenic Navy
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/23px-Flag_of_Myanmar.svg.png) [Myanmar](https://en.wikipedia.org/wiki/Myanmar "Myanmar"):Retired.[[5]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-5)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/23px-Flag_of_the_Netherlands.svg.png) [Netherlands](https://en.wikipedia.org/wiki/Netherlands "Netherlands")[[1]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-mcnab2002-1)
    -    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/23px-Flag_of_the_Netherlands.svg.png) [Dutch East Indies](https://en.wikipedia.org/wiki/Dutch_East_Indies "Dutch East Indies")[[1]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-mcnab2002-1)
-    ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/23px-Flag_of_New_Zealand.svg.png) [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand")[[2]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-wwiiafterwwii.wordpress.com-2)
-    ![](https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/23px-Flag_of_the_United_Kingdom.svg.png) [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom")[[1]](https://en.wikipedia.org/wiki/Lanchester_submachine_gun#cite_note-mcnab2002-1)
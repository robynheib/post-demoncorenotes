# [torch ambience loop](https://freesound.org/people/LordStirling/sounds/483692/#)

# [357Mag.wav](https://freesound.org/people/Jon285/sounds/49512/#)

# [Cocking a revolver](https://freesound.org/people/acidsnowflake/sounds/402790/#)

# [Monster Sounds](https://freesound.org/people/NachtmahrTV/packs/31014/) » [Scream in pain](https://freesound.org/people/NachtmahrTV/sounds/556706/#)

# [[Ambient Loop] Simple ambient loops](https://freesound.org/people/old_waveplay/packs/29763/) » [[Ambient Loop] Deep ambient pulses 2](https://freesound.org/people/old_waveplay/sounds/529137/#)
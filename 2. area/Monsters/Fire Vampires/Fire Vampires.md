## The Fire Vampires of Fthaggua

![[The_Fire_Vampires.png]]

These entities are [[Fthaggua]]'s subjects and dwell with him upon Ktynga. They appear as electrical discharges of bright crimson and, like their lord, feast upon the life-energy of sentient creatures. During this process, the victim will burst into flames as the Fire Vampires consume not only their essence, but their very memories as well. These stolen memories are added to the species' gestalt consciousness, which in turn allows them to better plan their raids against the races of the universe, as the Fire Vampires' sole purpose in life is to feed.

[[Fthaggua]] and his terrible race are due to reach planet Earth around four hundred years from now.

![[Screenshot (9).png]]
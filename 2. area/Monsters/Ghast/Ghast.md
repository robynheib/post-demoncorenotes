# Ghast

**Ghasts** are humanoid creatures who dwell in the vaults of Zin where they are often hunted by the [Gugs](https://lovecraft.fandom.com/wiki/Gug "Gug"). Their language appears to consist of "coughing gutturals".

They also appear to travel in packs, as [Randolph Carter](https://lovecraft.fandom.com/wiki/Randolph_Carter "Randolph Carter") meets a group of fifteen. Together they seemingly manage to take down a Gug.

![[Ghast.jpg]]

![[25-vision_of_hell.jpg]]
# Cthugha
Cthugha is a Great Old One
![[1000.png]]

### Also known as:

	The Living Flame  
	The Burning One  
	Kathigu-Ra
	
### Powers and Abilities

	Pyrokinesis

Cthugha resembles a giant ball of fire. He is served by the _Flame Creatures of Cthugha_. [[Fthaggua]], regent of the [[fire vampires]], may be his progeny. He has at least one other known progeny, the being known as [Aphoom-Zhah](https://lovecraft.fandom.com/wiki/Aphoom-Zhah "Aphoom-Zhah").

### Can also appear in the form:
![[720.png]]
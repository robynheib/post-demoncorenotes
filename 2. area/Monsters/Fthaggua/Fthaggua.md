# Fthaggua

![[Fthaggua.png]]

### Also known as

The King Regent of the Fire Vampires

### Relatives

[[Cthugha]], possible parent

## Overview

The King Regent of the [[Fire Vampires]], Fthaggua appears as a great flickering ball of cold blue flame. He lives with his servants within an enormous edifice upon the surface of the blue comet _Ktynga_, a celestial body which Fthaggua can steer across the cosmos and which is possessed of many strange properties, not least the incredible heat that it radiates or the fact that it can exceed the speed of light.

He is also the High Priest of the Great Old One known as [[Cthugha]], and may in fact even be its offspring, although this has yet to be confirmed.

